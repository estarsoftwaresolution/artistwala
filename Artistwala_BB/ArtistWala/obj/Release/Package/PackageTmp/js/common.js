﻿function checkTwoDecimalNumber(obj, evt) {
    var dotcontainer = obj.value.split('.');
    var precision = 0;
    precision = obj.value.split('.')[1];
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 8) {
        if (!(dotcontainer.length == 1 && charCode == 46) && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        if (precision.length > 1)
            return false;
    }
    return true;
}

function checkOnlyNumber(obj, evt) {
    var dotcontainer = obj.value.split('.');
    var charCode = (evt.which) ? evt.which : event.keyCode;

    if (charCode == 46)
    { return false; }

    if (charCode != 8) {
        if (!(dotcontainer.length == 1 && charCode == 46) && charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
    }
    return true;

}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


var today = new Date();
var expiry = new Date(today.getTime() + 24 * 3600 * 1000); // plus 1 days

function setCookie(name, value) {
    document.cookie = name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString();
}

function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}