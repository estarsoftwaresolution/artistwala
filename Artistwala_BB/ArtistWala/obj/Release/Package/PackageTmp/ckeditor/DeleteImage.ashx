﻿<%@ WebHandler Language="C#" Class="DeleteImage" %>

using System;
using System.Web;

public class DeleteImage : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {
        string filename = context.Request["imgname"];
        string baseurl = context.Server.MapPath(".") + "\\Images\\";
        try
        {
            System.IO.File.Delete(baseurl + filename);
        }
        catch
        {
            context.Response.Write("false");
            context.Response.End();
        }
        context.Response.Write("true");
        context.Response.End();
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}