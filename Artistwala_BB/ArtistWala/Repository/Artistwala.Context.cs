﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ArtistWala.Repository
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ArtistWalaEntities : DbContext
    {
        public ArtistWalaEntities()
            : base("name=ArtistWalaEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tblArtistCategory> tblArtistCategories { get; set; }
        public virtual DbSet<tblArtistCategoryMapping> tblArtistCategoryMappings { get; set; }
        public virtual DbSet<tblArtistCategoryType> tblArtistCategoryTypes { get; set; }
        public virtual DbSet<tblArtistGroupMember> tblArtistGroupMembers { get; set; }
        public virtual DbSet<tblArtistPortfolio> tblArtistPortfolios { get; set; }
        public virtual DbSet<tblArtistRating> tblArtistRatings { get; set; }
        public virtual DbSet<tblArtist> tblArtists { get; set; }
        public virtual DbSet<tblArtistSchedule> tblArtistSchedules { get; set; }
        public virtual DbSet<tblArtistSubCategory> tblArtistSubCategories { get; set; }
        public virtual DbSet<tblClient> tblClients { get; set; }
        public virtual DbSet<tblEmailTemplate> tblEmailTemplates { get; set; }
        public virtual DbSet<tblEventType> tblEventTypes { get; set; }
        public virtual DbSet<tblGeneralSetting> tblGeneralSettings { get; set; }
        public virtual DbSet<tblModarator> tblModarators { get; set; }
        public virtual DbSet<tblQuote> tblQuotes { get; set; }
        public virtual DbSet<tblRole> tblRoles { get; set; }
        public virtual DbSet<tblStaticContent> tblStaticContents { get; set; }
        public virtual DbSet<tblUserContact> tblUserContacts { get; set; }
        public virtual DbSet<tblUserMessage> tblUserMessages { get; set; }
        public virtual DbSet<tblUser> tblUsers { get; set; }
        public virtual DbSet<tblQuoteRatingMapping> tblQuoteRatingMappings { get; set; }
        public virtual DbSet<tblArtistOverview> tblArtistOverviews { get; set; }
        public virtual DbSet<tblSMSTemplate> tblSMSTemplates { get; set; }
    }
}
