//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ArtistWala.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblArtistOverview
    {
        public int OverviewId { get; set; }
        public Nullable<int> FK_ArtistId { get; set; }
        public string WhatToExpect { get; set; }
        public string Influence { get; set; }
        public string Calender { get; set; }
        public string Personal { get; set; }
        public string Tesimonials { get; set; }
        public string SetUpRequirements { get; set; }
    
        public virtual tblArtist tblArtist { get; set; }
    }
}
