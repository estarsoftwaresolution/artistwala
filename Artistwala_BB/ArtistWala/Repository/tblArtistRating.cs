//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ArtistWala.Repository
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblArtistRating
    {
        public tblArtistRating()
        {
            this.tblQuoteRatingMappings = new HashSet<tblQuoteRatingMapping>();
        }
    
        public int ArtistRatingId { get; set; }
        public int FK_ArtistId { get; set; }
        public Nullable<bool> IsRated { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> Rating { get; set; }
        public string Comment { get; set; }
    
        public virtual tblArtist tblArtist { get; set; }
        public virtual ICollection<tblQuoteRatingMapping> tblQuoteRatingMappings { get; set; }
    }
}
