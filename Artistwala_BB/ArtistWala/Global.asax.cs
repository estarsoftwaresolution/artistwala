﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace ArtistWala
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static TimeSpan whenTaskLastRan;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //TimeSpan abc = new TimeSpan(17, 52, 00);
            //whenTaskLastRan = abc;
            //if (System.DateTime.Now.TimeOfDay == abc)
            //{ DoTask();}
            whenTaskLastRan = new TimeSpan(18, 43, 00);
            //DoTask();
        }

        protected void Session_Start(Object sender, EventArgs E)
        {
            //DoTask();
            FormsAuthentication.SignOut();
        }

        protected void Session_End(Object sender, EventArgs E)
        {
        }

        static void DoTask()
        {
            var oneDayAgo = DateTime.Now.TimeOfDay;
            if (whenTaskLastRan<oneDayAgo)
            {
                var id = "Success";
                //var path = HttpContext.Current.Server.MapPath("Uploads");
                //var folder = new DirectoryInfo(path);
                //FileInfo[] files = folder.GetFiles();
                //foreach (var file in files)
                //{
                //    if (file.CreationTime.IsOlderThan(oneDayAgo))
                //    {
                //        File.Delete(path + file.Name);
                //    }
                //}
                //whenTaskLastRan = new TimeSpan(18, 27, 00);
            }
        }


    }
}


//TimeSpan abc = new TimeSpan(17, 52, 00);
//            whenTaskLastRan = abc;
//            if (System.DateTime.Now.TimeOfDay == abc)
//            { DoTask();}