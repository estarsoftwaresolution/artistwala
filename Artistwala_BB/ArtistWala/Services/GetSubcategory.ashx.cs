﻿using ArtistWala.Common;
using ArtistWala.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ArtistWala.Repository
{
    /// <summary>
    /// Summary description for GetSubcategory
    /// </summary>
    public class GetSubcategory : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            CommonMethods_bak objCommon = new CommonMethods_bak();
            List<SubCategoryModel> subcategoryList = new List<SubCategoryModel>();
            ArtistWalaEntities dbcontext = new ArtistWalaEntities();
            int catType = default(int);
            int.TryParse(context.Request.QueryString["catType"], out catType);
            if (catType > 0)
            {
                subcategoryList = dbcontext.tblArtistSubCategories.Where(x => x.FK_ArtistCategoryId == catType)
                                                     .Select(x => new SubCategoryModel
                                                     {
                                                         SubCategoryId = x.ArtistSubCategoryId,
                                                         SubCategoryName = x.SubCategoryName
                                                     }).ToList();
            }

            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write(objCommon.JsonSerialize(subcategoryList));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}