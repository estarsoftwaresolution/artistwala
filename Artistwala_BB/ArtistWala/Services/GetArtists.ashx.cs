﻿using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ArtistWala.Services
{
    /// <summary>
    /// Summary description for GetArtists
    /// </summary>
    public class GetArtists : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            CommonMethods_bak objCommon = new CommonMethods_bak();
            List<LoginModel> userList = new List<LoginModel>();
            ArtistWalaEntities dbcontext = new ArtistWalaEntities();

            string name = string.Empty;
            name = context.Request.QueryString["name"];

            if (!string.IsNullOrEmpty(name))
            {
                userList = dbcontext.tblUsers.Where(x => x.Name.Contains(name)).Select(x => new LoginModel
                {
                    UserId = x.UserId,
                    Name = (x.Name + " (" + x.UserName + ")")
                }).OrderBy(x => x.Name).ToList();
            }
            else
            {
                userList = dbcontext.tblUsers.Select(x => new LoginModel
                {
                    UserId = x.UserId,
                    Name = (x.Name + " (" + x.UserName + ")")
                }).OrderBy(x => x.Name).ToList();
            }

            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write(objCommon.JsonSerialize(userList));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}