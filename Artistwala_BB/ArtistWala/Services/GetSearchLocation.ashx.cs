﻿using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ArtistWala.Services
{
    /// <summary>
    /// Summary description for GetSearchLocation
    /// </summary>
    public class GetSearchLocation : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            ArtistWalaEntities db = new ArtistWalaEntities();
            CommonMethods objCommon = new CommonMethods();
            List<LoginModel> userList = new List<LoginModel>();

            string Location = Convert.ToString(context.Request.QueryString["Location"]);
            if (!string.IsNullOrEmpty(Location))
            {
                userList = db.tblArtists.Where(w => w.City.Contains(Location)).GroupBy(g => g.City).Select(s => new LoginModel { Name = s.FirstOrDefault().City }).Take(10).ToList();
            }
            else
            {
                userList = db.tblArtists.Select(s => new LoginModel { Name = s.City }).ToList();
            }
            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write(objCommon.JsonSerialize(userList));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}