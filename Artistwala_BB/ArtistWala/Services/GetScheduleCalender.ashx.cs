﻿using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace ArtistWala.Services
{
    /// <summary>
    /// Summary description for GetScheduleCalender
    /// </summary>
    public class GetScheduleCalender : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            CommonMethods_bak objCommon = new CommonMethods_bak();
            List<CalenderSchedule> artistList = new List<CalenderSchedule>();
            ArtistWalaEntities dbcontext = new ArtistWalaEntities();
            int ArtistId = default(int);

            LoginModel objLogin = (LoginModel)context.Session[SessionConstants.UserProfileObject];


            if (context.Request.QueryString["ArtistId"] != null)
            {
                int.TryParse(context.Request.QueryString["ArtistId"], out ArtistId);
            }
            else if (objLogin != null && objLogin.ArtistId > 0)
            {
                ArtistId = objLogin.ArtistId;
            }
            else if (context.Request.QueryString["Profile"] != null)
            {
                string userName = context.Request.QueryString["Profile"];

                ArtistId = (from u in dbcontext.tblUsers
                            join a in dbcontext.tblArtists on u.UserId equals a.FK_UserId
                            where u.UserName == userName
                            select a.ArtistId).FirstOrDefault();
            }

            if (ArtistId > 0)
            {
                var schedule = dbcontext.tblArtistSchedules.Where(x => x.FK_ArtistId == ArtistId && x.IsActive == true).ToList();

                foreach (var item in schedule)
                {
                    long dt = (item.ScheduleDate.Value.Ticks - 621355968000000000) / TimeSpan.TicksPerMillisecond;

                    //long dt = item.ScheduleDate.Value.ToFileTime() / 10000;
                    CalenderSchedule cm = new CalenderSchedule()
                    {
                        description = item.Description,
                        date = dt.ToString(),
                        title = item.Title,
                        type = item.Type,
                        url = item.Url,
                    };

                    artistList.Add(cm);
                }
            }

            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write(objCommon.JsonSerialize(artistList));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}