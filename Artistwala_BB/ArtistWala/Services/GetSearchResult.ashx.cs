﻿using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ArtistWala.Services
{
    /// <summary>
    /// Summary description for GetSearchResult
    /// </summary>
    public class GetSearchResult : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            CommonMethods objCommon = new CommonMethods();
            List<LoginModel> userList = new List<LoginModel>();
            List<LoginModel> userList1 = new List<LoginModel>();

            ArtistWalaEntities dbcontext = new ArtistWalaEntities();

            string name = string.Empty;
            name = context.Request.QueryString["name"];

            if (!string.IsNullOrEmpty(name))
            {
                //userList = dbcontext.tblArtists.Where(w => w.ArtistName.StartsWith(name)).Select(s => new LoginModel { UserId = s.ArtistId, Name = s.ArtistName }).ToList();
                userList1 = dbcontext.tblArtistSubCategories.Where(x => x.SubCategoryName.Contains(name)).Select(s => new LoginModel { UserId = s.ArtistSubCategoryId, Name = s.SubCategoryName }).Take(10).ToList();

                userList = userList.Concat(userList1).ToList();
            }
            else
            {
                userList = dbcontext.tblUsers.Select(x => new LoginModel
                {
                    UserId = x.UserId,
                    Name = (x.Name + " (" + x.UserName + ")")
                }).OrderBy(x => x.Name).ToList();
            }

            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write(objCommon.JsonSerialize(userList));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}