﻿using System.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ArtistWala.Repository;
using System.Text;
using System.Data.Entity;
using ArtistWala.Common;
using System.Web.Security;

namespace ArtistWala.Services
{
    /// <summary>
    /// Summary description for BookQuotes
    /// </summary>
    public class BookQuotes : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            CommonMethods_bak objCommon = new CommonMethods_bak();
            string ModaratorName = string.Empty;
            int UserId = default(int);
            int QuotesId = default(int);
            ArtistWalaEntities dbcontext = new ArtistWalaEntities();
            
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                int.TryParse(authTicket.UserData, out UserId);
                // data is empty !!!
            }

            int.TryParse(context.Request.QueryString["QuotesId"], out QuotesId);

            tblQuote objQuotes = dbcontext.tblQuotes.Find(QuotesId);
            if (context.Request.QueryString["check"] == "true")
            {
                objQuotes.AssignTo = UserId;

                dbcontext.Entry(objQuotes).State = EntityState.Modified;
                dbcontext.SaveChanges();

                var mod = dbcontext.tblUsers.Where(x => x.UserId == UserId).FirstOrDefault();

                if (mod != null)
                {
                    ModaratorName = mod.Name;
                }
            }
            else
            {
                objQuotes.AssignTo = null;
            }

           

            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write(objCommon.JsonSerialize(ModaratorName));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}