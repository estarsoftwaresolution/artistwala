﻿using ArtistWala.Areas.Admin.Models;
using ArtistWala.Common;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;

namespace ArtistWala.Services
{
    /// <summary>
    /// Summary description for ModaratorReport
    /// </summary>
    public class ModaratorReport : IHttpHandler
    {
        CommonMethods_bak objCommon = new CommonMethods_bak();
        ArtistWalaEntities dbcontext = new ArtistWalaEntities();
        public void ProcessRequest(HttpContext context)
        {
            string SearchBy = context.Request.QueryString["SearchBy"];

            int ModId = default(int);
            DateTime fromDate = default(DateTime);
            DateTime toDate = default(DateTime);
            int Month = default(int);
            DayOfWeek Week = DateTime.Now.DayOfWeek;
            string result = string.Empty;

            List<DayRecordCount> objCount = new List<DayRecordCount>();

            int.TryParse(context.Request.QueryString["ModId"], out ModId);

            switch (SearchBy)
            {
                case "Date":
                    DateTime.TryParse(context.Request.QueryString["FromDate"], out fromDate);
                    DateTime.TryParse(context.Request.QueryString["toDate"], out toDate);



                    var record = (from a in dbcontext.tblArtists
                                  where (DbFunctions.TruncateTime(a.AddedOn).Value >= DbFunctions.TruncateTime(fromDate) && DbFunctions.TruncateTime(a.AddedOn).Value <= DbFunctions.TruncateTime(toDate))
                                  group a by new
                                 {
                                     a.FK_UserId,
                                     a.AddedOn
                                 } into gcs
                                  join m in dbcontext.tblModarators on gcs.Key.FK_UserId equals m.FK_UserId
                                  where (m.ModaratorId == ModId)
                                  group m by new
                                {
                                    gcs.Key.FK_UserId,
                                    m.Name,
                                } into mod
                                  select new { NoofArtist = mod.Count(), mod.Key.Name }).ToList();

                    for (int i = 0; i < record.Count; i++)
                    {
                        DayRecordCount days = new DayRecordCount();
                        days.Name = record[i].Name;
                        days.count = record[i].NoofArtist;

                        objCount.Add(days);
                    }

                    context.Response.ContentType = "application/json";
                    context.Response.ContentEncoding = Encoding.UTF8;
                    context.Response.Write(objCommon.JsonSerialize(objCount));
                    context.Response.End();

                    break;
                case "Week":
                    List<DateTime> dt = objCommon.GetWeeks(Week, DateTime.Now.AddDays(-7));
                    var Records = (from a in dbcontext.tblArtists
                                   where (dt.Contains(DbFunctions.TruncateTime(a.AddedOn).Value))
                                   group a by new
                                  {
                                      a.FK_UserId,
                                      a.AddedOn
                                  } into gcs
                                   join m in dbcontext.tblModarators on gcs.Key.FK_UserId equals m.FK_UserId
                                   where (m.ModaratorId == ModId)
                                   group m by new
                                 {
                                     gcs.Key.FK_UserId,
                                     m.Name,
                                 } into mod
                                   select new { NoofArtist = mod.Count(), mod.Key.Name }).ToList();

                    for (int i = 0; i < Records.Count; i++)
                    {
                        DayRecordCount days = new DayRecordCount();
                        days.Name = Records[i].Name;
                        days.count = Records[i].NoofArtist;

                        objCount.Add(days);
                    }

                    context.Response.ContentType = "application/json";
                    context.Response.ContentEncoding = Encoding.UTF8;
                    context.Response.Write(objCommon.JsonSerialize(objCount));
                    context.Response.End();
                    break;
                default:
                    int.TryParse(context.Request.QueryString["month"], out Month);
                    var records = (from a in dbcontext.tblArtists
                                   where (a.AddedOn.Value.Month == Month)
                                   group a by new
                                  {
                                      a.FK_UserId,
                                      a.AddedOn
                                  } into gcs
                                   join m in dbcontext.tblModarators on gcs.Key.FK_UserId equals m.FK_UserId
                                   where (m.ModaratorId == ModId)
                                   group m by new
                                 {
                                     gcs.Key.FK_UserId,
                                     m.Name,
                                 } into mod
                                   select new { NoofArtist = mod.Count(), mod.Key.Name }).ToList();

                    for (int i = 0; i < records.Count; i++)
                    {
                        DayRecordCount days = new DayRecordCount();
                        days.Name = records[i].Name;
                        days.count = records[i].NoofArtist;

                        objCount.Add(days);
                    }

                    context.Response.ContentType = "application/json";
                    context.Response.ContentEncoding = Encoding.UTF8;
                    context.Response.Write(objCommon.JsonSerialize(objCount));
                    context.Response.End();
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}