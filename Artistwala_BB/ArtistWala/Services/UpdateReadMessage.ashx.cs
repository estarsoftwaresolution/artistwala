﻿using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;

namespace ArtistWala.Services
{
    /// <summary>
    /// Summary description for UpdateReadMessage
    /// </summary>
    public class UpdateReadMessage : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int id = default(int);
            int.TryParse(context.Request.QueryString["id"], out id);
            ArtistWalaEntities dbcontext = new ArtistWalaEntities();
            tblUserMessage objUserMessage = dbcontext.tblUserMessages.Where(x => x.MessageId == id).FirstOrDefault();

            objUserMessage.IsUnread = false;

            dbcontext.Entry(objUserMessage).State = EntityState.Modified;
            dbcontext.SaveChanges();

            context.Response.ContentType = "text/plain";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write("success");
            context.Response.End();           
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}