﻿using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace ArtistWala.Services
{
    /// <summary>
    /// Summary description for GetCategory
    /// </summary>
    public class GetCategory : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            CommonMethods_bak objCommon = new CommonMethods_bak();
            List<CategoryModel> categoryList = new List<CategoryModel>();
            ArtistWalaEntities dbcontext = new ArtistWalaEntities();
            int catType = default(int);
            int.TryParse(context.Request.QueryString["catType"], out catType);
            if (catType > 0)
            {
               categoryList = dbcontext.tblArtistCategories.Where(x => x.FK_ArtistCategoryTypeId == catType)
                                                    .Select(x => new CategoryModel
                                                    {
                                                        CategoryId = x.ArtistCategoryId,
                                                        CategoryName = x.CategoryName                                                        
                                                    }).ToList();
            }

            context.Response.ContentType = "application/json";
            context.Response.ContentEncoding = Encoding.UTF8;
            context.Response.Write(objCommon.JsonSerialize(categoryList));
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}