﻿using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ArtistWala.Controllers
{
    public class HomeController : Controller
    {
        ArtistWalaEntities context = new ArtistWalaEntities();
        CommonMethods objCommon = new CommonMethods();
        //
        // GET: /Home/
        public ActionResult Index()
        {
            string dd = "EventType";
            var blogs = context.Database.SqlQuery<string>("SELECT [" + dd + "] FROM dbo.tblStaticContent where [" + dd + "]!='' ").ToList();




            HomeModel home = GetAllHomeData();
            return View(home);
        }

        [HttpPost]
        public ActionResult Index(HomeModel home)
        {
            if (!string.IsNullOrEmpty(home.SearchId))
            {
                return RedirectToAction("SearchResults", "Users", new
                {
                    SearchText = HttpUtility.UrlDecode(!string.IsNullOrEmpty(home.SearchText) ? home.SearchText.ToLower() : ""),
                    SearchId = HttpUtility.UrlDecode(home.SearchId),
                    Location = HttpUtility.UrlDecode(home.Location)
                });
            }
            else
            {
                // return RedirectToAction("SearchResults", "Users", new { SearchText = HttpUtility.UrlDecode(!string.IsNullOrEmpty(home.SearchText) ? home.SearchText.ToLower() : ""), Location = HttpUtility.UrlDecode(!string.IsNullOrEmpty(home.Location) ? home.Location.ToLower() : "") });
                return RedirectToAction("SearchResults", "Users", new { SearchText = HttpUtility.UrlDecode(!string.IsNullOrEmpty(home.SearchText) ? home.SearchText.ToLower() : "") });
            }
        }

        public ActionResult headersearch()
        {
            return PartialView();
        }

        #region JSONAJAX
        public JsonResult SetLocation(string Location)
        {
            if (Location != "")
            {
                Request.Cookies["ArtistwalaLocation"].Value = Location;
            }

            return Json("");
        }
        #endregion


        public ActionResult AboutUs()
        {
            return View();
        }

        public ActionResult ContactUs()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ContactUs(ContactUsModel objContact)
        {
            tblUserContact objUser = new tblUserContact()
            {
                Name = objContact.Name,
                Email = objContact.Email,
                Company = objContact.Company,
                Phone = objContact.Phone,
                ContactReason = objContact.ContactReason,
                Message = objContact.Message,
                AddedDate = DateTime.Now
            };


            tblEmailTemplate email = context.tblEmailTemplates.Where(x => x.TemplateId == 5).FirstOrDefault();
            string emailBody = email.EmailBody.Replace("@Name", objUser.Name);

            string adminEmailSubject = "You have an inquiry | Admin";
            string adminEmailBody = "User Name:'" + objUser.Name + "', EmailId:'" + objUser.Email + "', Company: '" + objUser.Company + "',Phone:'" + objUser.Phone + "', Contact reason:'" + objUser.ContactReason + "', Message: '" + objUser.Message + "'";
            objCommon.sendMailInHtmlFormat(email.AddCC, objUser.Email, email.AddBCC, email.EmailSubject, emailBody);

            objCommon.sendMailInHtmlFormat(email.AddCC, email.AddCC, email.AddBCC, adminEmailSubject, adminEmailBody);

            context.tblUserContacts.Add(objUser);
            context.SaveChanges();

            ViewBag.Message = "Your message has been sent. We will contact you shortly.";

            return View();
        }

        public ActionResult faq()
        {
            return View();
        }

        public ActionResult Terms()
        {
            return View();
        }

        public ActionResult HowItWorks()
        {
            return View();
        }

        public PartialViewResult SearchArtist()
        {
            List<tblArtistCategory> CategoryList = context.tblArtistCategories.Where(x => x.IsActive == true).OrderByDescending(x => x.ArtistCategoryId).ToList();
            return PartialView(CategoryList);
        }

        public PartialViewResult TopFeatureArtist()
        {
            string loc = string.Empty;
            List<tblArtist> ArtistList = new List<tblArtist>();
            if (Request.Cookies["ArtistwalaLocation"] != null)
            {
                loc = Convert.ToString(HttpUtility.UrlDecode(Request.Cookies["ArtistwalaLocation"].Value)).Trim();
                ArtistList = context.tblArtists.Where(x => x.IsActive == true && x.IsFeatured == true && x.City == loc).OrderBy(x => x.AddedOn).ToList();

            }
            else
            {
                ArtistList = context.tblArtists.Where(x => x.IsActive == true && x.IsFeatured == true).ToList();
            }

            return PartialView(ArtistList);
        }

        public PartialViewResult FeatureArtist()
        {
            string loc = string.Empty;
            List<tblArtist> ArtistList = new List<tblArtist>();
            if (Request.Cookies["ArtistwalaLocation"] != null)
            {
                loc = Convert.ToString(Request.Cookies["ArtistwalaLocation"].Value).Trim();
                ArtistList = context.tblArtists.Where(x => x.IsActive == true && x.IsFeatured == true && x.City == loc).ToList();

            }
            else
            {
                ArtistList = context.tblArtists.Where(x => x.IsActive == true && x.IsFeatured == true).ToList();
            }

            return PartialView(ArtistList);
        }

        public PartialViewResult LoggedInMenu()
        {
            LoginModel UserTbl = objCommon.GetLoginDetails();

            return PartialView(UserTbl);
        }

        public PartialViewResult NormalMenu()
        {
            //LoginModel UserTbl = objCommon.GetLoginDetails();
            return PartialView();
        }

        public PartialViewResult MenuList()
        {
            LoginModel UserTbl = objCommon.GetLoginDetails();
            return PartialView(UserTbl);

        }

        #region Methods

        protected HomeModel GetAllHomeData()
        {
            try
            {
                HomeModel home = new HomeModel();
                home.CartgoryType = context.tblArtistCategoryTypes.ToList();

                return home;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetFirstTwentyCharechter(string input)
        {
            if (!string.IsNullOrEmpty(input))
                return input.Length > 20 ? input.Substring(0, 20) : input;
            else
                return "";
        }

        #endregion

        public ActionResult AutoMail()
        {
            SMSCAPI oSMS = new SMSCAPI();
            //int SndBy = Convert.ToInt32(Session["LogedUserID"].ToString());
            //int id = Convert.ToInt32(Session["ToSendUSerID"].ToString());
            //var idDetails = context.tblUsers.Where(x => x.UserId == id).Select(s => s).FirstOrDefault();
            DateTime today = System.DateTime.Today;
            try
            {
                var Quote = (from qm in context.tblQuoteRatingMappings
                             join ar in context.tblArtistRatings on qm.FK_RatingId equals ar.ArtistRatingId
                             join c in context.tblClients on ar.UserId equals c.FK_UserId
                             where qm.emaildate == today
                             select new
                             {
                                 c.ClientName,
                                 c.Email,
                                 c.ContactNo

                             }).ToList();

                // send message to Client                        
                foreach (var item in Quote)
                {
                    tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 3).FirstOrDefault();
                    string MessageBodyTo = SMSTo.SMSBody.Replace("@Name", item.ClientName);
                    oSMS.SendSMS(item.ContactNo.ToString(), MessageBodyTo);
                    tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 10).FirstOrDefault();
                    string emailBodyTo = emailTo.EmailBody.Replace("@Name", item.ClientName);
                    objCommon.sendMailInHtmlFormat(emailTo.AddCC, item.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);
                }

                var Artist = (from qm in context.tblQuoteRatingMappings
                             join ar in context.tblArtistRatings on qm.FK_RatingId equals ar.ArtistRatingId
                             join a in context.tblArtists on ar.FK_ArtistId equals a.ArtistId
                             join c in context.tblClients on a.FK_UserId equals c.FK_UserId
                             where qm.emaildate == today
                             select new
                             {
                                 c.ClientName,
                                 c.Email,
                                 c.ContactNo

                             }).ToList();

                // send message to artist                        
                foreach (var item in Artist)
                {
                    tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 4).FirstOrDefault();
                    string MessageBodyTo = SMSTo.SMSBody.Replace("@Name", item.ClientName);
                    oSMS.SendSMS(item.ContactNo.ToString(), MessageBodyTo);
                    tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 11).FirstOrDefault();
                    string emailBodyTo = emailTo.EmailBody.Replace("@Name", item.ClientName);
                    objCommon.sendMailInHtmlFormat(emailTo.AddCC, item.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);
                }


            }
            catch (Exception ex)
            {
                //ViewBag.errorMessage = "Type your message with your contact details";
                //return Json("");
            }

            //var MobileNofrom = context.tblClients.Where(s => s.FK_UserId == SndBy).Select(s => s).FirstOrDefault();
            //string MessageBodyFrom = "Dear " + MobileNofrom.ClientName + ", your message is sent. Please wait for the reply. Artistwala.com";
            //oSMS.SendSMS(MobileNofrom.ContactNo.ToString(), MessageBodyFrom);
            //tblEmailTemplate emailFrom = context.tblEmailTemplates.Where(x => x.TemplateId == 7).FirstOrDefault();
            //string emailBodyFrom = emailFrom.EmailBody.Replace("@Name", MobileNofrom.ClientName);
            //objCommon.sendMailInHtmlFormat(emailFrom.AddCC, MobileNofrom.Email, emailFrom.AddBCC, emailFrom.EmailSubject, emailBodyFrom);


            return View();
        }
    }
}