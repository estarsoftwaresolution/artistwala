using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using reCAPTCHA.MVC;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net;
using System.Data.Entity;
using System.Web.UI;

namespace ArtistWala.Controllers
{
    public class UsersController : Controller
    {
        ArtistWalaEntities context = new ArtistWalaEntities();
        AESEncryption objEncrp = new AESEncryption();
        CommonMethods objCommon = new CommonMethods();


        string Sessionprofile = string.Empty;

        //
        // GET: /Users/

        #region AjaxCall
        public JsonResult GenerateOPT(string UserName, string Email, string PhoneNo)
        {
            bool flag = false;
            var details = context.tblClients.Where(x => x.Email == Email || x.ContactNo == PhoneNo).Select(s => s.FK_UserId).FirstOrDefault();
            if (details == null)
            {
                SMSCAPI oSMSCAPI = new SMSCAPI();
                bool? CheckMobileValidation = context.tblGeneralSettings.Select(s => s.MobileVerification).FirstOrDefault();
                if (CheckMobileValidation == true)
                {
                    string MessageBody = string.Empty;
                    string passwordString = OTPGenerator();


                    if (passwordString != "")
                    {
                        Session["_OPT"] = passwordString;
                        tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 10).FirstOrDefault();
                        MessageBody = SMSTo.SMSBody.Replace("@Code", passwordString);
                        try
                        {
                            oSMSCAPI.SendSMS(PhoneNo, MessageBody);
                        }
                        catch { }
                        flag = true;
                    }
                }

            }
            else
            {

            }
            return Json(flag);
        }
        public JsonResult CheckOTP(string UserName, string Email, string PhoneNo, string OTPText)
        {
            bool flag = false;
            string GenerateOTP = (string)Session["_OPT"];
            if (GenerateOTP == OTPText)
            {
                RegistrationModel oRegister = new RegistrationModel();
                oRegister.Name = UserName;
                oRegister.Email = Email;
                oRegister.Phone = PhoneNo;
                JoinUs(oRegister);
                flag = true;
            }
            Session["_OPT"] = null;
            return Json(flag);
        }

        public JsonResult RegistrationWithoutOTP(string UserName, string Email, string PhoneNo)
        {
            bool flag = false;
            RegistrationModel oRegister = new RegistrationModel();
            oRegister.Name = UserName;
            oRegister.Email = Email;
            oRegister.Phone = PhoneNo;
            JoinUs(oRegister);
            flag = true;
            return Json(flag);

        }

        public JsonResult GenerateForgotPasswordOTP(string Email)
        {

            string PhoneNo = context.tblClients.Where(x => x.Email == Email).Select(s => s.ContactNo).FirstOrDefault();
            SMSCAPI oSMSCAPI = new SMSCAPI();
            bool flag = false;
            string MessageBody = string.Empty;
            string passwordString = OTPGenerator();
            if (passwordString != "")
            {
                Session["_OPT"] = passwordString;

                tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 10).FirstOrDefault();
                MessageBody = SMSTo.SMSBody.Replace("@Code", passwordString);
                try
                {
                    oSMSCAPI.SendSMS(PhoneNo, MessageBody);
                }
                catch { }
                flag = true;
            }
            return Json(PhoneNo);
        }
        public JsonResult CheckForgotPasswordOTP(string OTP)
        {
            bool flag = false;
            string SessionOTP = (string)Session["_OPT"];
            if (SessionOTP == OTP)
            {
                flag = true;
            }
            return Json(flag);

        }

        public JsonResult GetOverviewDetails(string ArtistId)
        {
            int Id = Convert.ToInt32(ArtistId);
            var OverViewDetails = context.tblArtistOverviews.Where(w => w.FK_ArtistId == Id).Select(s => new ArtistOverviewModel
            {
                WhatToExpect = s.WhatToExpect,
                Tesimonials = s.Tesimonials,
                SetUpRequirements = s.SetUpRequirements,
                Personal = s.Personal,
                Influence = s.Influence,
                Calender = s.Calender,
            }).FirstOrDefault();
            return Json(OverViewDetails, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public ActionResult Login(int id = 0)
        {
            switch (id)
            {
                case (int)ArtistWala.Models.AppsConstants.AuthenticationError.AccountExists:
                    ViewBag.Message = "This Email/Mobile No. is already register with us. Please login with your emailid/username and password.";
                    break;
                case (int)ArtistWala.Models.AppsConstants.AuthenticationError.InvalidUser:
                    ViewBag.Message = "Sorry! you are registered with us. Please reset your password using forgot password option.";
                    break;
                case (int)ArtistWala.Models.AppsConstants.AuthenticationError.EmailSend:
                    ViewBag.Message = "An email has been sent to your inbox with your password.";
                    break;
                case (int)ArtistWala.Models.AppsConstants.AuthenticationError.ServerError:
                    ViewBag.Message = "Sorry! Please try again later.";
                    break;
                default:
                    break;
            }

            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    String userName = model.UserName;
                    String passward = objEncrp.EncryptPassword(model.Password);
                    bool rememberUserName = model.IsRemember;
                    tblUser UserTbl = AuthenticateUser(userName, passward);
                    if (UserTbl != null)
                    {
                        model.Password = null;
                        model.Name = UserTbl.Name;
                        model.UserId = UserTbl.UserId;
                        model.ProfileImage = UserTbl.ProfileImage;
                        if (UserTbl.tblArtists.Any(x => x.FK_UserId == UserTbl.UserId))
                        {
                            model.ArtistId = UserTbl.tblArtists.Where(x => x.FK_UserId == UserTbl.UserId).FirstOrDefault().ArtistId;
                            model.IsArtist = true;

                        }
                        model.IsPromoKit = (UserTbl.IsPromoKit ?? false);
                        model.RoleId = UserTbl.FK_RoleId;
                        model.preview = UserTbl.UserName;
                        //Loged User Details
                        Session["LogedUserID"] = model.UserId;
                        Session["LogedUserName"] = model.Name;

                    }
                    else
                    {
                        // If we got this far, something failed, redisplay form
                        ViewBag.Message = "Username / Password is incorrect or your account does not exist anymore.";
                        return View(model);
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "Sorry! Please try again later.";
                    return View(model);
                }
                return GetUserAuthentication(model);
            }
            else
            {
                ViewBag.Message = "Please enter your Username / Password";
                // If we got this far, something failed, redisplay form
                return View(model);
            }
        }
        public ActionResult LoginPopUp(int id = 0)
        {
            switch (id)
            {
                case (int)ArtistWala.Models.AppsConstants.AuthenticationError.AccountExists:
                    ViewBag.Message = "This user already register with us. Please login with your username and password.";
                    break;
                case (int)ArtistWala.Models.AppsConstants.AuthenticationError.InvalidUser:
                    ViewBag.Message = "Sorry! you are is registered with us.";
                    break;
                case (int)ArtistWala.Models.AppsConstants.AuthenticationError.EmailSend:
                    ViewBag.Message = "An email has been send to you email id with your password.";
                    break;
                case (int)ArtistWala.Models.AppsConstants.AuthenticationError.ServerError:
                    ViewBag.Message = "Sorry! Please try again later.";
                    break;
                default:
                    break;
            }

            return View();
        }

        [HttpPost]
        public ActionResult LoginPopUp(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    String userName = model.UserName;
                    String passward = objEncrp.EncryptPassword(model.Password);
                    bool rememberUserName = model.IsRemember;
                    tblUser UserTbl = AuthenticateUser(userName, passward);
                    if (UserTbl != null)
                    {
                        model.Password = null;
                        model.Name = UserTbl.Name;
                        model.UserId = UserTbl.UserId;
                        model.ProfileImage = UserTbl.ProfileImage;
                        if (UserTbl.tblArtists.Any(x => x.FK_UserId == UserTbl.UserId))
                        {
                            model.ArtistId = UserTbl.tblArtists.Where(x => x.FK_UserId == UserTbl.UserId).FirstOrDefault().ArtistId;
                            model.IsArtist = true;

                        }
                        model.IsPromoKit = (UserTbl.IsPromoKit ?? false);
                        model.RoleId = UserTbl.FK_RoleId;

                        Session["LogedUserID"] = model.UserId;
                        Session["LogedUserName"] = model.Name;
                    }
                    else
                    {
                        // If we got this far, something failed, redisplay form
                        ViewBag.Message = "Username / Password is incorrect";
                        return View(model);
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "Sorry! Please try again later.";
                    return View(model);
                }
                return GetUserAuthentication(model);
            }
            else
            {
                ViewBag.Message = "Please enter your Username / Password";
                // If we got this far, something failed, redisplay form
                return View(model);
            }
        }

        public ActionResult JoinUs()
        {
            return View();
        }

        public ActionResult QuickQuote(QuotesModel objQuotes, string profile)
        {
            profile = HttpUtility.UrlDecode(profile);

            var EventTypeResult = context.tblStaticContents.Where(w => w.EventType != "" && w.EventType != null).ToList();
            foreach (var item in EventTypeResult)
            {
                objQuotes.EventsType.Add(new SelectListItem { Text = item.EventType, Value = item.StaticContentId.ToString() });
            }
            var EventTime = context.tblStaticContents.Where(w => w.EventTime != "" && w.EventTime != null).ToList();
            foreach (var item in EventTime)
            {
                objQuotes.EventTime.Add(new SelectListItem { Text = item.EventTime, Value = item.StaticContentId.ToString() });
            }
            var EvntDuration = context.tblStaticContents.Where(w => w.EventDuration != "" && w.EventDuration != null).ToList();
            foreach (var item in EvntDuration)
            {
                objQuotes.EvntDuration.Add(new SelectListItem { Text = item.EventDuration, Value = item.StaticContentId.ToString() });
            }
            objQuotes.EventsTypes = context.tblStaticContents.Where(w => w.EventType != "" && w.EventType != null).ToList();
            objQuotes.EventDate = System.DateTime.UtcNow;

            return View(objQuotes);
        }

        [HttpPost]
        public ActionResult QuickQuote(QuotesModel objQuotes, RegistrationModel regsiter, FormCollection fc, string profile, bool IsSendFreeQuote = false)
        {
            SMSCAPI oSMS = new SMSCAPI();
            string idName = string.Empty;

            profile = HttpUtility.UrlDecode(profile);
            int id = context.tblUsers.Where(x => x.UserName == profile).Select(s => s.UserId).FirstOrDefault();
            idName = idName + context.tblUsers.Where(x => x.UserId == id).Select(s => s.Name).FirstOrDefault();
            var idDetails = context.tblUsers.Where(x => x.UserId == id).Select(s => s).FirstOrDefault();
            DateTime eventDate = DateTime.Parse(objQuotes.EventDate.ToString(), CultureInfo.InvariantCulture);

            if (profile != "")
            {
                try
                {
                    var loggeduserId = Convert.ToInt32(Session["LogedUserID"].ToString());
                    int clientid = context.tblClients.Where(x => x.FK_UserId == loggeduserId).Select(s => s.ClientId).FirstOrDefault();
                    // save data in quote table
                    tblQuote objQuote = new tblQuote()
                    {
                        AddedDate = DateTime.Now,
                        City = objQuotes.City,
                        EventDate = eventDate,
                        EventDuration = objQuotes.DurationText,
                        EventStartTime = objQuotes.StartTimeText,
                        IsEventDateKnown = objQuotes.IsEventDateKnown,
                        IsVenueKnown = objQuotes.IsVenueKnown,
                        NoOfGuest = objQuotes.NoOfGuest,
                        Venue = objQuotes.Venue,
                        FK_ClientId = clientid,
                        EventDetails = objQuotes.EventDetails,
                        FK_EventType = Convert.ToInt32(objQuotes.FK_EventType)
                    };
                    context.tblQuotes.Add(objQuote);
                    context.SaveChanges();

                    int QuoteId = objQuote.QuoteId;
                    // check if send quote to 5 artist
                    if (IsSendFreeQuote == true)
                    {
                        // related top 5 artist start----------------
                        tblArtist objArtisty = new tblArtist();
                        var User = context.tblUsers.Where(x => x.UserId == id && x.IsActive == true).FirstOrDefault();
                        if (User != null)
                            objArtisty = User.tblArtists.Where(x => x.FK_UserId == User.UserId).FirstOrDefault();

                        var q = (from s in context.tblArtistSubCategories
                                 join cm in context.tblArtistCategoryMappings on s.ArtistSubCategoryId equals cm.FK_SubCategoryId
                                 where cm.FK_ArtistId == objArtisty.ArtistId
                                 select s.SubCategoryName).ToList();
                        var r = (from x in context.tblArtists where x.ArtistId == objArtisty.ArtistId select x.ArtistId).ToList();

                        var result = (from sc in context.tblArtistSubCategories
                                      join cm in context.tblArtistCategoryMappings on sc.ArtistSubCategoryId equals cm.FK_SubCategoryId
                                      join a in context.tblArtists on cm.FK_ArtistId equals a.ArtistId
                                      join u in context.tblUsers on a.FK_UserId equals u.UserId
                                      where q.Contains(sc.SubCategoryName) && a.City == objArtisty.City && a.IsFeatured == true && !context.tblArtists.Any(z => (a.FK_UserId == id))
                                      group new { sc, a } by new
                                      {
                                          a.ArtistName,
                                          a.ArtistId,
                                          u.CreatedDate,
                                          u.UserId
                                      } into grp
                                      select new
                                      {
                                          grp.Key.ArtistName,
                                          grp.Key.ArtistId,
                                          grp.Key.CreatedDate,
                                          grp.Key.UserId
                                      }
                                   ).OrderBy(x => x.CreatedDate).Take(5).ToList();

                        // end -------------------------------
                        if (result.Count == 0)
                        {
                            result = (from sc in context.tblArtistSubCategories
                                      join cm in context.tblArtistCategoryMappings on sc.ArtistSubCategoryId equals cm.FK_SubCategoryId
                                      join a in context.tblArtists on cm.FK_ArtistId equals a.ArtistId
                                      join u in context.tblUsers on a.FK_UserId equals u.UserId
                                      where q.Contains(sc.SubCategoryName) && a.City == objArtisty.City && !context.tblArtists.Any(z => (a.FK_UserId == id))
                                      group new { sc, a } by new
                                      {
                                          a.ArtistName,
                                          a.ArtistId,
                                          u.CreatedDate,
                                          u.UserId
                                      } into grp
                                      select new
                                      {
                                          grp.Key.ArtistName,
                                          grp.Key.ArtistId,
                                          grp.Key.CreatedDate,
                                          grp.Key.UserId
                                      }
                                   ).OrderBy(x => x.CreatedDate).Take(5).ToList();
                        }

                        // send message to related 5 related artist                        
                        foreach (var item in result)
                        {
                            tblUserMessage ssMessages = new tblUserMessage()
                            {
                                SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                                Fk_UserId = item.UserId,
                                MessageBody = "Hello : You have a new Quick quote request. Event City : " + objQuotes.City + ", Event Type : " + objQuotes.TypeText + ", Event Date : " + eventDate.ToString("dd/MM/yyyy") + ", Event Duration : " + objQuotes.DurationText + ", Details : " + objQuotes.EventDetails,
                                MessageType = "Contact",
                                MessageSubject = "Quick Quote",
                                IsUnread = true,
                                MessageDate = System.DateTime.Now,
                                MessageParentId = null,
                                Fk_QuoteId = QuoteId
                            };
                            context.tblUserMessages.Add(ssMessages);
                            context.SaveChanges();

                            var MobileNoTo = context.tblArtists.Where(s => s.FK_UserId == item.UserId).Select(s => s).FirstOrDefault();
                            tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 11).FirstOrDefault();
                            string MessageBodyTo = SMSTo.SMSBody.Replace("@Name", MobileNoTo.ArtistName);
                            oSMS.SendSMS(MobileNoTo.Mobile.ToString(), MessageBodyTo);
                            tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 8).FirstOrDefault();
                            string emailBodyTo = emailTo.EmailBody.Replace("@Name", MobileNoTo.ArtistName);
                            objCommon.sendMailInHtmlFormat(emailTo.AddCC, MobileNoTo.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);

                        }
                        ModelState.Clear();
                        ViewBag.Message = "Your query has been successsfully send.";

                        if (idDetails.Notification == true)
                        {
                            // send message to selected artist
                            tblUserMessage sMessages = new tblUserMessage()
                            {
                                SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                                Fk_UserId = id,
                                MessageBody = "Hello : You have a new Quick quote request. Event City : " + objQuotes.City + ", Event Type : " + objQuotes.TypeText + ", Event Date : " + eventDate.ToString("dd/MM/yyyy") + ", Event Duration : " + objQuotes.DurationText + ", Details : " + objQuotes.EventDetails,
                                MessageType = "Contact",
                                MessageSubject = "Quick Quote",
                                IsUnread = true,
                                MessageDate = System.DateTime.Now,
                                MessageParentId = null,
                                Fk_QuoteId = QuoteId
                            };
                            context.tblUserMessages.Add(sMessages);
                            context.SaveChanges();

                            var MobileNoTo = context.tblArtists.Where(s => s.FK_UserId == id).Select(s => s).FirstOrDefault();
                            tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 11).FirstOrDefault();
                            string MessageBodyTo = SMSTo.SMSBody.Replace("@Name", MobileNoTo.ArtistName);
                            oSMS.SendSMS(MobileNoTo.Mobile.ToString(), MessageBodyTo);
                            tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 8).FirstOrDefault();
                            string emailBodyTo = emailTo.EmailBody.Replace("@Name", MobileNoTo.ArtistName);
                            objCommon.sendMailInHtmlFormat(emailTo.AddCC, MobileNoTo.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);

                        }
                        else
                        {
                            // send message to selected artist
                            tblUserMessage sMessages = new tblUserMessage()
                            {
                                SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                                Fk_UserId = id,
                                MessageBody = "Hello : You have a new Quick quote request. Event City : " + objQuotes.City + ", Event Type : " + objQuotes.TypeText + ", Event Date : " + eventDate.ToString("dd/MM/yyyy") + ", Event Duration : " + objQuotes.DurationText + ", Details : " + objQuotes.EventDetails,
                                MessageType = "Contact",
                                MessageSubject = "Quick Quote",
                                IsUnread = true,
                                MessageDate = System.DateTime.Now,
                                MessageParentId = null,
                                Fk_QuoteId = QuoteId
                            };
                            context.tblUserMessages.Add(sMessages);
                            context.SaveChanges();
                        }

                        //send moderator and admin
                        var ToUserID = (from m in context.tblUsers where m.FK_RoleId == 1 || m.FK_RoleId == 2 select new { m.UserId }).ToList();
                        foreach (var item in ToUserID)
                        {
                            tblUserMessage ssMessages = new tblUserMessage()
                            {
                                SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                                Fk_UserId = item.UserId,
                                MessageBody = idName + " received a new Quick quote request. Event City : " + objQuotes.City + ", Event Type : " + objQuotes.TypeText + ", Event Date : " + eventDate.ToString("dd/MM/yyyy") + ", Event Duration : " + objQuotes.DurationText + ", Details : " + objQuotes.EventDetails,
                                MessageType = "Contact",
                                MessageSubject = "Quick Quote",
                                IsUnread = true,
                                MessageDate = System.DateTime.Now,
                                MessageParentId = null
                            };
                            context.tblUserMessages.Add(ssMessages);
                            context.SaveChanges();
                        }
                        ModelState.Clear();
                        ViewBag.Message = "Your query has been successsfully send.";
                    }
                    else
                    {
                        if (idDetails.Notification == true)
                        {
                            // send message to selected artist
                            tblUserMessage sMessages = new tblUserMessage()
                            {
                                SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                                Fk_UserId = id,
                                MessageBody = "Hello : You have a new Quick quote request. Event City : " + objQuotes.City + ", Event Type : " + objQuotes.TypeText + ", Event Date : " + eventDate.ToString("dd/MM/yyyy") + ", Event Duration : " + objQuotes.DurationText + ", Details : " + objQuotes.EventDetails,
                                MessageType = "Contact",
                                MessageSubject = "Quick Quote",
                                IsUnread = true,
                                MessageDate = System.DateTime.Now,
                                MessageParentId = null,
                                Fk_QuoteId = QuoteId
                            };
                            context.tblUserMessages.Add(sMessages);
                            context.SaveChanges();

                            var MobileNoTo = context.tblArtists.Where(s => s.FK_UserId == id).Select(s => s).FirstOrDefault();
                            tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 11).FirstOrDefault();
                            string MessageBodyTo = SMSTo.SMSBody.Replace("@Name", MobileNoTo.ArtistName);
                            oSMS.SendSMS(MobileNoTo.Mobile.ToString(), MessageBodyTo);
                            tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 8).FirstOrDefault();
                            string emailBodyTo = emailTo.EmailBody.Replace("@Name", MobileNoTo.ArtistName);
                            objCommon.sendMailInHtmlFormat(emailTo.AddCC, MobileNoTo.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);
                        }
                        else
                        {
                            // send message to selected artist
                            tblUserMessage sMessages = new tblUserMessage()
                            {
                                SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                                Fk_UserId = id,
                                MessageBody = "Hello : You have a new Quick quote request. Event City : " + objQuotes.City + ", Event Type : " + objQuotes.TypeText + ", Event Date : " + eventDate.ToString("dd/MM/yyyy") + ", Event Duration : " + objQuotes.DurationText + ", Details : " + objQuotes.EventDetails,
                                MessageType = "Contact",
                                MessageSubject = "Quick Quote",
                                IsUnread = true,
                                MessageDate = System.DateTime.Now,
                                MessageParentId = null,
                                Fk_QuoteId = QuoteId
                            };
                            context.tblUserMessages.Add(sMessages);
                            context.SaveChanges();
                        }

                        //send moderator and admin
                        var ToUserID = (from m in context.tblUsers where m.FK_RoleId == 1 || m.FK_RoleId == 2 select new { m.UserId }).ToList();
                        foreach (var item in ToUserID)
                        {
                            tblUserMessage ssMessages = new tblUserMessage()
                            {
                                SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                                Fk_UserId = item.UserId,
                                MessageBody = idName + " received a new Quick quote request. Event City : " + objQuotes.City + ", Event Type : " + objQuotes.TypeText + ", Event Date : " + eventDate.ToString("dd/MM/yyyy") + ", Event Duration : " + objQuotes.DurationText + ", Details : " + objQuotes.EventDetails,
                                MessageType = "Contact",
                                MessageSubject = "Quick Quote",
                                IsUnread = true,
                                MessageDate = System.DateTime.Now,
                                MessageParentId = null
                            };
                            context.tblUserMessages.Add(ssMessages);
                            context.SaveChanges();
                        }
                        ModelState.Clear();
                        ViewBag.Message = "Your query has been successsfully send.";
                    }
                    //send message to client
                    var MobileNofrom = context.tblClients.Where(s => s.FK_UserId == loggeduserId).Select(s => s).FirstOrDefault();
                    tblSMSTemplate SMSFrom = context.tblSMSTemplates.Where(x => x.TemplateId == 12).FirstOrDefault();
                    string MessageBodyFrom = SMSFrom.SMSBody.Replace("@Name", MobileNofrom.ClientName);
                    oSMS.SendSMS(MobileNofrom.ContactNo.ToString(), MessageBodyFrom);
                    tblEmailTemplate emailFrom = context.tblEmailTemplates.Where(x => x.TemplateId == 9).FirstOrDefault();
                    string emailBodyFrom = emailFrom.EmailBody.Replace("@Name", MobileNofrom.ClientName);
                    objCommon.sendMailInHtmlFormat(emailFrom.AddCC, MobileNofrom.Email, emailFrom.AddBCC, emailFrom.EmailSubject, emailBodyFrom);

                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = "Sorry! Please try again later.";
                }
            }
            else
            {
                try
                {
                    var loggeduserId = Convert.ToInt32(Session["LogedUserID"].ToString());
                    int clientid = context.tblClients.Where(x => x.FK_UserId == loggeduserId).Select(s => s.ClientId).FirstOrDefault();
                    // save data in quote table
                    tblQuote objQuote = new tblQuote()
                    {
                        AddedDate = DateTime.Now,
                        City = objQuotes.City,
                        EventDate = eventDate,
                        EventDuration = objQuotes.DurationText,
                        EventStartTime = objQuotes.StartTimeText,
                        IsEventDateKnown = objQuotes.IsEventDateKnown,
                        IsVenueKnown = objQuotes.IsVenueKnown,
                        NoOfGuest = objQuotes.NoOfGuest,
                        Venue = objQuotes.Venue,
                        FK_ClientId = clientid,
                        EventDetails = objQuotes.EventDetails,
                        FK_EventType = Convert.ToInt32(objQuotes.FK_EventType)
                    };
                    context.tblQuotes.Add(objQuote);
                    context.SaveChanges();

                    int QuoteId = objQuote.QuoteId;

                    // have to add top 10 artist message with mail and text message

                    var topArtist = (from a in context.tblArtists
                                     join cm in context.tblArtistCategoryMappings on a.ArtistId equals cm.FK_ArtistId
                                     join sc in context.tblArtistSubCategories on cm.FK_SubCategoryId equals sc.ArtistSubCategoryId
                                     join u in context.tblUsers on a.FK_UserId equals u.UserId
                                     where sc.SubCategoryName == objQuotes.SearchText1 && a.City == objQuotes.Location1 && a.IsFeatured == true
                                     select new
                                     {
                                         a.FK_UserId,
                                         a.ArtistId,
                                         a.ArtistName,
                                         u.UserName,
                                         sc.SubCategoryName,
                                         a.City,
                                         u.CreatedDate,
                                         a.IsFeatured

                                     }).OrderBy(x => x.CreatedDate).Take(10).ToList();

                    if (topArtist.Count == 0)
                    {
                        topArtist = (from a in context.tblArtists
                                     join cm in context.tblArtistCategoryMappings on a.ArtistId equals cm.FK_ArtistId
                                     join sc in context.tblArtistSubCategories on cm.FK_SubCategoryId equals sc.ArtistSubCategoryId
                                     join u in context.tblUsers on a.FK_UserId equals u.UserId
                                     where sc.SubCategoryName == objQuotes.SearchText1 && a.City == objQuotes.Location1
                                     select new
                                     {
                                         a.FK_UserId,
                                         a.ArtistId,
                                         a.ArtistName,
                                         u.UserName,
                                         sc.SubCategoryName,
                                         a.City,
                                         u.CreatedDate,
                                         a.IsFeatured

                                     }).OrderBy(x => x.CreatedDate).Take(10).ToList();
                    }

                    // send message to related 10 related artist                        
                    foreach (var item in topArtist)
                    {
                        tblUserMessage ssMessages = new tblUserMessage()
                        {
                            SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                            Fk_UserId = item.FK_UserId,
                            MessageBody = "Hello : You have a new Quick quote request. Event City : " + objQuotes.City + ", Event Type : " + objQuotes.TypeText + ", Event Date : " + eventDate.ToString("dd/MM/yyyy") + ", Event Duration : " + objQuotes.DurationText + ", Details : " + objQuotes.EventDetails,
                            MessageType = "Contact",
                            MessageSubject = "Quick Quote",
                            IsUnread = true,
                            MessageDate = System.DateTime.Now,
                            MessageParentId = null,
                            Fk_QuoteId = QuoteId
                        };
                        context.tblUserMessages.Add(ssMessages);
                        context.SaveChanges();

                        var MobileNoTo = context.tblArtists.Where(s => s.FK_UserId == item.FK_UserId).Select(s => s).FirstOrDefault();
                        tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 11).FirstOrDefault();
                        string MessageBodyTo = SMSTo.SMSBody.Replace("@Name", MobileNoTo.ArtistName);
                        oSMS.SendSMS(MobileNoTo.Mobile.ToString(), MessageBodyTo);
                        tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 8).FirstOrDefault();
                        string emailBodyTo = emailTo.EmailBody.Replace("@Name", MobileNoTo.ArtistName);
                        objCommon.sendMailInHtmlFormat(emailTo.AddCC, MobileNoTo.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);
                    }
                    ModelState.Clear();
                    ViewBag.Message = "Your query has been successsfully sent.";

                    //send moderator and admin
                    var ToUserID = (from m in context.tblUsers where m.FK_RoleId == 1 || m.FK_RoleId == 2 select new { m.UserId }).ToList();
                    foreach (var item in ToUserID)
                    {
                        tblUserMessage ssMessages = new tblUserMessage()
                        {
                            SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                            Fk_UserId = item.UserId,
                            MessageBody = "Hello : We have a new Quick quote request. For Search Result, Category : " + objQuotes.SearchText1 + " and Location : " + objQuotes.Location1 + " . Event City : " + objQuotes.City + ", Event Type : " + objQuotes.TypeText + ", Event Date : " + eventDate.ToString("dd/MM/yyyy") + ", Event Duration : " + objQuotes.DurationText + ", Details : " + objQuotes.EventDetails,
                            MessageType = "Contact",
                            MessageSubject = "Quick Quote",
                            IsUnread = true,
                            MessageDate = System.DateTime.Now,
                            MessageParentId = null,

                        };
                        context.tblUserMessages.Add(ssMessages);
                        context.SaveChanges();
                    }
                    ModelState.Clear();
                    ViewBag.Message = "Your query has been successsfully sent.";

                    //send message to client
                    var MobileNofrom = context.tblClients.Where(s => s.FK_UserId == loggeduserId).Select(s => s).FirstOrDefault();
                    tblSMSTemplate SMSFrom = context.tblSMSTemplates.Where(x => x.TemplateId == 12).FirstOrDefault();
                    string MessageBodyFrom = SMSFrom.SMSBody.Replace("@Name", MobileNofrom.ClientName);
                    oSMS.SendSMS(MobileNofrom.ContactNo.ToString(), MessageBodyFrom);
                    tblEmailTemplate emailFrom = context.tblEmailTemplates.Where(x => x.TemplateId == 9).FirstOrDefault();
                    string emailBodyFrom = emailFrom.EmailBody.Replace("@Name", MobileNofrom.ClientName);
                    objCommon.sendMailInHtmlFormat(emailFrom.AddCC, MobileNofrom.Email, emailFrom.AddBCC, emailFrom.EmailSubject, emailBodyFrom);

                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = "Sorry! Please try again later.";
                }
            }
            objQuotes.EventsTypes = context.tblStaticContents.Where(w => w.EventType != "" && w.EventType != null).ToList();
            return View(objQuotes);
        }

        public ActionResult QuickArtist()
        {
            string dd = "EventType";
            var blogs = context.Database.SqlQuery<string>("SELECT [" + dd + "] FROM dbo.tblStaticContent where [" + dd + "]!='' ").ToList();

            HomeModel home = GetAllHomeData();
            return View(home);
        }

        protected HomeModel GetAllHomeData()
        {
            try
            {
                HomeModel home = new HomeModel();
                home.CartgoryType = context.tblArtistCategoryTypes.ToList();

                return home;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public ActionResult SearchResults(string SearchText, string Types, string SearchId, string Location)
        {
            if (Location == null)
            {
                if (Request.Cookies["ArtistwalaLocation"] != null)
                {
                    Location = HttpUtility.UrlDecode(Request.Cookies["ArtistwalaLocation"].Value);
                }
            }
            try
            {
                List<tblArtist> artistList = new List<tblArtist>();
                List<tblArtist> LikeartistList = new List<tblArtist>();
                SearchText = HttpUtility.UrlDecode(SearchText);

                if (Location == null)
                {
                    artistList = context.tblArtists.Where(x => x.ArtistName.Contains(SearchText) && x.IsActive == true).ToList();
                }
                else
                {
                    artistList = context.tblArtists.Where(x => x.City.Contains(Location.Trim()) && x.ArtistName.Contains(SearchText) && x.IsActive == true).OrderByDescending(x => x.AddedOn).OrderByDescending(x => x.IsFeatured).ToList();
                }
                if (artistList.Count == 0)
                {
                    if (Types != null)
                    {
                        int TypesId = Convert.ToInt32(Types.ToString());
                        if (Location == null)
                        {
                            artistList = context.tblArtists.Where(x =>
                                                              (x.tblArtistCategoryMappings.Where(v => v.tblArtistSubCategory.SubCategoryName.StartsWith(SearchText) && x.IsActive == true).Select(p => p.tblArtistSubCategory.SubCategoryName).FirstOrDefault().StartsWith(SearchText))).ToList();
                        }

                        else
                        {

                            var q = (from tm in context.tblArtistCategoryMappings
                                     join tc in context.tblArtistCategories on tm.FK_CategoryId equals tc.ArtistCategoryId
                                     join tct in context.tblArtistCategoryTypes on tc.FK_ArtistCategoryTypeId equals tct.ArtistCategoryTypeId
                                     where tct.ArtistCategoryTypeId == TypesId
                                     select tm.FK_ArtistId).Distinct().ToList();

                            artistList = (from ta in context.tblArtists
                                          where q.Contains(ta.ArtistId) && ta.City.Contains(Location) && ta.IsActive == true
                                          select ta).OrderByDescending(ta => ta.AddedOn).OrderByDescending(ta => ta.IsFeatured).ToList();

                        }
                    }
                    else
                    {
                        var subCateId = context.tblArtistSubCategories.Where(x => x.SubCategoryName == SearchText).Select(s => s.ArtistSubCategoryId).FirstOrDefault();


                        if (Location == null)
                        {
                            artistList = context.tblArtists.Where(x =>
                                  (x.tblArtistCategoryMappings.Where(v => v.tblArtistSubCategory.SubCategoryName.StartsWith(SearchText) && x.IsActive == true).Select(p => p.tblArtistSubCategory.SubCategoryName).FirstOrDefault().StartsWith(SearchText))).ToList();
                        }
                        else
                        {
                            artistList = context.tblArtists.Where(x => x.City.Contains(Location.Trim())
                           && (x.tblArtistCategoryMappings.Where(v => v.tblArtistSubCategory.SubCategoryName.StartsWith(SearchText)).Select(p => p.tblArtistSubCategory.SubCategoryName).FirstOrDefault().StartsWith(SearchText)) && x.IsActive == true).OrderByDescending(x => x.AddedOn).OrderByDescending(x => x.IsFeatured).ToList();
                        }

                    }


                }

                SearchModel objSearch = new SearchModel()
                {
                    ArtistList = artistList,

                    CartgoryType = context.tblArtistCategoryTypes.ToList()
                };

                return View(objSearch);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult SearchResults(FormCollection fc)
        {
            try
            {
                string SearchText = Convert.ToString(fc["txtSearchText"]);
                string SearchLocation = Convert.ToString(fc["txtArea"]);

                SearchText = HttpUtility.UrlDecode(SearchText);
                List<tblArtist> artistList = new List<tblArtist>();
                if (!string.IsNullOrEmpty(SearchText) && string.IsNullOrEmpty(SearchLocation))
                {
                    artistList = context.tblArtists.Where(x => x.IsActive == true &&
                        (x.ArtistName.ToLower().Contains(SearchText) ||
                        (x.tblArtistCategoryMappings.Where(c => c.tblArtistSubCategory.SubCategoryName.ToLower().Contains(SearchText) && c.tblArtistSubCategory.IsActive == true).FirstOrDefault().tblArtistSubCategory.SubCategoryName.ToLower().Contains(SearchText)) ||
                        (x.tblArtistCategoryMappings.Where(c => c.tblArtistSubCategory.tblArtistCategory.CategoryName.ToLower().Contains(SearchText) && c.tblArtistSubCategory.tblArtistCategory.IsActive == true).FirstOrDefault().tblArtistSubCategory.tblArtistCategory.CategoryName.ToLower().Contains(SearchText))
                        )).ToList();
                }
                else if (string.IsNullOrEmpty(SearchText) && !string.IsNullOrEmpty(SearchLocation))
                {
                    artistList = context.tblArtists.Where(x => x.IsActive == true &&
                           (x.City.ToLower().Contains(SearchLocation) ||
                           x.State.ToLower().Contains(SearchLocation) ||
                           x.Country.ToLower().Contains(SearchLocation))).ToList();
                }
                else if (!string.IsNullOrEmpty(SearchText) && !string.IsNullOrEmpty(SearchLocation))
                {
                    artistList = context.tblArtists.Where(x => x.IsActive == true &&
                           ((x.City.ToLower().Contains(SearchLocation) ||
                           x.State.ToLower().Contains(SearchLocation) ||
                           x.Country.ToLower().Contains(SearchLocation)) &&
                           (x.ArtistName.ToLower().Contains(SearchText) || (x.tblArtistCategoryMappings.Where(c => c.tblArtistSubCategory.SubCategoryName.ToLower().Contains(SearchText) && c.tblArtistSubCategory.IsActive == true).FirstOrDefault().tblArtistSubCategory.SubCategoryName.ToLower().Contains(SearchText)) ||
                           (x.tblArtistCategoryMappings.Where(c => c.tblArtistSubCategory.tblArtistCategory.CategoryName.ToLower().Contains(SearchText) && c.tblArtistSubCategory.tblArtistCategory.IsActive == true).FirstOrDefault().tblArtistSubCategory.tblArtistCategory.CategoryName.ToLower().Contains(SearchText))
                           ))).ToList();
                }

                SearchModel objSearch = new SearchModel()
                {
                    ArtistList = artistList,
                    CartgoryType = context.tblArtistCategoryTypes.ToList()
                };

                return View(objSearch);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public ActionResult SearchCategory(string SearchName)
        {


            int CategoryId = 0;
            List<CategoryModel> oCategoryModel = new List<CategoryModel>();
            if (!string.IsNullOrEmpty(SearchName))
            {
                if (SearchName == "MusicalActs")
                {
                    CategoryId = 1;
                }
                else if (SearchName == "Entertainers")
                {
                    CategoryId = 2;
                }
                else if (SearchName == "Speakers")
                {
                    CategoryId = 3;
                }
                else if (SearchName == "Services")
                {
                    CategoryId = 4;
                }
                oCategoryModel = context.tblArtistCategories.Where(x => x.FK_ArtistCategoryTypeId == CategoryId).Select(s => new CategoryModel
                {
                    CategoryId = s.ArtistCategoryId,
                    CategoryName = s.CategoryName,
                    Subcategorylist = s.tblArtistSubCategories.Where(w => w.FK_ArtistCategoryId == s.ArtistCategoryId).Select(v => new SubCategoryModel
                    {
                        SubCategoryId = v.ArtistSubCategoryId,
                        SubCategoryName = v.SubCategoryName,
                    }).ToList(),
                }).ToList();
            }
            else
            {
                oCategoryModel = context.tblArtistCategories.Select(s => new CategoryModel
                {
                    CategoryId = s.ArtistCategoryId,
                    CategoryName = s.CategoryName,
                    Subcategorylist = s.tblArtistSubCategories.Select(v => new SubCategoryModel
                    {
                        SubCategoryId = v.ArtistSubCategoryId,
                        SubCategoryName = v.SubCategoryName,
                    }).ToList(),
                }).ToList();
            }

            return View(oCategoryModel);
        }
        public ActionResult ArtistDetails(string profile)
        {
            profile = HttpUtility.UrlDecode(profile);
            tblArtist objArtisty = new tblArtist();
            var User = context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == profile.Trim().ToUpper() && x.IsActive == true).FirstOrDefault();
            if (User != null)
                objArtisty = User.tblArtists.Where(x => x.FK_UserId == User.UserId).FirstOrDefault();
            Session["ToSendUSerID"] = objArtisty.FK_UserId;
            return View(objArtisty);
        }

        public ActionResult fetchReview(string profile)
        {
            profile = HttpUtility.UrlDecode(profile);
            var User = context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == profile.Trim().ToUpper() && x.IsActive == true).FirstOrDefault();
            var ArtistID = context.tblArtists.Where(x => x.FK_UserId == User.UserId).Select(s => s.ArtistId).FirstOrDefault();
            List<ArtistRating> ReviewList = context.tblArtistRatings.Where(x => x.FK_ArtistId == ArtistID && x.IsRated == true && x.Rating != null).Select(s => new ArtistRating
            {
                ArtistRatingId = s.ArtistRatingId,
                FK_ArtistId = s.FK_ArtistId,
                UserId = s.UserId,
                Rating = s.Rating,
                Comment = s.Comment,
                ClientName = context.tblClients.Where(x => x.FK_UserId == s.UserId).Select(i => i.ClientName).FirstOrDefault(),
                ProfileImage = context.tblUsers.Where(x => x.UserId == s.UserId).Select(i => i.ProfileImage).FirstOrDefault(),
            }).ToList();
            return View(ReviewList);
        }

        //public ActionResult ddlCategory(string profile)
        //{
        //    profile = HttpUtility.UrlDecode(profile);
        //    tblArtist objArtisty = new tblArtist();
        //    var User = context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == profile.Trim().ToUpper() && x.IsActive == true).FirstOrDefault();
        //    if (User != null)
        //        objArtisty = User.tblArtists.Where(x => x.tblArtistCategoryMappings == User.UserId).FirstOrDefault();

        //    return View(objArtisty);
        //}

        public ActionResult UserImage(string profile)
        {
            profile = HttpUtility.UrlDecode(profile);
            tblArtist objArtisty = new tblArtist();
            var User = context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == profile.Trim().ToUpper() && x.IsActive == true).FirstOrDefault();
            if (User != null)
                objArtisty = User.tblArtists.Where(x => x.FK_UserId == User.UserId).FirstOrDefault();

            return View(objArtisty);
        }

        public ActionResult ArtistSubCat(string profile)
        {
            profile = HttpUtility.UrlDecode(profile);
            tblArtist objArtisty = new tblArtist();
            var User = context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == profile.Trim().ToUpper() && x.IsActive == true).FirstOrDefault();
            if (User != null)
            {
                objArtisty = User.tblArtists.Where(x => x.FK_UserId == User.UserId).FirstOrDefault();
                Session["ABCD"] = User.UserId;
            }
            else { }
            return View(objArtisty);
        }

        public ActionResult CoupleContactClown()
        {
            return View();
        }

        [HttpPost]
        public ActionResult JoinUs(RegistrationModel regsiter)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!context.tblClients.Any(x => x.Email.Trim().ToUpper() == regsiter.Email.Trim().ToUpper()))
                    {
                        LoginModel model = SaveDetails(regsiter);
                        return GetUserAuthentication(model);
                    }
                    else
                    {
                        ViewBag.Message = "This user already register with us. Please login with your username and password.";
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        public ActionResult FbRegistration(FormCollection registerForm)
        {
            RegistrationModel objRegister = new RegistrationModel()
            {
                Name = registerForm["name"],
                Email = registerForm["Email"]
            };

            if (!context.tblClients.Any(x => x.Email.Trim().ToUpper() == objRegister.Email.Trim().ToUpper())
                && !context.tblArtists.Any(x => x.Email.Trim().ToUpper() == objRegister.Email.Trim().ToUpper()))
            {
                LoginModel model = SaveDetails(objRegister);
                return GetUserAuthentication(model);
            }
            else
            {
                return RedirectToAction("Login", "Users", new { id = (int)ArtistWala.Models.AppsConstants.AuthenticationError.AccountExists });
            }
        }

        public ActionResult LoginWithFb(FormCollection registerForm)
        {
            tblUser objUser = default(tblUser);
            RegistrationModel objRegister = new RegistrationModel()
            {
                Name = registerForm["name"],
                Email = registerForm["Email"]
            };

            var client = context.tblClients.Where(x => x.Email.Trim().ToUpper() == objRegister.Email.Trim().ToUpper()).FirstOrDefault();
            var artist = context.tblArtists.Where(x => x.Email.Trim().ToUpper() == objRegister.Email.Trim().ToUpper()).FirstOrDefault();

            if (client != null)
            {
                objUser = context.tblUsers.Where(x => x.IsActive == true && x.IsDelete == null && x.UserId == client.FK_UserId).FirstOrDefault();
            }
            if (artist != null)
            {
                objUser = context.tblUsers.Where(x => x.IsActive == true && x.IsDelete == null && x.UserId == artist.FK_UserId).FirstOrDefault();                
            }

            if (!string.IsNullOrEmpty(objRegister.Name) && objUser != null)
            {
                LoginModel model = new LoginModel()
                {
                   UserId = objUser.UserId,
                   Password = null,
                   Name = objUser.Name,
                   UserName = objUser.UserName,
                   preview = objUser.UserName,
                   RoleId = objUser.FK_RoleId,
                   ProfileImage = objUser.ProfileImage,
                   IsPromoKit = (objUser.IsPromoKit ?? false)                  
                    
                };
                if(artist!=null)
                {
                    model.ArtistId = artist.ArtistId;
                    model.IsArtist = true;
                }
                Session["LogedUserID"] = model.UserId;
                Session["LogedUserName"] = model.UserName;
                return GetUserAuthentication(model);
            }
            else
            {
                return FbRegistration(registerForm);
                //return RedirectToAction("Login", "Users", new { id = (int)ArtistWala.Models.AppsConstants.AuthenticationError.NotExixts });
            }
        }

        public ActionResult ForgotPassword(string Email)
        {
            try
            {
                int UserId = default(int);
                var client = context.tblClients.Where(x => x.Email.Trim().ToUpper() == Email.Trim().ToUpper()).FirstOrDefault();
                var artist = context.tblArtists.Where(x => x.Email.Trim().ToUpper() == Email.Trim().ToUpper()).FirstOrDefault();
                string emailTo = string.Empty;
                if (client != null)
                {
                    UserId = client.FK_UserId.Value;
                    emailTo = client.Email;
                }
                else if (artist != null)
                {
                    UserId = artist.FK_UserId.Value;
                    emailTo = artist.Email;
                }
                else
                {
                    return RedirectToAction("Login", "Users", new { id = (int)ArtistWala.Models.AppsConstants.AuthenticationError.InvalidUser });
                }
                SMSCAPI oSMS = new SMSCAPI();
                tblUser objUser = context.tblUsers.Where(x => x.UserId == UserId).FirstOrDefault();
                string UserName = objUser.UserName;
                string DecryptedPassword = objEncrp.DecryptPassword(objUser.Password);
                string MobileNo = objUser.tblClients.Where(s => s.FK_UserId == UserId).Select(s => s.ContactNo).FirstOrDefault();
                tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 9).FirstOrDefault();
                string MessageBody = SMSTo.SMSBody.Replace("@Name", UserName).Replace("@Password", DecryptedPassword);
                tblEmailTemplate email = context.tblEmailTemplates.Where(x => x.TemplateId == 3).FirstOrDefault();
                string emailBody = email.EmailBody.Replace("@Name", objUser.Name).Replace("@UserName", objUser.UserName).Replace("@Password", objEncrp.DecryptPassword(objUser.Password));
                oSMS.SendSMS(MobileNo, MessageBody);
                objCommon.sendMailInHtmlFormat(email.AddCC, emailTo, email.AddBCC, email.EmailSubject, emailBody);

            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Users", new { id = (int)ArtistWala.Models.AppsConstants.AuthenticationError.ServerError });
            }

            return RedirectToAction("Login", "Users", new { id = (int)ArtistWala.Models.AppsConstants.AuthenticationError.EmailSend });
        }

        #region Get Details

        private tblUser AuthenticateUser(String userName, String userPassword)
        {
            var result = context.tblUsers.Where(x => x.IsActive == true && x.IsDelete == null
                                            && (x.UserName.Trim().ToUpper() == userName.Trim().ToUpper() || x.tblClients.Where(w => w.Email == userName.Trim()).Select(r => r.Email).FirstOrDefault().Trim() == userName)
                                            && (x.Password.Trim().ToUpper() == userPassword.Trim().ToUpper())).FirstOrDefault();
            return result;
        }

        private ActionResult GetUserAuthentication(LoginModel model)
        {
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
            1, // Ticket version
            model.UserName,// Username to be associated with this ticket
            DateTime.Now, // Date/time ticket was issued
            DateTime.Now.AddHours(5), // Date and time the cookie will expire
            false, // if user has chcked rememebr me then create persistent cookie
            model.UserId.ToString(), // store the user data, in this case userId of the user
            FormsAuthentication.FormsCookiePath); // Cookie path specified in the web.config file in <Forms> tag if any. 
            // To give more security it is suggested to hash it
            string hashCookies = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hashCookies); // Hashed ticket 
            Session[SessionConstants.UserProfileObject] = model;
            // Add the cookie to the response, user browser 
            Response.Cookies.Add(cookie);
            string ReturnUrl = Convert.ToString(Session["ReturnUrl"]);
            if (ReturnUrl == "/Users/Login")
            {
                return RedirectToAction("Dashboard", "MyProfile");
            }
            else if (!string.IsNullOrEmpty(ReturnUrl))
            {
                //ReturnUrl = ReturnUrl.Replace("/", "");
                return Redirect(ReturnUrl);
                //return RedirectToAction("Dashboard", "MyProfile");
            }
            else
            {
                return RedirectToAction("Dashboard", "MyProfile");
            }
        }

        private LoginModel SaveDetails(RegistrationModel regsiter)
        {
            tblUser objUser = new tblUser()
            {
                UserName = objCommon.GetRandomNumber(9999999).ToString(),
                Password = objCommon.GetAutogeneratedPassword(),
                IsActive = true,
                FK_RoleId = (int)ArtistWala.Models.AppsConstants.UserRoles.Client,
                CreatedDate = DateTime.Now,
                Name = regsiter.Name,
                Notification = true
            };

            context.tblUsers.Add(objUser);
            context.SaveChanges();

            int UserId = objUser.UserId;

            tblClient objClient = new tblClient()
            {
                ClientName = regsiter.Name,
                Email = regsiter.Email,
                ContactNo = regsiter.Phone,
                FK_UserId = UserId,
                IsActive = true,
                CreatedDate = DateTime.Now
            };

            context.tblClients.Add(objClient);
            context.SaveChanges();

            tblUserMessage objUserMessage = new tblUserMessage()
            {
                Fk_UserId = UserId,
                MessageSubject = Messages.RegistrationWelcomeMessage,
                MessageBody = Messages.RegistrationWelcomeMessageBody,
                MessageType = "welcome",
                SendBy = 1,
                IsUnread = true
            };

            context.tblUserMessages.Add(objUserMessage);
            context.SaveChanges();

            LoginModel model = new LoginModel()
            {
                UserId = UserId,
                Password = null,
                Name = regsiter.Name,
                UserName = objUser.UserName,
                RoleId = objUser.FK_RoleId,
                IsArtist = false,
                IsPromoKit = false,
                ProfileImage = objUser.ProfileImage
            };
            AESEncryption oDecript = new AESEncryption();
            string DecriptedPassword = oDecript.DecryptPassword(objUser.Password);

            tblEmailTemplate email = context.tblEmailTemplates.Where(x => x.TemplateId == 1).FirstOrDefault();
            string emailBody = email.EmailBody.Replace("@Subject", objUserMessage.MessageSubject).Replace("@name", objUser.Name).Replace("@username", objUser.UserName).Replace("@password", DecriptedPassword);
            objCommon.sendMailInHtmlFormat(email.AddCC, regsiter.Email, email.AddBCC, email.EmailSubject, emailBody);

            //tblEmailTemplate email2 = context.tblEmailTemplates.Where(x => x.TemplateId == 2).FirstOrDefault();
            //string emailBody2 = email2.EmailBody.Replace("@name", objUser.Name).Replace("@username", objUser.UserName).Replace("@password", DecriptedPassword);
            //objCommon.sendMailInHtmlFormat(email2.AddCC, regsiter.Email, email2.AddBCC, email2.EmailSubject, emailBody2);

            return model;
        }
        private string OTPGenerator()
        {
            int lenthofpass = 6;
            string allowedChars = "";

            allowedChars += "1,2,3,4,5,6,7,8,9,0";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < lenthofpass; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            return passwordString;
        }

        #endregion


        // new code for send message
        #region JSONAJAX
        public JsonResult SendMessage(string MessageBody, string MessageSubject, string SendTo)
        {
            SMSCAPI oSMS = new SMSCAPI();
            int SndBy = Convert.ToInt32(Session["LogedUserID"].ToString());
            int id = Convert.ToInt32(Session["ToSendUSerID"].ToString());
            var idDetails = context.tblUsers.Where(x => x.UserId == id).Select(s => s).FirstOrDefault();

            if (SendTo == "Yes")
            {
                try
                {
                    if (idDetails.Notification == true)
                    {
                        tblUserMessage sMessages = new tblUserMessage()
                        {
                            SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                            Fk_UserId = id,
                            MessageBody = "Hello : You have a new contact request. Message : " + MessageBody,
                            MessageType = "Contact",
                            MessageSubject = MessageSubject,
                            IsUnread = true,
                            MessageDate = System.DateTime.Now,
                            MessageParentId = null
                        };
                        context.tblUserMessages.Add(sMessages);
                        context.SaveChanges();


                        var MobileNoTo = context.tblArtists.Where(s => s.FK_UserId == id).Select(s => s).FirstOrDefault();
                        tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 1).FirstOrDefault();
                        string MessageBodyTo = SMSTo.SMSBody.Replace("@Name", MobileNoTo.ArtistName);
                        oSMS.SendSMS(MobileNoTo.Mobile.ToString(), MessageBodyTo);
                        tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 6).FirstOrDefault();
                        string emailBodyTo = emailTo.EmailBody.Replace("@Name", MobileNoTo.ArtistName);
                        objCommon.sendMailInHtmlFormat(emailTo.AddCC, MobileNoTo.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);

                    }
                    else
                    {
                        tblUserMessage sMessages = new tblUserMessage()
                        {
                            SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                            Fk_UserId = id,
                            MessageBody = "Hello : You have a new contact request. Message : " + MessageBody,
                            MessageType = "Contact",
                            MessageSubject = MessageSubject,
                            IsUnread = true,
                            MessageDate = System.DateTime.Now,
                            MessageParentId = null
                        };
                        context.tblUserMessages.Add(sMessages);
                        context.SaveChanges();
                    }


                    var ToUserID = (from m in context.tblUsers where m.FK_RoleId == 1 || m.FK_RoleId == 2 select new { m.UserId }).ToList();
                    foreach (var item in ToUserID)
                    {
                        tblUserMessage ssMessages = new tblUserMessage()
                        {
                            SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                            Fk_UserId = item.UserId,
                            MessageBody = idDetails.Name + " , have recive a new contact request. Message : " + MessageBody,
                            MessageType = "Contact",
                            MessageSubject = MessageSubject,
                            IsUnread = true,
                            MessageDate = System.DateTime.Now,
                            MessageParentId = null
                        };
                        context.tblUserMessages.Add(ssMessages);
                        context.SaveChanges();
                    }
                    ViewBag.errorMessage = "Your mesage has been successfully sent. Please give us sometime to come back to you.";
                }
                catch (Exception ex)
                {
                    ViewBag.errorMessage = "Type your message with your contact details";
                    //return Json("");
                }

                var MobileNofrom = context.tblClients.Where(s => s.FK_UserId == SndBy).Select(s => s).FirstOrDefault();
                tblSMSTemplate SMSFrom = context.tblSMSTemplates.Where(x => x.TemplateId == 2).FirstOrDefault();
                string MessageBodyFrom = SMSFrom.SMSBody.Replace("@Name", MobileNofrom.ClientName);
                oSMS.SendSMS(MobileNofrom.ContactNo.ToString(), MessageBodyFrom);
                tblEmailTemplate emailFrom = context.tblEmailTemplates.Where(x => x.TemplateId == 7).FirstOrDefault();
                string emailBodyFrom = emailFrom.EmailBody.Replace("@Name", MobileNofrom.ClientName);
                objCommon.sendMailInHtmlFormat(emailFrom.AddCC, MobileNofrom.Email, emailFrom.AddBCC, emailFrom.EmailSubject, emailBodyFrom);

            }
            else
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        var ToUserID = (from m in context.tblUsers where m.FK_RoleId == 1 || m.FK_RoleId == 2 select new { m.UserId }).ToList();
                        foreach (var item in ToUserID)
                        {
                            tblUserMessage sMessages = new tblUserMessage()
                            {
                                SendBy = Convert.ToInt32(Session["LogedUserID"].ToString()),
                                Fk_UserId = item.UserId,
                                MessageBody = "Hello : we recive a contact request for " + idDetails.Name + " Message : " + MessageBody,
                                MessageType = "Contact",
                                MessageSubject = MessageSubject,
                                IsUnread = true,
                                MessageDate = System.DateTime.Now,
                                MessageParentId = null
                            };
                            context.tblUserMessages.Add(sMessages);
                            context.SaveChanges();
                        }
                        ViewBag.errorMessage = "Your mesage has been successfully sent. We will contact you shortly.";
                    }
                    else
                    {
                        ViewBag.errorMessage = "Type your message with your contact details";
                    }

                }
                catch (Exception ex)
                {
                    ViewBag.errorMessage = "Type your message with your contact details";
                    //return Json("");
                }

                var MobileNofrom = context.tblClients.Where(s => s.FK_UserId == SndBy).Select(s => s).FirstOrDefault();
                tblSMSTemplate SMSFrom = context.tblSMSTemplates.Where(x => x.TemplateId == 2).FirstOrDefault();
                string MessageBodyFrom = SMSFrom.SMSBody.Replace("@Name", MobileNofrom.ClientName);
                oSMS.SendSMS(MobileNofrom.ContactNo.ToString(), MessageBodyFrom);
                tblEmailTemplate emailFrom = context.tblEmailTemplates.Where(x => x.TemplateId == 7).FirstOrDefault();
                string emailBodyFrom = emailFrom.EmailBody.Replace("@Name", MobileNofrom.ClientName);
                objCommon.sendMailInHtmlFormat(emailFrom.AddCC, MobileNofrom.Email, emailFrom.AddBCC, emailFrom.EmailSubject, emailBodyFrom);
            }

            return Json("");
        }
        #endregion


        // new code for send Quote
        #region JSONAJAX
        public JsonResult FreeQuote(string SendTo, string QuoteMessage)
        {
            //string idName = string.Empty;
            int id = Convert.ToInt32(Session["ToSendUSerID"].ToString());
            var idDetails = context.tblUsers.Where(x => x.UserId == id).Select(s => s).FirstOrDefault();
            // related top 5 artist start
            tblArtist objArtisty = new tblArtist();
            var User = context.tblUsers.Where(x => x.UserId == id && x.IsActive == true).FirstOrDefault();
            if (User != null)
                objArtisty = User.tblArtists.Where(x => x.FK_UserId == User.UserId).FirstOrDefault();

            var q = (from s in context.tblArtistSubCategories
                     join cm in context.tblArtistCategoryMappings on s.ArtistSubCategoryId equals cm.FK_SubCategoryId
                     where cm.FK_ArtistId == objArtisty.ArtistId
                     select s.SubCategoryName).ToList();
            var r = (from x in context.tblArtists where x.ArtistId == objArtisty.ArtistId select x.ArtistId).ToList();

            var result = (from sc in context.tblArtistSubCategories
                          join cm in context.tblArtistCategoryMappings on sc.ArtistSubCategoryId equals cm.FK_SubCategoryId
                          join a in context.tblArtists on cm.FK_ArtistId equals a.ArtistId
                          join u in context.tblUsers on a.FK_UserId equals u.UserId
                          where q.Contains(sc.SubCategoryName) && !context.tblArtists.Any(z => (a.FK_UserId == id))
                          group new { sc, a } by new
                          {
                              a.ArtistName,
                              a.ArtistId,
                              u.CreatedDate,
                              u.UserId
                          } into grp
                          select new
                          {
                              grp.Key.ArtistName,
                              grp.Key.ArtistId,
                              grp.Key.CreatedDate,
                              grp.Key.UserId
                          }
                       ).OrderBy(x => x.CreatedDate).Take(5).ToList();

            // end -------------------------------

            if (SendTo == "Yes")
            {
                try
                {

                    if (idDetails.Notification == true)
                    {
                        tblUserMessage sMessages = new tblUserMessage()
                        {
                            SendBy = null,
                            Fk_UserId = id,
                            MessageBody = "Hello : You have a new Free quote request. Message : " + QuoteMessage,
                            MessageType = "Contact",
                            MessageSubject = "Free Quote",
                            IsUnread = true,
                            MessageDate = System.DateTime.Now,
                            MessageParentId = null
                        };
                        context.tblUserMessages.Add(sMessages);
                        context.SaveChanges();

                        SMSCAPI oSMS = new SMSCAPI();
                        var MobileNoTo = context.tblArtists.Where(s => s.FK_UserId == id).Select(s => s).FirstOrDefault();
                        string MessageBodyTo = "Dear " + MobileNoTo.ArtistName + ", you have a quotation request in your inbox, valid till 24 hours. Plz login and reply ASAP. Artistwala.com";
                        oSMS.SendSMS(MobileNoTo.Mobile.ToString(), MessageBodyTo);
                        tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 8).FirstOrDefault();
                        string emailBodyTo = emailTo.EmailBody.Replace("@Name", MobileNoTo.ArtistName);
                        objCommon.sendMailInHtmlFormat(emailTo.AddCC, MobileNoTo.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);

                    }
                    else
                    {
                        tblUserMessage sMessages = new tblUserMessage()
                        {
                            SendBy = null,
                            Fk_UserId = id,
                            MessageBody = "Hello : You have a new Free quote request. Message : " + QuoteMessage,
                            MessageType = "Contact",
                            MessageSubject = "Free Quote",
                            IsUnread = true,
                            MessageDate = System.DateTime.Now,
                            MessageParentId = null
                        };
                        context.tblUserMessages.Add(sMessages);
                        context.SaveChanges();
                    }

                    var ToUserID = (from m in context.tblUsers where m.FK_RoleId == 1 || m.FK_RoleId == 2 select new { m.UserId }).ToList();
                    foreach (var item in ToUserID)
                    {
                        tblUserMessage ssMessages = new tblUserMessage()
                        {
                            SendBy = null,
                            Fk_UserId = item.UserId,
                            MessageBody = idDetails.Name + " , have recive a new Free quote request. Message : " + QuoteMessage,
                            MessageType = "Contact",
                            MessageSubject = "Free Quote",
                            IsUnread = true,
                            MessageDate = System.DateTime.Now,
                            MessageParentId = null
                        };
                        context.tblUserMessages.Add(ssMessages);
                        context.SaveChanges();
                    }
                    ViewBag.errorMessage = "Your message has been successfully sent. Please give us sometime to come back to you.";
                }
                catch (Exception ex)
                {
                    ViewBag.errorMessage = "Something is wrong!";
                }
            }
            else
            {
                try
                {
                    if (idDetails.Notification == true)
                    {
                        tblUserMessage sMessages = new tblUserMessage()
                        {
                            SendBy = null,
                            Fk_UserId = id,
                            MessageBody = "Hello : You have a new Free quote request. Message : " + QuoteMessage,
                            MessageType = "Contact",
                            MessageSubject = "Free Quote",
                            IsUnread = true,
                            MessageDate = System.DateTime.Now,
                            MessageParentId = null
                        };
                        context.tblUserMessages.Add(sMessages);
                        context.SaveChanges();

                        SMSCAPI oSMS = new SMSCAPI();
                        var MobileNoTo = context.tblArtists.Where(s => s.FK_UserId == id).Select(s => s).FirstOrDefault();
                        string MessageBodyTo = "Dear " + MobileNoTo.ArtistName + ", you have a quotation request in your inbox, valid till 24 hours. Plz login and reply ASAP. Artistwala.com";
                        oSMS.SendSMS(MobileNoTo.Mobile.ToString(), MessageBodyTo);
                        tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 8).FirstOrDefault();
                        string emailBodyTo = emailTo.EmailBody.Replace("@Name", MobileNoTo.ArtistName);
                        objCommon.sendMailInHtmlFormat(emailTo.AddCC, MobileNoTo.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);
                    }
                    else
                    {
                        tblUserMessage sMessages = new tblUserMessage()
                        {
                            SendBy = null,
                            Fk_UserId = id,
                            MessageBody = "Hello : You have a new Free quote request. Message : " + QuoteMessage,
                            MessageType = "Contact",
                            MessageSubject = "Free Quote",
                            IsUnread = true,
                            MessageDate = System.DateTime.Now,
                            MessageParentId = null
                        };
                        context.tblUserMessages.Add(sMessages);
                        context.SaveChanges();
                    }

                    foreach (var item in result)
                    {
                        tblUserMessage ssMessages = new tblUserMessage()
                        {
                            SendBy = null,
                            Fk_UserId = item.UserId,
                            MessageBody = "Hello : You have a new Free quote request. Message : " + QuoteMessage,
                            MessageType = "Contact",
                            MessageSubject = "Free Quote",
                            IsUnread = true,
                            MessageDate = System.DateTime.Now,
                            MessageParentId = null
                        };
                        context.tblUserMessages.Add(ssMessages);
                        context.SaveChanges();

                        SMSCAPI oSMS = new SMSCAPI();
                        var MobileNoTo = context.tblArtists.Where(s => s.FK_UserId == item.UserId).Select(s => s).FirstOrDefault();
                        string MessageBodyTo = "Dear " + MobileNoTo.ArtistName + ", you have a quotation request in your inbox, valid till 24 hours. Plz login and reply ASAP. Artistwala.com";
                        oSMS.SendSMS(MobileNoTo.Mobile.ToString(), MessageBodyTo);
                        tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 8).FirstOrDefault();
                        string emailBodyTo = emailTo.EmailBody.Replace("@Name", MobileNoTo.ArtistName);
                        objCommon.sendMailInHtmlFormat(emailTo.AddCC, MobileNoTo.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);

                    }
                    ViewBag.errorMessage = "Your mesage has been successfully sent. We will contact you shortly.";
                }
                catch (Exception ex)
                {
                    ViewBag.errorMessage = "Something is wrong!";
                }
            }
            return Json("");
        }
        #endregion


        public ActionResult WriteARview(string profile)
        {
            profile = HttpUtility.UrlDecode(profile);
            var User = context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == profile.Trim().ToUpper() && x.IsActive == true).FirstOrDefault();
            var ArtistID = context.tblArtists.Where(x => x.FK_UserId == User.UserId).Select(s => s.ArtistId).FirstOrDefault();
            LoginModel objLogin = objCommon.GetLoginDetails();
            List<ArtistRating> allReview = context.tblArtistRatings.Where(x => x.FK_ArtistId == ArtistID && x.UserId == objLogin.UserId && x.IsRated == true && x.Rating != null).Select(s => new ArtistRating
            {
                ArtistRatingId = s.ArtistRatingId,
                FK_ArtistId = s.FK_ArtistId,
                UserId = s.UserId,
                Rating = s.Rating,
                Comment = s.Comment,
                //ImageList = context.tblClients.Where(x=>x.FK_UserId ==s.UserId).Select(s=>s).FirstOrDefault(),
            }).ToList();
            return View(allReview);
        }


        #region JSONAJAX
        public JsonResult UpdateReview(string ArtistRatingId, string Review, string Comment)
        {
            int ARId = Convert.ToInt32(ArtistRatingId);
            tblArtistRating Arevw = context.tblArtistRatings.Where(x => x.ArtistRatingId == ARId).Select(s => s).FirstOrDefault();
            string RVW = Review;
            //var MessageDetails = context.tblUserMessages.Where(x => x.MessageId == MessageId && x.Fk_QuoteId != null && x.Approved == null).Select(s => s).FirstOrDefault();
            if (RVW != null)
            {
                Arevw.Rating = Convert.ToInt32(RVW.ToString());
                Arevw.Comment = Comment.ToString();
                context.Entry(Arevw).State = EntityState.Modified;
                context.SaveChanges();
            }
            else
            {
                ViewBag.errorMessage = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion



        #region JSONAJAX
        public JsonResult SaveReview(string Review, string Comment)
        {
            SMSCAPI oSMS = new SMSCAPI();
            int SndBy = Convert.ToInt32(Session["LogedUserID"].ToString());
            int id = Convert.ToInt32(Session["ToSendUSerID"].ToString());
            var ArtistID = context.tblArtists.Where(x => x.FK_UserId == id).Select(s => s.ArtistId).FirstOrDefault();
            LoginModel objLogin = objCommon.GetLoginDetails();
            string RVW = Review;
            if (RVW != null)
            {
                tblArtistRating TAR = new tblArtistRating()
                {
                    FK_ArtistId = ArtistID,
                    IsRated = true,
                    UserId = objLogin.UserId,
                    Rating = Convert.ToInt32(Review.ToString()),
                    Comment = Comment
                };
                context.tblArtistRatings.Add(TAR);
                context.SaveChanges();

                //send mail and message to artist
                var MobileNoTo = context.tblArtists.Where(s => s.FK_UserId == id).Select(s => s).FirstOrDefault();
                tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 7).FirstOrDefault();
                string MessageBodyTo = SMSTo.SMSBody.Replace("@Name", MobileNoTo.ArtistName);
                oSMS.SendSMS(MobileNoTo.Mobile.ToString(), MessageBodyTo);
                tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 12).FirstOrDefault();
                string emailBodyTo = emailTo.EmailBody.Replace("@Name", MobileNoTo.ArtistName);
                objCommon.sendMailInHtmlFormat(emailTo.AddCC, MobileNoTo.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);

                //send mail and message to client
                var MobileNofrom = context.tblClients.Where(s => s.FK_UserId == SndBy).Select(s => s).FirstOrDefault();
                tblSMSTemplate SMSFrom = context.tblSMSTemplates.Where(x => x.TemplateId == 8).FirstOrDefault();
                string MessageBodyFrom = SMSFrom.SMSBody.Replace("@Name", MobileNofrom.ClientName);
                oSMS.SendSMS(MobileNofrom.ContactNo.ToString(), MessageBodyFrom);
                tblEmailTemplate emailFrom = context.tblEmailTemplates.Where(x => x.TemplateId == 13).FirstOrDefault();
                string emailBodyFrom = emailFrom.EmailBody.Replace("@Name", MobileNofrom.ClientName);
                objCommon.sendMailInHtmlFormat(emailFrom.AddCC, MobileNofrom.Email, emailFrom.AddBCC, emailFrom.EmailSubject, emailBodyFrom);


            }
            else
            {
                ViewBag.errorMessage = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

    }
}