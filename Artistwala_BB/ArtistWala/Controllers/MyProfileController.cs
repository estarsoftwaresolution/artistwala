﻿using ArtistWala.Areas.Admin.Models;
using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;

namespace ArtistWala.Controllers
{
    [Authorize]
    public class MyProfileController : Controller
    {
        ArtistWalaEntities context = new ArtistWalaEntities();
        AESEncryption objEncrp = new AESEncryption();
        CommonMethods objCommon = new CommonMethods();
        //
        // GET: /MyProfile/

        #region JSONAJAX
        public JsonResult SendReplayMessage(string id, string MessageBody)
        {
            SMSCAPI oSMS = new SMSCAPI();
            
            tblUserMessage oMessages = new tblUserMessage();
            int MessageId = Convert.ToInt32(id);
            var MessageDetails = context.tblUserMessages.Where(x => x.MessageId == MessageId).Select(s => s).FirstOrDefault();
            if (MessageDetails != null)
            {
                oMessages.SendBy = MessageDetails.Fk_UserId;
                oMessages.Fk_UserId = MessageDetails.SendBy;
                oMessages.MessageBody = MessageBody;
                oMessages.MessageType = MessageDetails.MessageType;
                oMessages.MessageSubject = MessageDetails.MessageSubject;
                oMessages.IsUnread = true;
                oMessages.MessageDate = System.DateTime.Now;
                oMessages.MessageParentId = MessageDetails.MessageId;
                context.tblUserMessages.Add(oMessages);
                context.SaveChanges();

                //send mail and message to reciver
                var MobileNoTo = context.tblClients.Where(s => s.FK_UserId == MessageDetails.SendBy).Select(s => s).FirstOrDefault();
                tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 5).FirstOrDefault();
                string MessageBodyTo = SMSTo.SMSBody.Replace("@Name", MobileNoTo.ClientName);
                oSMS.SendSMS(MobileNoTo.ContactNo.ToString(), MessageBodyTo);
                tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 14).FirstOrDefault();
                string emailBodyTo = emailTo.EmailBody.Replace("@Name", MobileNoTo.ClientName);
                objCommon.sendMailInHtmlFormat(emailTo.AddCC, MobileNoTo.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);

                //send mail and message to sender
                var MobileNofrom = context.tblClients.Where(s => s.FK_UserId == MessageDetails.Fk_UserId).Select(s => s).FirstOrDefault();
                tblSMSTemplate SMSFrom = context.tblSMSTemplates.Where(x => x.TemplateId == 6).FirstOrDefault();
                string MessageBodyFrom = SMSFrom.SMSBody.Replace("@Name", MobileNofrom.ClientName);
                oSMS.SendSMS(MobileNofrom.ContactNo.ToString(), MessageBodyFrom);
                tblEmailTemplate emailFrom = context.tblEmailTemplates.Where(x => x.TemplateId == 15).FirstOrDefault();
                string emailBodyFrom = emailFrom.EmailBody.Replace("@Name", MobileNofrom.ClientName);
                objCommon.sendMailInHtmlFormat(emailFrom.AddCC, MobileNofrom.Email, emailFrom.AddBCC, emailFrom.EmailSubject, emailBodyFrom);


            }
            else
            {
                ViewBag.errorMessage = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion
        public ActionResult Dashboard()
        {
            string SendByName = string.Empty;
            string SendByPic = string.Empty;
            string ReplayByNmae = string.Empty;
            LoginModel objLogin = objCommon.GetLoginDetails();
            List<MessageModel> objUserMessage = context.tblUserMessages.Where(x => x.Fk_UserId == objLogin.UserId).Select(s => new MessageModel
            {
                Fk_UserId = s.Fk_UserId,
                MessageBody = s.MessageBody,
                IsUnread = s.IsUnread,
                MessageDate = s.MessageDate,
                MessageId = s.MessageId,
                MessageParentId = s.MessageParentId,
                MessageSubject = s.MessageSubject,
                MessageType = s.MessageType,
                SendBy = s.SendBy,
                SendByPic = SendByPic + context.tblUsers.Where(x => x.UserId == s.SendBy).Select(u => u.ProfileImage).FirstOrDefault(),
                ReplyByPic = s.tblUser.ProfileImage,
                tblUser = s.tblUser,
                ReplayList = context.tblUserMessages.Where(p => p.MessageParentId != null && s.MessageId == p.MessageParentId).Select(v => v).OrderByDescending(o => o.MessageDate).ToList(),
            }).OrderByDescending(o => o.MessageId).ToList();
            if (objUserMessage.Count > 0)
            {
                foreach (var item in objUserMessage)
                {
                    SendByName = SendByName + context.tblUsers.Where(x => x.UserId == item.SendBy).Select(s => s.Name).FirstOrDefault() + ",";
                    if (item.ReplayList.Count > 0)
                    {
                        foreach (var reply in item.ReplayList)
                        {
                            ReplayByNmae = ReplayByNmae + context.tblUsers.Where(x => x.UserId == reply.SendBy).Select(s => s.Name).FirstOrDefault() + ",";
                        }
                        ViewBag.ReplayByNmae = ReplayByNmae;
                    }
                    else
                    {
                        ViewBag.ReplayByNmae = ReplayByNmae;
                    }
                }
                ViewBag.SendByName = SendByName;
            }
            else
            {
                ViewBag.SendByName = SendByName;
            }
            return View(objUserMessage);
        }

        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            Session.Abandon();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult AddYourPicture()
        {
            LoginModel objLogin = objCommon.GetLoginDetails();
            tblUser objUser = context.tblUsers.Where(x => x.UserId == objLogin.UserId).FirstOrDefault();
            ViewBag.CurrentImage = ConfigurationManager.AppSettings["ArtistImagePath"] + objUser.ProfileImage;
            return View();
        }

        [HttpPost]
        public ActionResult AddYourPicture(FormCollection fC)
        {
            try
            {
                //if (!string.IsNullOrEmpty(fC["hidCoordinates"]))
                if (!string.IsNullOrEmpty(fC["hidCoordinates"]) && !Convert.ToString(fC["hidCoordinates"]).Equals("NaN|NaN|NaN|NaN"))
                {
                    LoginModel objLogin = objCommon.GetLoginDetails();

                    //fC["X"]
                    ///int xAxis = Convert.ToInt32(fC["X"].ToString());
                    int xAxis = Convert.ToInt32(fC["hidCoordinates"].Split('|')[0]);
                    int yAxis = Convert.ToInt32(fC["hidCoordinates"].Split('|')[1]);
                    int width = Convert.ToInt32(fC["hidCoordinates"].Split('|')[2]);
                    int height = Convert.ToInt32(fC["hidCoordinates"].Split('|')[3]);



                    Bitmap profImage = (Bitmap)Bitmap.FromFile(Convert.ToString(Session[SessionConstants.ProfilImage]));
                    Bitmap CroppedImage = CommonMethods_bak.CropImage(profImage, xAxis, yAxis, width, height);
                    string Name = Guid.NewGuid() + ".jpg";

                    profImage.Dispose();

                    var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

                    if (!System.IO.File.Exists(path))
                    {
                        CroppedImage.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                        System.IO.File.Delete(Convert.ToString(Session[SessionConstants.ProfilImage]));
                    }

                    if (!context.tblUsers.Any(x => x.UserId == objLogin.UserId))
                    {
                        tblUser objUser = new tblUser()
                        {
                            ProfileImage = Name,
                            IsHoldRights = Convert.ToBoolean(fC["image_ownership"] ?? "false")
                        };

                        context.tblUsers.Add(objUser);
                        context.SaveChanges();

                        ViewBag.Message = "Your profile image is added.";
                    }
                    else
                    {
                        tblUser objUser = context.tblUsers.Where(x => x.UserId == objLogin.UserId).FirstOrDefault();

                        objUser.ProfileImage = Name;
                        objUser.IsHoldRights = true;

                        context.Entry(objUser).State = EntityState.Modified;
                        context.SaveChanges();

                        ViewBag.Message = "Your profile image is updated.";
                    }

                    objLogin.ProfileImage = Name;
                    Session[SessionConstants.UserProfileObject] = objLogin;
                }
                else
                {
                    string Name = SaveImagesinServer("FILE2");
                    ViewBag.Image = ConfigurationManager.AppSettings["ArtistImagePath"] + Name;
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //ViewBag.Message =  "Sorry! Please try again later.";
            }

            return View();
        }

        [HttpGet]
        public ActionResult AddCoverPicture()
        {
            LoginModel objLogin = objCommon.GetLoginDetails();
            tblUser objUser = context.tblUsers.Where(x => x.UserId == objLogin.UserId).FirstOrDefault();
            ViewBag.CurrentImage = ConfigurationManager.AppSettings["ArtistImagePath"] + objUser.CoverImage;
            return View();
        }
        [HttpPost]
        public ActionResult AddCoverPicture(FormCollection fC)
        {
            try
            {
                if (!string.IsNullOrEmpty(fC["hidCoordinates"]) && !Convert.ToString(fC["hidCoordinates"]).Equals("NaN|NaN|NaN|NaN"))
                {
                    LoginModel objLogin = objCommon.GetLoginDetails();

                    int xAxis = Convert.ToInt32(fC["hidCoordinates"].Split('|')[0]);
                    int yAxis = Convert.ToInt32(fC["hidCoordinates"].Split('|')[1]);
                    int width = Convert.ToInt32(fC["hidCoordinates"].Split('|')[2]);
                    int height = Convert.ToInt32(fC["hidCoordinates"].Split('|')[3]);

                    Bitmap profImage = (Bitmap)Bitmap.FromFile(Convert.ToString(Session[SessionConstants.ProfilImage]));
                    Bitmap CroppedImage = CommonMethods_bak.CropImage(profImage, xAxis, yAxis, width, height);
                    string Name = Guid.NewGuid() + ".jpg";

                    profImage.Dispose();

                    var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

                    if (!System.IO.File.Exists(path))
                    {
                        CroppedImage.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                        System.IO.File.Delete(Convert.ToString(Session[SessionConstants.ProfilImage]));
                    }

                    if (!context.tblUsers.Any(x => x.UserId == objLogin.UserId))
                    {
                        tblUser objUser = new tblUser()
                        {
                            CoverImage = Name,
                            IsHoldRights = Convert.ToBoolean(fC["image_ownership"] ?? "false")
                        };

                        context.tblUsers.Add(objUser);
                        context.SaveChanges();

                        ViewBag.Message = "Your cover image is added.";
                    }
                    else
                    {
                        tblUser objUser = context.tblUsers.Where(x => x.UserId == objLogin.UserId).FirstOrDefault();

                        objUser.CoverImage = Name;
                        objUser.IsHoldRights = true;

                        context.Entry(objUser).State = EntityState.Modified;
                        context.SaveChanges();

                        ViewBag.Message = "Your cover image has been updated successfully.";
                        //ViewBag.Image = ConfigurationManager.AppSettings["ArtistImagePath"] + objUser.ProfileImage;
                    }

                    //objLogin.ProfileImage = Name;
                    Session[SessionConstants.UserProfileObject] = objLogin;
                }
                else
                {
                    string Name = SaveImagesinServer2("FILE2");
                    if (Name == "Please upload an image with higher resolution")
                    {
                        ViewBag.Resize = "Please upload an image with higher resolution, minimum of 1200 pixel in width.";
                    }
                    else
                    {
                        ViewBag.Image = ConfigurationManager.AppSettings["ArtistImagePath"] + Name;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
                //ViewBag.Message =  "Sorry! Please try again later.";
            }
            return View();
        }

        public ActionResult AddPhotos()
        {
            List<tblArtistPortfolio> listimages = new List<tblArtistPortfolio>();
            CommonMethods_bak objCommon = new CommonMethods_bak();
            LoginModel UserTbl = objCommon.GetLoginDetails();
            if (UserTbl.ArtistId != 0)
            {
                int id = UserTbl.ArtistId;
                listimages = context.tblArtistPortfolios.Where(x => x.FK_Artist == id).ToList();
            }
            return View(listimages);
        }
        [HttpPost]
        public ActionResult AddPhotos(FormCollection fC, string image_caption)
        {
            List<tblArtistPortfolio> listimages = new List<tblArtistPortfolio>();
            try
            {
                LoginModel UserTbl = objCommon.GetLoginDetails();
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var Photo = Request.Files[i];
                    var fileName = Photo.FileName;
                    string ext = Path.GetExtension(fileName);
                    string Name = Guid.NewGuid() + ext;
                    var newFileName = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

                    var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), fileName);
                    Photo.SaveAs(path);

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Copy(path, newFileName);
                        System.IO.File.Delete(path);
                    }

                    if (System.IO.File.Exists(newFileName))
                    {
                        tblArtistPortfolio objPhoto = new tblArtistPortfolio();
                        objPhoto.FileName = Name;
                        objPhoto.FK_Artist = UserTbl.ArtistId;
                        objPhoto.FileType = "image";
                        objPhoto.Url = "";
                        objPhoto.AddedOn = DateTime.Now;
                        objPhoto.Title = image_caption;
                        context.tblArtistPortfolios.Add(objPhoto);
                        context.SaveChanges();
                        ViewBag.Message = "Your file is uploaded. ";
                    }
                }

                if (UserTbl.ArtistId != 0)
                {
                    int id = UserTbl.ArtistId;
                    listimages = context.tblArtistPortfolios.Where(x => x.FK_Artist == id).ToList();
                }
            }
            catch (Exception ex)
            { }
            return View(listimages);
        }

        public ActionResult Audios()
        {
            List<tblArtistPortfolio> listaudio = new List<tblArtistPortfolio>();
            CommonMethods_bak objCommon = new CommonMethods_bak();
            LoginModel UserTbl = objCommon.GetLoginDetails();
            if (UserTbl.ArtistId != 0)
            {
                int id = UserTbl.ArtistId;
                listaudio = context.tblArtistPortfolios.Where(x => x.FK_Artist == id).ToList();
            }
            return View(listaudio);
        }
        [HttpPost]
        public ActionResult Audios(FormCollection fC, string image_title)
        {
            List<tblArtistPortfolio> listaudio = new List<tblArtistPortfolio>();
            try
            {
                LoginModel UserTbl = objCommon.GetLoginDetails();
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var Audio = Request.Files[i];
                    var fileName = Audio.FileName;
                    string ext = Path.GetExtension(fileName);
                    string Name = Guid.NewGuid() + ext;
                    var newFileName = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

                    var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), fileName);
                    Audio.SaveAs(path);

                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Copy(path, newFileName);
                        System.IO.File.Delete(path);
                    }

                    if (System.IO.File.Exists(newFileName))
                    {
                        tblArtistPortfolio objAudio = new tblArtistPortfolio();
                        objAudio.FileName = Name;
                        objAudio.FK_Artist = UserTbl.ArtistId;
                        objAudio.FileType = "audio";
                        objAudio.Url = "";
                        objAudio.AddedOn = DateTime.Now;
                        objAudio.Title = image_title;
                        context.tblArtistPortfolios.Add(objAudio);
                        context.SaveChanges();
                        ViewBag.Message = "You audio file is Uploaded Successfully";
                    }
                }

                if (UserTbl.ArtistId != 0)
                {
                    int id = UserTbl.ArtistId;
                    listaudio = context.tblArtistPortfolios.Where(x => x.FK_Artist == id).ToList();
                }
            }
            catch (Exception ex)
            { }
            return View(listaudio);
        }

        public ActionResult Videos()
        {
            List<tblArtistPortfolio> listvideo = new List<tblArtistPortfolio>();
            CommonMethods_bak objCommon = new CommonMethods_bak();
            LoginModel UserTbl = objCommon.GetLoginDetails();
            if (UserTbl.ArtistId != 0)
            {
                int id = UserTbl.ArtistId;
                listvideo = context.tblArtistPortfolios.Where(x => x.FK_Artist == id).ToList();
            }
            return View(listvideo);
        }

        [HttpPost]
        public ActionResult Videos(FormCollection fC, string video_title, string video_url)
        {
            List<tblArtistPortfolio> listvideo = new List<tblArtistPortfolio>();
            try
            {
                if (ModelState.IsValid)
                {
                    if (video_url.Split('?').Length > 1)
                    {
                        string queryString = video_url.Split('?')[1];

                        if (!string.IsNullOrEmpty(queryString))
                        {
                            video_url = "http://www.youtube.com/embed/" + queryString.Replace("v=", "");
                        }
                    }
                    LoginModel UserTbl = objCommon.GetLoginDetails();
                    tblArtistPortfolio objVideo = new tblArtistPortfolio();
                    objVideo.FileName = "";
                    objVideo.FK_Artist = UserTbl.ArtistId;
                    objVideo.FileType = "video";
                    objVideo.Url = video_url;
                    objVideo.AddedOn = DateTime.Now;
                    objVideo.Title = video_title;
                    context.tblArtistPortfolios.Add(objVideo);
                    context.SaveChanges();
                    ViewBag.Message = "Your video file is uploaded successfully";

                    if (UserTbl.ArtistId != 0)
                    {
                        int id = UserTbl.ArtistId;
                        listvideo = context.tblArtistPortfolios.Where(x => x.FK_Artist == id).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Sorry! Invalid youtube link. Please add embeded video";
            }
            return View(listvideo);
        }

        public ActionResult YourCalender(FormCollection fC)
        {
            return View();
        }

        public ActionResult ArtistCalender()
        {
            CalenderModel oCalendar = new CalenderModel();
            var TypeResult = context.tblStaticContents.Where(x => x.EventType != "" && x.EventType != null).ToList();
            oCalendar.EventTypeList.Add(new SelectListItem { Text = "Select", Value = "0" });
            foreach (var item in TypeResult)
            {
                oCalendar.EventTypeList.Add(new SelectListItem { Text = item.EventType, Value = item.StaticContentId.ToString() });
            }
            var TimeResult = context.tblStaticContents.Where(x => x.EventTime != "" && x.EventTime != null).ToList();
            oCalendar.TimeiList.Add(new SelectListItem { Text = "Select", Value = "0" });
            foreach (var item in TimeResult)
            {
                oCalendar.TimeiList.Add(new SelectListItem { Text = item.EventTime, Value = item.StaticContentId.ToString() });
            }
            return View(oCalendar);
        }

        [HttpPost]
        public ActionResult ArtistCalender(CalenderModel objCalender)
        {
            try
            {
                int ArtistId = default(int);
                LoginModel objLogin = (LoginModel)Session[SessionConstants.UserProfileObject];
                if (objLogin.ArtistId > 0)
                {
                    ArtistId = objLogin.ArtistId;
                }

                tblArtistSchedule objSchedule = new tblArtistSchedule()
                {
                    Description = objCalender.Description,
                    FK_ArtistId = objLogin.ArtistId,
                    IsActive = objCalender.IsActive,
                    Title = objCalender.Title,
                    ScheduleDate = objCalender.ScheduleDate,
                    Type = objCalender.TypeId.ToString(),
                    Url = objCalender.Url
                };

                context.tblArtistSchedules.Add(objSchedule);
                context.SaveChanges();

                ViewBag.Message = "Your event has been added successfully.";
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Sorry! Please try again later.";
            }
            return View();
        }

        public ActionResult Location(FormCollection fC)
        {
            return View();
        }

        public ActionResult CreatePromokit(int id)
        {
            tblUser objUser = context.tblUsers.Where(x => x.UserId == id).FirstOrDefault();
            tblClient objclient = context.tblClients.Where(x => x.FK_UserId == id).FirstOrDefault();
            objUser.IsPromoKit = true;

            context.Entry(objUser).State = EntityState.Modified;
            context.SaveChanges();

            tblArtist oArtist = context.tblArtists.Where(x => x.FK_UserId == id).FirstOrDefault();

            if (objUser != null)
            {
                if (oArtist != null)
                {
                    LoginModel UserTbl = objCommon.GetLoginDetails();

                    UserTbl.IsPromoKit = true;
                    UserTbl.IsArtist = true;
                    UserTbl.ArtistId = oArtist.ArtistId;

                    Session[SessionConstants.UserProfileObject] = UserTbl;
                }
                else
                {
                    tblArtist objArtist = new tblArtist()
                    {
                        ArtistName = objUser.Name,
                        FK_UserId = id,
                        IsActive = false,
                        Email=objclient.Email
                    };

                    context.tblArtists.Add(objArtist);
                    context.SaveChanges();

                    LoginModel UserTbl = objCommon.GetLoginDetails();

                    UserTbl.IsPromoKit = true;
                    UserTbl.IsArtist = true;
                    UserTbl.ArtistId = objArtist.ArtistId;

                    Session[SessionConstants.UserProfileObject] = UserTbl;
                }

            }

            return RedirectToAction("Dashboard", "MyProfile");
        }

        public ActionResult ReturnClientProfile(int id)
        {
            tblUser objUser = context.tblUsers.Where(x => x.UserId == id).FirstOrDefault();

            objUser.IsPromoKit = false;

            context.Entry(objUser).State = EntityState.Modified;
            context.SaveChanges();

            if (objUser != null)
            {
                tblArtist objArtist = context.tblArtists.Where(x => x.FK_UserId == id).FirstOrDefault();

                LoginModel UserTbl = objCommon.GetLoginDetails();

                UserTbl.IsPromoKit = false;
                UserTbl.IsArtist = false;
                UserTbl.ArtistId = objArtist.ArtistId;

                Session[SessionConstants.UserProfileObject] = UserTbl;
            }
            return RedirectToAction("Dashboard", "MyProfile");
        }

        public ActionResult YourInfo()
        {
            return View(GetYourDetails());
        }

        [HttpPost]
        public ActionResult YourInfo(MyProfileModel objmyprofile, FormCollection fc, string infobutton)
        {
            if (ModelState.IsValid)
            {
                LoginModel UserTbl = objCommon.GetLoginDetails();
                tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == UserTbl.ArtistId).FirstOrDefault();

                tblUser objUser = context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == objmyprofile.UserName.Trim().ToUpper() && x.UserId != UserTbl.UserId).FirstOrDefault();

                tblClient objClient = context.tblClients.Where(x => x.FK_UserId == UserTbl.UserId).FirstOrDefault();

                if (objUser != null)
                {
                    ViewBag.Message = "This username already taken by another user. Please try different.";
                    return View();
                }
                else
                {
                    if (infobutton == "Insert")
                    {
                        objArtist.ArtistName = objmyprofile.ArtistName;
                        objClient.ClientName = objmyprofile.ArtistName;

                        objArtist.DateOfBirth = objmyprofile.DateOfBirth;

                        objArtist.Sex = objmyprofile.Sex;
                        objArtist.HouseNumber = objmyprofile.HouseNumber;
                        objArtist.Address = objmyprofile.Address.Trim();
                        objArtist.City = objmyprofile.City.Trim();
                        objArtist.State = objmyprofile.State.Trim();
                        objArtist.Country = objmyprofile.Country.Trim();
                        objArtist.Pin = objmyprofile.Pin;
                        objArtist.Email = objmyprofile.Email;
                        objClient.Email = objmyprofile.Email;


                        objArtist.Website = objmyprofile.Website;
                        objArtist.Phone = objmyprofile.Phone;
                        objClient.ContactNo = objmyprofile.Mobile;

                        objArtist.Mobile = objmyprofile.Mobile;
                        objArtist.AddedOn = DateTime.Now;
                        objArtist.AboutMe = objmyprofile.AboutMe;

                        context.tblArtists.Add(objArtist);
                        context.SaveChanges();

                        context.tblClients.Add(objClient);
                        context.SaveChanges();

                        ViewBag.Message = "Your details has been saved successfully";
                    }
                    else if (infobutton == "Update")
                    {


                        tblUser objUsers = context.tblUsers.Where(x => x.UserId == UserTbl.UserId).FirstOrDefault();

                        objUsers.UserName = objmyprofile.UserName;
                        context.Entry(objUsers).State = EntityState.Modified;
                        context.SaveChanges();

                        objArtist.ArtistName = objmyprofile.ArtistName;
                        objArtist.DateOfBirth = Convert.ToDateTime(objmyprofile.DateOfBirth);
                        objArtist.Sex = objmyprofile.Sex;
                        objArtist.HouseNumber = objmyprofile.HouseNumber;
                        objArtist.Address = objmyprofile.Address.Trim();
                        objArtist.City = objmyprofile.City.Trim();
                        objArtist.State = objmyprofile.State.Trim();
                        objArtist.Country = objmyprofile.Country.Trim();
                        objArtist.Pin = objmyprofile.Pin;
                        objArtist.Email = objmyprofile.Email;
                        objArtist.Website = objmyprofile.Website;
                        objArtist.Phone = objmyprofile.Phone;
                        objArtist.Mobile = objmyprofile.Mobile;
                        objArtist.AboutMe = objmyprofile.AboutMe;

                        context.Entry(objArtist).State = EntityState.Modified;
                        context.SaveChanges();

                        ViewBag.Message = "Your profile has been updated successfully.";
                    }
                }
            }

            return View(GetYourDetails());
        }

        public ActionResult UserInfo()
        {
            return View(GetUserDetails());
        }

        [HttpPost]
        public ActionResult UserInfo(UserProfileModel objuserprofile, FormCollection fc, string infobutton)
        {
            if (ModelState.IsValid)
            {
                LoginModel UserTbl = objCommon.GetLoginDetails();
                tblClient objClient = context.tblClients.Where(x => x.FK_UserId == UserTbl.UserId).FirstOrDefault();

                tblUser objUser = context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == objuserprofile.UserName.Trim().ToUpper() && x.UserId != UserTbl.UserId).FirstOrDefault();

                //tblClient objClient = context.tblClients.Where(x => x.FK_UserId == UserTbl.UserId).FirstOrDefault();

                if (objUser != null)
                {
                    ViewBag.Message = "This username already taken by another user. Please try different.";
                    return View();
                }
                else
                {
                    if (infobutton == "Insert")
                    {
                        objClient.ClientName = objuserprofile.ClientName;
                        objClient.ContactNo = objuserprofile.ContactNo;

                        objClient.Email = objuserprofile.Email;

                       

                        ViewBag.Message = "Your details has been saved successfully";
                    }
                    else if (infobutton == "Update")
                    {


                        tblUser objUsers = context.tblUsers.Where(x => x.UserId == UserTbl.UserId).FirstOrDefault();

                        objUsers.UserName = objuserprofile.UserName;
                        objUsers.Name = objuserprofile.ClientName;
                        context.Entry(objUsers).State = EntityState.Modified;
                        context.SaveChanges();

                        objClient.ClientName = objuserprofile.ClientName;
                        objClient.Email = objuserprofile.Email;
                        objClient.ContactNo = objuserprofile.ContactNo;

                        context.Entry(objClient).State = EntityState.Modified;
                        context.SaveChanges();

                        ViewBag.Message = "Your profile has been updated successfully. Informatin will effective in next Login.";
                    }
                }
            }

            return View(GetUserDetails());
        }
        public ActionResult Categories()
        {
            List<tblArtistCategoryMapping> objSubCategoryList = new List<tblArtistCategoryMapping>();
            try
            {
                LoginModel UserTbl = objCommon.GetLoginDetails();
                objSubCategoryList = context.tblArtistCategoryMappings.Where(x => x.FK_ArtistId == UserTbl.ArtistId).ToList();
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return View(objSubCategoryList);
        }

        [HttpPost]
        public ActionResult Categories(FormCollection fc)
        {
            List<tblArtistCategoryMapping> objSubCategoryList = new List<tblArtistCategoryMapping>();
            try
            {
                LoginModel UserTbl = objCommon.GetLoginDetails();
                int count = default(int);

                int.TryParse(fc["hidSubcatCount"], out count);

                for (int i = 1; i <= count; i++)
                {
                    int catId = default(int);
                    int subCatId = default(int);

                    int.TryParse(fc["ddlCategory"], out catId);
                    int.TryParse(fc["chkSubCat" + i], out subCatId);

                    if (catId > 0 && subCatId > 0)
                    {
                        tblArtistCategoryMapping objMapping = new tblArtistCategoryMapping()
                        {
                            FK_ArtistId = UserTbl.ArtistId,
                            FK_CategoryId = catId,
                            FK_SubCategoryId = subCatId
                        };

                        context.tblArtistCategoryMappings.Add(objMapping);
                    }
                }

                context.SaveChanges();

                ViewBag.Message = "Your categories have been saved successfully";
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Sorry! There is an error. Please try again later.";
            }
            return RedirectToAction("Categories", "MyProfile");
        }

        public ActionResult DeleteGroupMember(int id)
        {
            tblArtistGroupMember objGroupMember = context.tblArtistGroupMembers.Where(x => x.GroupMemberId == id).FirstOrDefault();

            context.tblArtistGroupMembers.Remove(objGroupMember);
            context.SaveChanges();

            return RedirectToAction("ProfessionalInfo", "MyProfile");
        }

        public ActionResult DeletePhoto(int id)
        {
            tblArtistPortfolio objPhoto = context.tblArtistPortfolios.Where(x => x.PortfolioId == id).FirstOrDefault();

            context.tblArtistPortfolios.Remove(objPhoto);
            context.SaveChanges();

            return RedirectToAction("AddPhotos", "MyProfile");
        }

        public ActionResult DeleteAudio(int id)
        {
            tblArtistPortfolio objPhoto = context.tblArtistPortfolios.Where(x => x.PortfolioId == id).FirstOrDefault();

            context.tblArtistPortfolios.Remove(objPhoto);
            context.SaveChanges();

            return RedirectToAction("Audios", "MyProfile");
        }

        public ActionResult DeleteVideo(int id)
        {
            tblArtistPortfolio objPhoto = context.tblArtistPortfolios.Where(x => x.PortfolioId == id).FirstOrDefault();

            context.tblArtistPortfolios.Remove(objPhoto);
            context.SaveChanges();

            return RedirectToAction("Videos", "MyProfile");
        }

        public ActionResult DeleteCategoryMapping(int id)
        {
            tblArtistCategoryMapping objMapping = context.tblArtistCategoryMappings.Where(x => x.ArtistCategoryMappingId == id).FirstOrDefault();

            context.tblArtistCategoryMappings.Remove(objMapping);
            context.SaveChanges();

            return RedirectToAction("Categories", "MyProfile");
        }

        //private string SaveImagesinServer(string file)
        //{
        //    var profileImage = Request.Files[file];
        //    var fileName = profileImage.FileName;
        //    string ext = Path.GetExtension(fileName);
        //    string Name = Guid.NewGuid() + ext;
        //    var newFileName = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

        //    var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), fileName);
        //    profileImage.SaveAs(path);

        //    if (System.IO.File.Exists(path))
        //    {
        //        System.IO.File.Copy(path, newFileName);
        //        Session[SessionConstants.ProfilImage] = newFileName;
        //        Session[SessionConstants.ImageName] = Name;

        //        System.IO.File.Delete(path);
        //    }
        //    return Name;
        //}
        private string SaveImagesinServer(string file)
        {
            var profileImage = Request.Files[file];
            WebImage img = new WebImage(profileImage.InputStream);

            int imgWidth = img.Width;

            if (imgWidth > 1024)
            {
                img.Resize(1024, 768, true);
            }

            var fileName = profileImage.FileName;
            //var fileName = img.FileName;
            string ext = Path.GetExtension(fileName);
            string Name = Guid.NewGuid() + ext;
            var newFileName = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

            var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), fileName);
            //profileImage.SaveAs(path);
            img.Save(path);

            if (System.IO.File.Exists(path))
            {
                System.IO.File.Copy(path, newFileName);
                Session[SessionConstants.ProfilImage] = newFileName;
                Session[SessionConstants.ImageName] = Name;

                System.IO.File.Delete(path);
            }
            return Name;
        }

        private string SaveImagesinServer2(string file)
        {
            var profileImage = Request.Files[file];
            WebImage img = new WebImage(profileImage.InputStream);
            string Name;
            int imgWidth = img.Width;

            if (imgWidth > 1400)
            {
                img.Resize(1200, 900, true);

                var fileName = profileImage.FileName;
                //var fileName = img.FileName;
                string ext = Path.GetExtension(fileName);
                Name = Guid.NewGuid() + ext;
                var newFileName = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

                var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), fileName);
                //profileImage.SaveAs(path);
                img.Save(path);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Copy(path, newFileName);
                    Session[SessionConstants.ProfilImage] = newFileName;
                    Session[SessionConstants.ImageName] = Name;

                    System.IO.File.Delete(path);
                }
                return Name;
            }
            else //(imgWidth < 1400)
            {

                Name = "Please upload image with high resolution";
                //ViewBag.Resize(Name);
                return Name;
            }
        }


        private ArtistOverviewModel OverviewDetails()
        {

            //new code
            ArtistOverviewModel oArtistOverview = new ArtistOverviewModel();
            LoginModel objLogin = objCommon.GetLoginDetails();
            var objArtistOverview = context.tblArtistOverviews.Where(x => x.FK_ArtistId == objLogin.ArtistId).FirstOrDefault();
            if (objArtistOverview != null)
            {
                oArtistOverview.Calender = objArtistOverview.Calender;
                oArtistOverview.Influence = objArtistOverview.Influence;
                oArtistOverview.Personal = objArtistOverview.Personal;
                oArtistOverview.SetUpRequirements = objArtistOverview.SetUpRequirements;
                oArtistOverview.Tesimonials = objArtistOverview.Tesimonials;
                oArtistOverview.WhatToExpect = objArtistOverview.WhatToExpect;

            }

            return oArtistOverview;
        }



        private MyProfileModel GetYourDetails()
        {

            LoginModel objLogin = objCommon.GetLoginDetails();

            tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == objLogin.ArtistId).FirstOrDefault();
            tblUser objUser = context.tblUsers.Where(x => x.UserId == objLogin.UserId).FirstOrDefault();
            MyProfileModel objmyprofile = new MyProfileModel()
            {
                UserName = objArtist.tblUser.UserName,
                ArtistId = objLogin.ArtistId,
                ArtistName = objArtist.ArtistName,
                DateOfBirth = (objArtist.DateOfBirth),
                Sex = objArtist.Sex,
                HouseNumber = objArtist.HouseNumber,
                Address = objArtist.Address,
                City = objArtist.City,
                State = objArtist.State,
                Country = objArtist.Country,
                Pin = objArtist.Pin,
                Email = objArtist.Email,
                Website = objArtist.Website,
                Phone = objArtist.Phone,
                Mobile = objArtist.Mobile,
                IsActive = objArtist.IsActive,
                AboutMe = objArtist.AboutMe,
                FK_UserId = objArtist.FK_UserId,
                Notification = objUser.Notification
            };

            return objmyprofile;
        }

        private UserProfileModel GetUserDetails()
        {

            LoginModel objLogin = objCommon.GetLoginDetails();

            tblClient objclient = context.tblClients.Where(x => x.FK_UserId == objLogin.UserId).FirstOrDefault();
            tblUser objUser = context.tblUsers.Where(x => x.UserId == objLogin.UserId).FirstOrDefault();
            UserProfileModel objuserprofile = new UserProfileModel()
            {
                UserName = objclient.tblUser.UserName,
                FK_UserId = objLogin.UserId,
                ClientName = objclient.ClientName,
                ClientId = objclient.ClientId,
                Email = objclient.Email,
                ContactNo = objclient.ContactNo,
            };

            return objuserprofile;
        }

        private ProfessionalInfoModel GetYourProfessionalInfoDetails()
        {

            LoginModel objLogin = objCommon.GetLoginDetails();

            tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == objLogin.ArtistId).FirstOrDefault();

            ProfessionalInfoModel objmyprofile = new ProfessionalInfoModel()
            {
                ArtistId = objArtist.ArtistId,
                FB_FanLink = objArtist.FB_FanLink,
                groupMember = objArtist.tblArtistGroupMembers.ToList(),
                IsFeatured = (objArtist.IsFeatured ?? false),
                IsGroup = objArtist.IsGroup,
                MinmumRates = (objArtist.MinmumRates ?? 0M)
            };

            return objmyprofile;
        }

        public ActionResult ProfessionalInfo()
        {
            return View(GetYourProfessionalInfoDetails());
        }

        [HttpPost]
        public ActionResult ProfessionalInfo(ProfessionalInfoModel objmyprofile, FormCollection fc)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    LoginModel UserTbl = objCommon.GetLoginDetails();
                    tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == UserTbl.ArtistId).FirstOrDefault();

                    objArtist.IsFeatured = objmyprofile.IsFeatured;
                    objArtist.IsGroup = objmyprofile.IsGroup;
                    objArtist.FB_FanLink = objmyprofile.FB_FanLink;
                    objArtist.MinmumRates = objmyprofile.MinmumRates;

                    context.Entry(objArtist).State = EntityState.Modified;
                    context.SaveChanges();

                    int groupCount = default(int);

                    int.TryParse(fc["hidGroupMember"], out groupCount);

                    for (int i = 1; i <= groupCount; i++)
                    {
                        if (!string.IsNullOrEmpty(fc["txtGrMemberName" + i]))
                        {
                            string Name = string.Empty;
                            if (!string.IsNullOrEmpty(fc["txtGrMemberName" + i]))
                            {
                                Name = SaveImagesinServer("txtGrMemberImg" + i);
                            }

                            tblArtistGroupMember objGroup = new tblArtistGroupMember()
                            {
                                FK_ArtistId = objArtist.ArtistId,
                                ImagePath = Name,
                                MemberName = fc["txtGrMemberName" + i],
                                MemberDescription = fc["txtGrDescription" + i],
                                IsActive = true
                            };

                            context.tblArtistGroupMembers.Add(objGroup);
                            context.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return View(GetYourProfessionalInfoDetails());
        }

        public ActionResult QuotesLists()
        {
            List<tblQuote> objQuote = context.tblQuotes.OrderByDescending(x => x.AddedDate).ToList();
            return View(objQuote);
        }

        public ActionResult ChangePassword()
        {
            return View();

        }

        [HttpPost]
        public ActionResult ChangePassword(ChangeUserPasswordModel objCP)
        {
            try
            {
                int UserId = default(int);
                var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null)
                {
                    var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    int.TryParse(authTicket.UserData, out UserId);
                    // data is empty !!!
                }

                tblUser objUser = context.tblUsers.Find(UserId);
                tblClient objClient = context.tblClients.Where(x => x.FK_UserId == objUser.UserId).FirstOrDefault();

                if (objEncrp.DecryptPassword(objUser.Password) == objCP.OldPassword)
                {
                    objUser.Password = objEncrp.EncryptPassword(objCP.NewPassword);

                    context.Entry(objUser).State = EntityState.Modified;
                    context.SaveChanges();

                    var Modarators = context.tblModarators.Where(x => x.FK_UserId == UserId).FirstOrDefault();
                    string emailTo = string.Empty;
                    if (Modarators != null)
                    {
                        emailTo = Modarators.Email;
                    }

                    tblEmailTemplate email = context.tblEmailTemplates.Where(x => x.TemplateId == 4).FirstOrDefault();
                    string emailBody = email.EmailBody.Replace("@Name", objUser.Name).Replace("@Password", objEncrp.DecryptPassword(objUser.Password));
                    objCommon.sendMailInHtmlFormat(email.AddCC, objClient.Email, email.AddBCC, email.EmailSubject, emailBody);

                    ViewBag.Message = "Your password has been successfully updated.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Your current password did not match.";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Sorry! Please try gain later.";
            }

            ModelState.Clear();
            return View();
        }

        [HttpGet]
        public ActionResult ArtistOverview()
        {
            return View(OverviewDetails());
        }
        [HttpPost]
        public ActionResult ArtistOverview(ArtistOverviewModel oModel)
        {
            tblArtistOverview oOverview = new tblArtistOverview();
            LoginModel objLogin = objCommon.GetLoginDetails();
            var UserDetails = context.tblArtistOverviews.Where(w => w.FK_ArtistId == objLogin.ArtistId).FirstOrDefault();
            if (UserDetails != null)
            {
                oOverview = context.tblArtistOverviews.Where(w => w.FK_ArtistId == UserDetails.FK_ArtistId).FirstOrDefault();
                oOverview.FK_ArtistId = objLogin.ArtistId;
                oOverview.Calender = oModel.Calender;
                oOverview.Influence = oModel.Influence;
                oOverview.Personal = oModel.Personal;
                oOverview.SetUpRequirements = oModel.SetUpRequirements;
                oOverview.Tesimonials = oModel.Tesimonials;
                oOverview.WhatToExpect = oModel.WhatToExpect;
                context.Entry(oOverview).State = EntityState.Modified;
                context.SaveChanges();
            }
            else
            {
                oOverview.FK_ArtistId = objLogin.ArtistId;
                oOverview.Calender = oModel.Calender;
                oOverview.Influence = oModel.Influence;
                oOverview.Personal = oModel.Personal;
                oOverview.SetUpRequirements = oModel.SetUpRequirements;
                oOverview.Tesimonials = oModel.Tesimonials;
                oOverview.WhatToExpect = oModel.WhatToExpect;
                context.tblArtistOverviews.Add(oOverview);
                context.SaveChanges();
            }
            return RedirectToAction("ArtistOverView", "MyProfile");
        }

        // new Views for Leeds and Booking

        public ActionResult Leads()
        {
            string SendByName = string.Empty;
            string SendByPic = string.Empty;
            string ReplayByNmae = string.Empty;
            LoginModel objLogin = objCommon.GetLoginDetails();
            List<MessageModel> objUserMessage = context.tblUserMessages.Where(x => x.Fk_UserId == objLogin.UserId && x.Fk_QuoteId != null && x.Approved == null).Select(s => new MessageModel
            {
                Fk_UserId = s.Fk_UserId,
                MessageBody = s.MessageBody,
                IsUnread = s.IsUnread,
                MessageDate = s.MessageDate,
                MessageId = s.MessageId,
                MessageParentId = s.MessageParentId,
                MessageSubject = s.MessageSubject,
                MessageType = s.MessageType,
                SendBy = s.SendBy,
                SendByPic = SendByPic + context.tblUsers.Where(x => x.UserId == s.SendBy).Select(u => u.ProfileImage).FirstOrDefault(),
                ReplyByPic = s.tblUser.ProfileImage,
                tblUser = s.tblUser,
                Fk_QuoteId = s.Fk_QuoteId,
                ReplayList = context.tblUserMessages.Where(p => p.MessageParentId != null && s.MessageId == p.MessageParentId).Select(v => v).OrderByDescending(o => o.MessageDate).ToList(),
            }).OrderByDescending(o => o.MessageId).ToList();
            if (objUserMessage.Count > 0)
            {
                foreach (var item in objUserMessage)
                {
                    SendByName = SendByName + context.tblUsers.Where(x => x.UserId == item.SendBy).Select(s => s.Name).FirstOrDefault() + ",";
                    if (item.ReplayList.Count > 0)
                    {
                        foreach (var reply in item.ReplayList)
                        {
                            ReplayByNmae = ReplayByNmae + context.tblUsers.Where(x => x.UserId == reply.SendBy).Select(s => s.Name).FirstOrDefault() + ",";
                        }
                        ViewBag.ReplayByNmae = ReplayByNmae;
                    }
                    else
                    {
                        ViewBag.ReplayByNmae = ReplayByNmae;
                    }
                }
                ViewBag.SendByName = SendByName;
            }
            else
            {
                ViewBag.SendByName = SendByName;
            }
            return View(objUserMessage);
        }

        #region JSONAJAX
        public JsonResult ApproveToBook(string id, string Fk_QuoteId)
        {
            int QuoteId = Convert.ToInt32(Fk_QuoteId);
            var cQuote = context.tblQuotes.Where(x => x.QuoteId == QuoteId).Select(s => s).FirstOrDefault();
            int MessageId = Convert.ToInt32(id);
            var MessageDetails = context.tblUserMessages.Where(x => x.MessageId == MessageId && x.Fk_QuoteId != null && x.Approved == null).Select(s => s).FirstOrDefault();
            //
            var ArtistId = context.tblArtists.Where(x => x.FK_UserId == MessageDetails.Fk_UserId).Select(s => s.ArtistId).FirstOrDefault();
            var UserId = MessageDetails.SendBy;
            //
            var review = context.tblArtistRatings.Where(x => x.FK_ArtistId == ArtistId && x.UserId == UserId).FirstOrDefault();

            if (MessageDetails != null)
            {
                MessageDetails.Approved = true;
                context.Entry(MessageDetails).State = EntityState.Modified;
                context.SaveChanges();

                cQuote.AssignTo = MessageDetails.Fk_UserId;
                context.Entry(cQuote).State = EntityState.Modified;
                context.SaveChanges();

            }
            else
            {
                ViewBag.errorMessage = "Something is wrong!";
            }

            if (review == null)
            {
                tblArtistRating TAR = new tblArtistRating()
                {
                    FK_ArtistId = Convert.ToInt32(ArtistId.ToString()),
                    IsRated = false,
                    UserId = Convert.ToInt32(UserId.ToString()),
                    Rating = null,
                    Comment = null
                };
                context.tblArtistRatings.Add(TAR);
                context.SaveChanges();

                int ArtistRatingId = TAR.ArtistRatingId;

                tblQuoteRatingMapping QRM = new tblQuoteRatingMapping()
                {
                    FK_QuoteId = QuoteId,
                    FK_RatingId = ArtistRatingId,
                    emaildate = cQuote.EventDate,
                };
                context.tblQuoteRatingMappings.Add(QRM);
                context.SaveChanges();

                var duration = 7;
                //context.tblStaticContents.Select(x => x.MailSendDuration).FirstOrDefault();

                for (int i = 0; i < 4; i++)
                {
                    var LASTDATE = context.tblQuoteRatingMappings.Where(x => x.FK_QuoteId == QuoteId && x.FK_RatingId == ArtistRatingId).Select(s => s.emaildate).Max();
                    DateTime msgdate = LASTDATE.Value.AddDays(7);//Convert.ToDouble(duration.ToString())
                    tblQuoteRatingMapping RQM = new tblQuoteRatingMapping()
                    {
                        FK_QuoteId = QuoteId,
                        FK_RatingId = ArtistRatingId,
                        emaildate = msgdate//DateTime.Parse(msgdate.ToString())
                    };
                    context.tblQuoteRatingMappings.Add(RQM);
                    context.SaveChanges();
                }
            }
            else
            {
            }
            return Json("Success");
        }
        #endregion

        public ActionResult Booking()
        {
            string SendByName = string.Empty;
            string SendByPic = string.Empty;
            string ReplayByNmae = string.Empty;
            LoginModel objLogin = objCommon.GetLoginDetails();
            List<MessageModel> objUserMessage = context.tblUserMessages.Where(x => x.Fk_UserId == objLogin.UserId && x.Approved == true).Select(s => new MessageModel
            {
                Fk_UserId = s.Fk_UserId,
                MessageBody = s.MessageBody,
                IsUnread = s.IsUnread,
                MessageDate = s.MessageDate,
                MessageId = s.MessageId,
                MessageParentId = s.MessageParentId,
                MessageSubject = s.MessageSubject,
                MessageType = s.MessageType,
                SendBy = s.SendBy,
                SendByPic = SendByPic + context.tblUsers.Where(x => x.UserId == s.SendBy).Select(u => u.ProfileImage).FirstOrDefault(),
                ReplyByPic = s.tblUser.ProfileImage,
                tblUser = s.tblUser,
                ReplayList = context.tblUserMessages.Where(p => p.MessageParentId != null && s.MessageId == p.MessageParentId).Select(v => v).OrderByDescending(o => o.MessageDate).ToList(),
            }).OrderByDescending(o => o.MessageId).ToList();
            if (objUserMessage.Count > 0)
            {
                foreach (var item in objUserMessage)
                {
                    SendByName = SendByName + context.tblUsers.Where(x => x.UserId == item.SendBy).Select(s => s.Name).FirstOrDefault() + ",";
                    if (item.ReplayList.Count > 0)
                    {
                        foreach (var reply in item.ReplayList)
                        {
                            ReplayByNmae = ReplayByNmae + context.tblUsers.Where(x => x.UserId == reply.SendBy).Select(s => s.Name).FirstOrDefault() + ",";
                        }
                        ViewBag.ReplayByNmae = ReplayByNmae;
                    }
                    else
                    {
                        ViewBag.ReplayByNmae = ReplayByNmae;
                    }
                }
                ViewBag.SendByName = SendByName;
            }
            else
            {
                ViewBag.SendByName = SendByName;
            }
            return View(objUserMessage);
        }


        public ActionResult Reviews()
        {
            string ArtistName = string.Empty;
            string ProfileImage = string.Empty;
            LoginModel objLogin = objCommon.GetLoginDetails();
            var LogedClient = context.tblClients.Where(x => x.FK_UserId == objLogin.UserId).Select(s => s).FirstOrDefault();
            List<ArtistRating> allReview = context.tblArtistRatings.Where(x => x.UserId == objLogin.UserId && x.IsRated == false && x.Rating == null).Select(s => new ArtistRating
            {
                ArtistRatingId = s.ArtistRatingId,
                FK_ArtistId = s.FK_ArtistId,
                ArtistName = ArtistName + context.tblArtists.Where(x => x.ArtistId == s.FK_ArtistId).Select(a => a.ArtistName).FirstOrDefault(),
                ProfileImage = ProfileImage + context.tblArtists.Where(x => x.ArtistId == s.FK_ArtistId).Select(u => u.tblUser.ProfileImage).FirstOrDefault(),
            }).ToList();
            return View(allReview);
        }

        #region JSONAJAX
        public JsonResult SaveReview(string ArtistRatingId, string Review, string Comment)
        {
            SMSCAPI oSMS = new SMSCAPI();
            int SndBy = Convert.ToInt32(Session["LogedUserID"].ToString());
            int ARId = Convert.ToInt32(ArtistRatingId);
            var AUID = context.tblArtists.Where(x => x.ArtistId == ARId).Select(s => s.FK_UserId).FirstOrDefault();
                        
            var ReviewDetails = context.tblArtistRatings.Where(x => x.ArtistRatingId == ARId).Select(s => s).FirstOrDefault();
            if (ReviewDetails != null)
            {
                ReviewDetails.IsRated = true;
                ReviewDetails.Rating = Convert.ToInt32(Review.ToString());
                ReviewDetails.Comment = Comment;
                context.Entry(ReviewDetails).State = EntityState.Modified;
                context.SaveChanges();

                //send mail and message to artist
                var MobileNoTo = context.tblArtists.Where(s => s.FK_UserId == AUID).Select(s => s).FirstOrDefault();
                tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 7).FirstOrDefault();
                string MessageBodyTo = SMSTo.SMSBody.Replace("@Name", MobileNoTo.ArtistName);
                oSMS.SendSMS(MobileNoTo.Mobile.ToString(), MessageBodyTo);
                tblEmailTemplate emailTo = context.tblEmailTemplates.Where(x => x.TemplateId == 12).FirstOrDefault();
                string emailBodyTo = emailTo.EmailBody.Replace("@Name", MobileNoTo.ArtistName);
                objCommon.sendMailInHtmlFormat(emailTo.AddCC, MobileNoTo.Email, emailTo.AddBCC, emailTo.EmailSubject, emailBodyTo);

                //send mail and message to client
                var MobileNofrom = context.tblClients.Where(s => s.FK_UserId == SndBy).Select(s => s).FirstOrDefault();
                tblSMSTemplate SMSFrom = context.tblSMSTemplates.Where(x => x.TemplateId == 8).FirstOrDefault();
                string MessageBodyFrom = SMSFrom.SMSBody.Replace("@Name", MobileNofrom.ClientName);
                oSMS.SendSMS(MobileNofrom.ContactNo.ToString(), MessageBodyFrom);
                tblEmailTemplate emailFrom = context.tblEmailTemplates.Where(x => x.TemplateId == 13).FirstOrDefault();
                string emailBodyFrom = emailFrom.EmailBody.Replace("@Name", MobileNofrom.ClientName);
                objCommon.sendMailInHtmlFormat(emailFrom.AddCC, MobileNofrom.Email, emailFrom.AddBCC, emailFrom.EmailSubject, emailBodyFrom);

            }
            else
            {
                ViewBag.errorMessage = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        public ActionResult GivenReview()
        {
            string ArtistName = string.Empty;
            string ProfileImage = string.Empty;
            LoginModel objLogin = objCommon.GetLoginDetails();
            var LogedClient = context.tblClients.Where(x => x.FK_UserId == objLogin.UserId).Select(s => s).FirstOrDefault();
            List<ArtistRating> allReview = context.tblArtistRatings.Where(x => x.UserId == objLogin.UserId && x.IsRated == true && x.Rating != null).Select(s => new ArtistRating
            {
                ArtistRatingId = s.ArtistRatingId,
                FK_ArtistId = s.FK_ArtistId,
                ArtistName = ArtistName + context.tblArtists.Where(x => x.ArtistId == s.FK_ArtistId).Select(a => a.ArtistName).FirstOrDefault(),
                ProfileImage = ProfileImage + context.tblArtists.Where(x => x.ArtistId == s.FK_ArtistId).Select(u => u.tblUser.ProfileImage).FirstOrDefault(),
                Rating = s.Rating,
                Comment = s.Comment,
            }).ToList();
            return View(allReview);
        }

        public ActionResult EditReviewDetails(string id)
        {
            int ARId = Convert.ToInt32(id);
            string ArtistName = string.Empty;
            string ProfileImage = string.Empty;
            LoginModel objLogin = objCommon.GetLoginDetails();
            var LogedClient = context.tblClients.Where(x => x.FK_UserId == objLogin.UserId).Select(s => s).FirstOrDefault();
            List<ArtistRating> allReview = context.tblArtistRatings.Where(x => x.ArtistRatingId == ARId).Select(s => new ArtistRating
            {
                ArtistRatingId = s.ArtistRatingId,
                FK_ArtistId = s.FK_ArtistId,
                ArtistName = ArtistName + context.tblArtists.Where(x => x.ArtistId == s.FK_ArtistId).Select(a => a.ArtistName).FirstOrDefault(),
                ProfileImage = ProfileImage + context.tblArtists.Where(x => x.ArtistId == s.FK_ArtistId).Select(u => u.tblUser.ProfileImage).FirstOrDefault(),
                Rating = s.Rating,
                Comment = s.Comment,
            }).ToList();
            return View(allReview);
        }

        #region JSONAJAX
        public JsonResult UpdateReview(string ArtistRatingId, string Review, string Comment)
        {
            int ARId = Convert.ToInt32(ArtistRatingId);
            tblArtistRating Arevw = context.tblArtistRatings.Where(x => x.ArtistRatingId == ARId).Select(s => s).FirstOrDefault();
            string RVW = Review;
            //var MessageDetails = context.tblUserMessages.Where(x => x.MessageId == MessageId && x.Fk_QuoteId != null && x.Approved == null).Select(s => s).FirstOrDefault();
            if (RVW != null)
            {
                Arevw.Rating = Convert.ToInt32(RVW.ToString());
                Arevw.Comment = Comment.ToString();
                context.Entry(Arevw).State = EntityState.Modified;
                context.SaveChanges();
            }
            else
            {
                ViewBag.errorMessage = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        public ActionResult MyReviews()
        {
            string ClientName = string.Empty;
            string ClientImage = string.Empty;
            LoginModel objLogin = objCommon.GetLoginDetails();
            List<ArtistRating> allReview = new List<ArtistRating>();
            var LogedArtist = context.tblArtists.Where(x => x.FK_UserId == objLogin.UserId).Select(s => s).FirstOrDefault();

            if (LogedArtist != null)
            {
                allReview = context.tblArtistRatings.Where(x => x.FK_ArtistId == LogedArtist.ArtistId && x.IsRated == true && x.Rating != null).Select(s => new ArtistRating
                {
                    ArtistRatingId = s.ArtistRatingId,
                    UserId = s.UserId,
                    ClientName = ClientName + context.tblClients.Where(x => x.FK_UserId == s.UserId).Select(a => a.ClientName).FirstOrDefault(),
                    ClientImage = ClientImage + context.tblUsers.Where(x => x.UserId == s.UserId).Select(u => u.ProfileImage).FirstOrDefault(),
                    Rating = s.Rating,
                    Comment = s.Comment,
                }).ToList();
            }
            else
            { 
            }           
            //return View(allReview);

            return View(allReview);
        }

        public ActionResult Tools()
        {
            return View(GetYourDetails());
        }

        #region JSONAJAX
        public JsonResult Deactive(string Deactive)
        {
            int Id = Convert.ToInt32(Deactive);
            tblArtist Artist = context.tblArtists.Where(x => x.FK_UserId == Id).Select(s => s).FirstOrDefault();
            if (User != null)
            {
                Artist.IsActive = false;
                context.Entry(Artist).State = EntityState.Modified;
                context.SaveChanges();
            }
            else
            {
                ViewBag.errorMessage = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        #region JSONAJAX
        public JsonResult Active(string Active)
        {
            int Id = Convert.ToInt32(Active);
            tblArtist Artist = context.tblArtists.Where(x => x.FK_UserId == Id).Select(s => s).FirstOrDefault();
            if (User != null)
            {
                Artist.IsActive = true;
                context.Entry(Artist).State = EntityState.Modified;
                context.SaveChanges();
            }
            else
            {
                ViewBag.errorMessage = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion


        #region JSONAJAX
        public JsonResult DeletePermanently(string DeletePermanently)
        {
            int Id = Convert.ToInt32(DeletePermanently);
            var Artist = context.tblArtists.Where(x => x.FK_UserId == Id).Select(s => s).FirstOrDefault();
            if (User != null)
            {
                try
                {

                    tblArtist TA = context.tblArtists.First(d => d.ArtistId == Artist.ArtistId);
                    TA.IsActive = false;
                    context.Entry(TA).State = EntityState.Modified;
                    context.SaveChanges();
                    //
                    tblUser Users = context.tblUsers.First(d => d.UserId == Id);
                    Users.IsDelete = true;
                    Users.IsActive = false;
                    context.Entry(Users).State = EntityState.Modified;
                    context.SaveChanges();

                    FormsAuthentication.SignOut();

                    Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Cache.SetNoStore();
                    Session.Abandon();

                    //return RedirectToAction("Index", "Home");

                }
                catch { }
            }
            else
            {
                ViewBag.errorMessage = "Something is wrong!";
            }
            return Json("Success");
        }
        #endregion

        public ActionResult Notification()
        {
            return View(GetYourDetails());
        }


        #region JSONAJAX
        public JsonResult Notify(int id, bool Notify)
        {
            int Id = id;
            var Status = Notify;
            if (Status == true)
            {
                try
                {
                    tblUser TU = context.tblUsers.First(d => d.UserId == id);
                    TU.Notification = true;
                    context.Entry(TU).State = EntityState.Modified;
                    context.SaveChanges();

                }
                catch { }
            }
            else
            {
                try
                {
                    tblUser TU = context.tblUsers.First(d => d.UserId == id);
                    TU.Notification = false;
                    context.Entry(TU).State = EntityState.Modified;
                    context.SaveChanges();
                }
                catch { }
            }
            return Json("Success");
        }
        #endregion

    }
}