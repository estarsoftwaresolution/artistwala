﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Areas.Admin.Models
{
    public class SMSModel
    {
        public int TemplateId { get; set; }
        [Required]
        public string TemplateName { get; set; }

        [Required]
        [StringLength(160)]
        public string SMSBody { get; set; }
        public bool IsActive { get; set; }
    }
}