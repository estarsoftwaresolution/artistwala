﻿using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Areas.Admin.Models
{
    public class CategoryModel
    {
        public int CategoryId { get; set; }

        public List<tblArtistCategory> objArtistCategory { get; set; }

        [Required]
        public int ArtistTypeId { get; set; }

        [Required]
        [StringLength(40)]
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }
}