﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Areas.Admin.Models
{
    public class EmailModel
    {
        public int TemplateId { get; set; }

        [Required]
        public string TemplateName { get; set; }

        [Required]
        [StringLength(130)]
        public string EmailSubject { get; set; }

        [Required]
        [StringLength(4000)]
        public string EmailBody { get; set; }

        [StringLength(400)]
        public string AddBCC { get; set; }

        [StringLength(400)]
        public string AddCC { get; set; }
        public bool IsActive { get; set; }

        public string AssociatedTags { get; set; }
    }
}