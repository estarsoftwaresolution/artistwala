﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtistWala.Areas.Admin.Models
{
    public class DashaboardModel
    {
        public int ArtistCount { get; set; }

        public double MessageCount { get; set; }

        public int ApprovalRequestCount { get; set; }

    }
}