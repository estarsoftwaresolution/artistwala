﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArtistWala.Areas.Admin.Models
{
    /// <summary>
    /// this is for content
    /// </summary>
    public class ContentModel
    {
        public string Id { get; set; }
        public string ContentValue { get; set; }
        public List<ContentDetails> ContentViewList = new List<ContentDetails>();
        public List<SelectListItem> ContentList = new List<SelectListItem>();
    }
    public class ContentDetails
    {
        public int StaticContentId { get; set; }
        public string StaticContentName { get; set; }
        public string ColumnName { get; set; }
    }
}