﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Areas.Admin.Models
{
    public class GeneralSettingModel
    {
        public int SettingId { get; set; }

        [Display(Name = "SMTP Host")]
        [StringLength(50)]
        [Required]
        public string SMTPServer { get; set; }

        [Display(Name = "SMTP Port")]
        [Required]
        public int SMTPPort { get; set; }

        [Display(Name = "Mail From")]
        [Required]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Email is not valid")]
        [StringLength(100)]
        public string FromEmail { get; set; }

        [Display(Name = "Email Password")]
        [Required]
        [StringLength(100)]
        public string EmailPassword { get; set; }

        [Display(Name = "SSL Required")]
        public bool SSLSecurity { get; set; }

        [Display(Name = "SMS Link")]
        [Required]
        public string SMSLink { get; set; }

        [Display(Name = "SMS Id")]
        [Required]
        public string SMSId { get; set; }

        [Display(Name = "SMS Password")]
        [Required]
        public string SMSPassword { get; set; }
         
        [Display(Name = "Profile Pic Size (Max.)")]
        [Required]
        public int MaxProfilePicSize { get; set; }

        [Display(Name = "No. Of Photos (Max.)")]
        [Required]
        public int MaxNoOfPhoto { get; set; }

        [Display(Name = "Photo Size (Max.)")]
        [Required]
        public int MaxPhotoSize { get; set; }

        [Display(Name = "No Of Audio (Max.)")]
        [Required]
        public int MaxNoOfAudio { get; set; }

        [Display(Name = "Audio Size (Max.)")]
        [Required]
        public int MaxAudioSize { get; set; }

        [Display(Name = "Cover Pic Size (Max.)")]
        [Required]
        public int MaxCoverPicSize { get; set; }

        [Display(Name = "No Of Video (Max.)")]
        [Required]
        public int MaxNoOfVideo { get; set; }

        public bool MobileVerification { get; set; }


    }
}