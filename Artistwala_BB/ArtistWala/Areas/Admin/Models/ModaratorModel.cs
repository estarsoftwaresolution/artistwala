﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Areas.Admin.Models
{
    public class ModaratorModel
    {
        [Required]
        public int ModaratorId { get; set; }

        [Display(Name = "Name")]
        [Required]
        public string ModaratorName { get; set; }

        [Display(Name = "Date Of Birth")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? DateOfBirth { get; set; }

        [StringLength(10)]
        public string Sex { get; set; }

        [Display(Name = "House Number")]
        [StringLength(50)]
        public string HouseNumber { get; set; }

        [StringLength(350)]
        public string Address { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(50)]
        public string State { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(10)]
        public string Pin { get; set; }

        [StringLength(100)]       
        [Required]       
        public string Email { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [Required]
        [StringLength(20)]
        public string Mobile { get; set; }

        public bool IsActive { get; set; }

        public DateTime AddedOn { get; set; }

        public int? FK_UserId { get; set; }

        [Display(Name = "User Name")]
        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [Display(Name = "Password")]
        [Required]
        [StringLength(50)]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        [Display(Name = "Confirm Password")]
        [Required]
        [DataType(DataType.Password)]
        [StringLength(50)]
        [Compare("Password", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword { get; set; }
    }
}