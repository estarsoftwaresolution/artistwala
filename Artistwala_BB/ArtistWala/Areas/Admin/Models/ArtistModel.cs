﻿using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Areas.Admin.Models
{
    public class ArtistModel
    {

        public int ArtistId { get; set; }

        [Required]
        [Display(Name = "User Name")]
        [StringLength(100)]
        public string UserName { get; set; }

        [Display(Name = "Artist Name")]
        [Required]
        [StringLength(150)]
        public string ArtistName { get; set; }

        [Display(Name = "Date Of Birth")]
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        [StringLength(10)]
        public string Sex { get; set; }

        [Display(Name = "House Number")]        
        [StringLength(50)]
        public string HouseNumber { get; set; }

        [Required]
        [StringLength(350)]
        public string Address { get; set; }

        [Required]
        [StringLength(50)]
        public string City { get; set; }

        [Required]
        [StringLength(50)]
        public string State { get; set; }

        [Required]
        [StringLength(50)]
        public string Country { get; set; }
               
        [StringLength(10)]       
        public string Pin { get; set; }

        [Required]
        [StringLength(100)]
        [DataType(DataType.EmailAddress)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Email is not valid")]
        public string Email { get; set; }

        [StringLength(300)]
        public string Website { get; set; }

        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public string Mobile { get; set; }

        [Required]
        public bool IsActive { get; set; }

        public DateTime AddedOn { get; set; }

        [Display(Name = "About Me")]
        [Required]
        [StringLength(500)]
        public string AboutMe { get; set; }

        public int? FK_UserId { get; set; }

        [Display(Name = "Select User Type")]
        public bool? IsGroup { get; set; }

        [Display(Name = "Featured")]
        public bool IsFeatured { get; set; }

        [Display(Name = "Minimum Rates")]
        [Required]
        public decimal MinmumRates { get; set; }

        [Display(Name = "Facebook Fan Link")]
        [StringLength(300)]
        public string FB_FanLink { get; set; }

        public List<tblArtistGroupMember> groupMember { get; set; }
    }
}