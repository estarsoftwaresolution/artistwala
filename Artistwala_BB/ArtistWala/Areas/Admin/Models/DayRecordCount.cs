﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtistWala.Areas.Admin.Models
{
    public class DayRecordCount
    {
        public int Day { get; set; }

        public string Name { get; set; }

        public string Month { get; set; }

        public int count { get; set; }
    }
}