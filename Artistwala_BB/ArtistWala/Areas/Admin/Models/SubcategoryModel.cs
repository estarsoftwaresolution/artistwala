﻿using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Areas.Admin.Models
{
    public class SubcategoryModel
    {
        public int SubCategoryId { get; set; }

        public List<tblArtistCategory> objCategory { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        [StringLength(40)]
        [Display(Name = "Subcategory Name")]
        public string SubCategoryName { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        public List<tblArtistSubCategory> objSubCategory { get; set; }
    }
}