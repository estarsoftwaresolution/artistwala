﻿using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtistWala.Areas.Admin.Models
{
    public class MessageModel
    {
        public int MessageId { get; set; }
        public Nullable<int> Fk_QuoteId { get; set; }
        public string MessageSubject { get; set; }
        public string MessageBody { get; set; }
        public string MessageType { get; set; }
        public Nullable<int> Fk_UserId { get; set; }
        public Nullable<int> SendBy { get; set; }
        public string SendByPic { get; set; }
        public string ReplyByPic { get; set; }
        public Nullable<bool> IsUnread { get; set; }
        public Nullable<bool> Approved { get; set; }
        public Nullable<int> MessageParentId { get; set; }
        public Nullable<System.DateTime> MessageDate { get; set; }
        public List<tblUserMessage> ReplayList { get; set; }
        public virtual tblUser tblUser { get; set; }
    }
}