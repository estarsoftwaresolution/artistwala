﻿using ArtistWala.Areas.Admin.Models;
using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ArtistWala.Areas.Admin.Controllers
{
    [CustomAuthorization(LoginPage = "~/Admin/Dashboard/AdminLogin")]
    public class MessagesController : Controller
    {
        ArtistWalaEntities context = new ArtistWalaEntities();
        //
        // GET: /Admin/Messages/
        public JsonResult SendReplayMessage(string id, string MessageBody)
        {
            FormsIdentity frmTicket = (FormsIdentity)User.Identity;
            FormsAuthenticationTicket ticket = frmTicket.Ticket;
            int accountID = Convert.ToInt32(ticket.UserData);

            tblUserMessage oMessages = new tblUserMessage();
            int MessageId = Convert.ToInt32(id);
            var MessageDetails = context.tblUserMessages.Where(x => x.MessageId == MessageId).Select(s => s).FirstOrDefault();
            if (MessageDetails != null)
            {
                oMessages.SendBy = accountID;
                oMessages.Fk_UserId = MessageDetails.SendBy;
                oMessages.MessageBody = MessageBody;
                oMessages.MessageType = MessageDetails.MessageType;
                oMessages.MessageSubject = MessageDetails.MessageSubject;
                oMessages.IsUnread = false;
                oMessages.MessageParentId = MessageDetails.MessageParentId;
                oMessages.MessageDate = System.DateTime.Now;
                context.tblUserMessages.Add(oMessages);
                context.SaveChanges();
                SendMessageSMS(oMessages.Fk_UserId);
            }
            else
            {
                ViewBag.errorMessage = "Something is wrong!";
            }
            return Json("Success");
        }
        public JsonResult GetMessageById(string id)
        {
            int MessageId = Convert.ToInt32(id);
            var Message = context.tblUserMessages.Where(x => x.MessageId == MessageId).Select(s => s.MessageBody).ToArray();
            return Json(Message);
        }
        public ActionResult ArtistMessage(int id = 0, string Name = "")
        {
            FormsIdentity frmTicket = (FormsIdentity)User.Identity;
            FormsAuthenticationTicket ticket = frmTicket.Ticket;
            int accountID = Convert.ToInt32(ticket.UserData);

            List<MessageModel> objMessage = new List<MessageModel>();
            string SendByName = string.Empty;
            string ReplayByNmae = string.Empty;

            if (id > 0)
            {
                objMessage = context.tblUserMessages
                    .Where(x => x.Fk_UserId == id && x.SendBy == accountID && x.MessageParentId==null).OrderByDescending(o => o.MessageId)
                    .OrderByDescending(o=>o.MessageId)
                    .Select(s => new MessageModel { 
                    Fk_UserId = s.Fk_UserId,
                    IsUnread = s.IsUnread,
                    MessageBody = s.MessageBody,
                    MessageDate = s.MessageDate,
                    MessageId=s.MessageId,
                    MessageParentId = s.MessageParentId,
                    MessageSubject = s.MessageSubject,
                    MessageType = s.MessageType,
                    ReplayList = context.tblUserMessages.Where(p => p.MessageParentId != null && s.MessageId == p.MessageParentId).Select(v => v).OrderBy(o=>o.MessageId).ToList(),
                    SendBy = s.SendBy,
                    tblUser = s.tblUser
                    }).ToList();
                foreach (var item in objMessage)
                {
                    SendByName = SendByName + context.tblUsers.Where(x => x.UserId == item.SendBy).Select(s => s.UserName).FirstOrDefault() +",";
                    foreach (var ReplyName in item.ReplayList)
                    {
                        ReplayByNmae = ReplayByNmae + context.tblUsers.Where(x => x.UserId == ReplyName.SendBy).Select(s => s.UserName).FirstOrDefault() + ",";
                        ViewBag.ReplayByNmae = ReplayByNmae;
                    }
                }
                ViewBag.SendByName = SendByName;
            }
            else if (id == 0 && accountID > 0)
            {
                objMessage = context.tblUserMessages
                    .Where(x =>x.MessageParentId==null && (x.SendBy == accountID || x.Fk_UserId == accountID))
                    .OrderByDescending(o => o.MessageId)
                    .Select(s => new MessageModel {
                        Fk_UserId = s.Fk_UserId,
                        IsUnread = s.IsUnread,
                        MessageBody = s.MessageBody,
                        MessageDate = s.MessageDate,
                        MessageId = s.MessageId,
                        MessageParentId = s.MessageParentId,
                        MessageSubject = s.MessageSubject,
                        MessageType = s.MessageType,
                        ReplayList = context.tblUserMessages.Where(p => p.MessageParentId != null && s.MessageId == p.MessageParentId).Select(v => v).OrderBy(o => o.MessageId).ToList(),
                        SendBy = s.SendBy, 
                        tblUser = s.tblUser
                    }).ToList();
                foreach (var item in objMessage)
                {
                    SendByName = SendByName + context.tblUsers.Where(x => x.UserId == item.SendBy).Select(s => s.UserName).FirstOrDefault() + ",";
                    foreach (var ReplyName in item.ReplayList)
                    {
                        ReplayByNmae = ReplayByNmae + context.tblUsers.Where(x => x.UserId == ReplyName.SendBy).Select(s => s.UserName).FirstOrDefault() + ",";
                        ViewBag.ReplayByNmae = ReplayByNmae;
                    }
                    ViewBag.SendByName = SendByName;
                }
            }

            Session[SessionConstants.ArtistId] = id;
            Session[SessionConstants.UserId] = accountID;
            ViewBag.SendByName = SendByName;
            ViewBag.ReplayByNmae = ReplayByNmae;
            ViewBag.ArtistName = Name;
            return View(objMessage);
        }

        [HttpPost]
        public ActionResult ArtistMessage(FormCollection fc)
        {
            FormsIdentity frmTicket = (FormsIdentity)User.Identity;
            FormsAuthenticationTicket ticket = frmTicket.Ticket;
            int accountID = Convert.ToInt32(ticket.UserData);

            tblUserMessage objMessage = new tblUserMessage()
            {
                MessageBody = fc["txtMessage"],
                MessageSubject = fc["txtMessageSubject"],
                MessageType = "contact",
                SendBy = accountID,
                Fk_UserId = Convert.ToInt32(Session[SessionConstants.ArtistId])
            };

            context.tblUserMessages.Add(objMessage);
            context.SaveChanges();
            SendMessageSMS(objMessage.Fk_UserId);

            return RedirectToAction("ArtistMessage", "Messages", new { Area = "Admin" });
        }
        private void SendMessageSMS(int? UserId)
        {
            SMSCAPI oSMSCAPI = new SMSCAPI();
            string MobileNo = context.tblClients.Where(x => x.FK_UserId == UserId).Select(s => s.ContactNo).FirstOrDefault();
            var UnreadMessage = context.tblUserMessages.Where(x => x.Fk_UserId == UserId && x.IsUnread == false).ToList();
            string MessageCount = Convert.ToString(UnreadMessage.Count);
            tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 14).FirstOrDefault();
            string MessageBody = SMSTo.SMSBody.Replace("@MessageCount", MessageCount);
            //string MessageBody = "Hi, you have " + MessageCount + " new message in your inbox of Artistwala.com.";
            if (MobileNo != null && MobileNo != "")
            {
                oSMSCAPI.SendSMS(MobileNo, MessageBody);
            }
        }
    }
}