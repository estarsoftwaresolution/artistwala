﻿using ArtistWala.Common;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArtistWala.Areas.Admin.Controllers
{
    [CustomAuthorization(LoginPage = "~/Admin/Dashboard/AdminLogin")]
    public class QuotesController : Controller
    {
        ArtistWalaEntities context = new ArtistWalaEntities();
        //
        // GET: /Admin/Quotes/
        public ActionResult Index()
        {
            List<tblQuote> objQuote = context.tblQuotes.OrderByDescending(x => x.AddedDate).ToList();
            return View(objQuote);
        }

        public ActionResult Details(int id)
        {
            var Fk_UserId = context.tblUserMessages.Where(x => x.Fk_QuoteId == id).Select(s=>s.Fk_UserId).FirstOrDefault();
            var ArtistId = context.tblArtists.Where(x => x.FK_UserId == Fk_UserId).Select(s=>s.ArtistId).FirstOrDefault();

            tblQuote objQuote = context.tblQuotes.Where(x => x.QuoteId == id).FirstOrDefault();
            objQuote.SubCategoryName = (from s in context.tblArtistSubCategories
                                        join cm in context.tblArtistCategoryMappings on s.ArtistSubCategoryId equals cm.FK_SubCategoryId
                                        where cm.FK_ArtistId == ArtistId
                                        select s).ToList();
            return View(objQuote);
        }

        public ActionResult UnAssignModarator(int id)
        {
            tblQuote objQuotes = context.tblQuotes.Find(id);
            objQuotes.AssignTo = null;

            context.Entry(objQuotes).State = EntityState.Modified;
            context.SaveChanges();

            return RedirectToAction("Index", "Quotes", new { Area = "Admin" });
        }
    }
}