﻿using ArtistWala.Areas.Admin.Models;
using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;

namespace ArtistWala.Areas.Admin.Controllers
{
    [CustomAuthorization(LoginPage = "~/Admin/Dashboard/AdminLogin")]
    public class ArtistController : Controller
    {
        ArtistWalaEntities context = new ArtistWalaEntities();
        CommonMethods objCommon = new CommonMethods();
        //
        // GET: /Admin/Artist/
        public ActionResult List(int Status = 0)
        {
            try
            {
                Session["ArtistId"] = null;
                Session["EditArtistId"] = null;

                switch (Status)
                {
                    case 1:
                        ViewBag.Message = "The record deleted successfully.";
                        break;
                    case -1:
                        ViewBag.ErrorMessage = "Sorry! Server error. Please try again.";
                        break;
                    case 2:
                        ViewBag.Message = "The artist has been successfully approved.";
                        break;
                    case 3:
                        ViewBag.Message = "The artist has been successfully rejected.";
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex) { }
            List<tblArtist> objArtist = context.tblArtists.OrderByDescending(x => x.AddedOn).ToList();
            return View(objArtist);
        }

        public ActionResult AddArtist()
        {
            ArtistModel objModel = new ArtistModel();
            try
            {
                int ArtistId = default(int);

                if (!string.IsNullOrEmpty(Convert.ToString(Session["ArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["ArtistId"] ?? 0), out ArtistId);
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["EditArtistId"] ?? 0), out ArtistId);
                }


                tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == ArtistId).FirstOrDefault();
                if (objArtist != null)
                {
                    objModel.ArtistName = objArtist.ArtistName;
                    objModel.UserName = objArtist.tblUser.UserName;
                    objModel.DateOfBirth = objArtist.DateOfBirth;
                    objModel.Sex = objArtist.Sex;
                    objModel.HouseNumber = objArtist.HouseNumber;
                    objModel.Address = objArtist.Address;
                    objModel.City = objArtist.City;
                    objModel.State = objArtist.State;
                    objModel.Country = objArtist.Country;
                    objModel.Pin = objArtist.Pin;
                    objModel.Email = objArtist.Email;
                    objModel.Website = objArtist.Website;
                    objModel.Phone = objArtist.Phone;
                    objModel.Mobile = objArtist.Mobile;
                    objModel.AboutMe = objArtist.AboutMe;
                    objModel.FK_UserId = objArtist.FK_UserId;
                    objModel.MinmumRates = (objArtist.MinmumRates ?? 0);
                    objModel.IsFeatured = (objArtist.IsFeatured ?? false);
                    objModel.FB_FanLink = objArtist.FB_FanLink;
                    objModel.IsGroup = objArtist.IsGroup;
                    objModel.IsActive = (objArtist.IsActive ?? false);
                    objModel.groupMember = new List<tblArtistGroupMember>();
                    objModel.groupMember = objArtist.tblArtistGroupMembers.ToList();
                    
                }
            }
            catch (Exception ex) { }
            return PartialView(objModel);
        }

        [HttpPost]
        public ActionResult AddArtist(ArtistModel objModel, FormCollection fc)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    tblUser objUser = context.tblUsers.Where(x => x.UserId != objModel.FK_UserId).FirstOrDefault();
                    tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId != objModel.ArtistId).FirstOrDefault();
                    tblClient objClient = context.tblClients.Where(x => x.FK_UserId != objModel.FK_UserId).FirstOrDefault();
                    if (objUser != null && objArtist != null && objClient!=null)
                    {
                        UpdateArtistDetails(objModel, Convert.ToInt32(Session["EditArtistId"]), fc);
                    }
                    else
                    {
                        return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -5, tab = "a1" });
                    }
                }
                else
                {
                    tblUser objUser = context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == objModel.UserName.Trim().ToUpper()).FirstOrDefault();
                    tblArtist objArtist = context.tblArtists.Where(x => x.Email.Trim().ToUpper() == objModel.Email.Trim().ToUpper()).FirstOrDefault();
                    tblClient objClient = context.tblClients.Where(x => x.Email.Trim().ToUpper() == objModel.Email.Trim().ToUpper()).FirstOrDefault();
                    if (objUser == null && objArtist == null && objClient==null)
                    {
                        AddArtistDetails(objModel, fc);
                    }
                    else
                    {
                        return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["ArtistId"]), Status = -5, tab = "a1" });
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -4, tab = "a1" });
                }
                else
                {
                    return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -4, tab = "a1" });
                }
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
            {
                return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = 2, tab = "a2" });
            }
            else
            {
                return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["ArtistId"]), Status = 1, tab = "a2" });
            }
        }

        public ActionResult AddProfilePicture()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult AddProfilePicture(FormCollection fC)
        {
            try
            {
                int ArtistId = default(int);

                if (!string.IsNullOrEmpty(Convert.ToString(Session["ArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["ArtistId"] ?? 0), out ArtistId);
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["EditArtistId"] ?? 0), out ArtistId);
                }

                if (!string.IsNullOrEmpty(fC["hidCoordinates"]))
                {
                    int xAxis = Convert.ToInt32(fC["hidCoordinates"].Split('|')[0]);
                    int yAxis = Convert.ToInt32(fC["hidCoordinates"].Split('|')[1]);
                    int width = Convert.ToInt32(fC["hidCoordinates"].Split('|')[2]);
                    int height = Convert.ToInt32(fC["hidCoordinates"].Split('|')[3]);

                    Bitmap profImage = (Bitmap)Bitmap.FromFile(Server.MapPath(Convert.ToString(Session[SessionConstants.ProfilImage])));
                    Bitmap CroppedImage = CommonMethods_bak.CropImage(profImage, xAxis, yAxis, width, height);
                    string Name = Guid.NewGuid() + ".jpg";

                    profImage.Dispose();

                    var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

                    if (!System.IO.File.Exists(path))
                    {
                        CroppedImage.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                        System.IO.File.Delete(Server.MapPath(Convert.ToString(Session[SessionConstants.ProfilImage])));

                        Session[SessionConstants.ProfilImage] = Path.Combine(ConfigurationManager.AppSettings["ArtistImagePath"], Name);
                    }

                    tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == ArtistId).FirstOrDefault();

                    tblUser objUser = context.tblUsers.Where(x => x.UserId == objArtist.FK_UserId).FirstOrDefault();

                    objUser.ProfileImage = Name;
                    objUser.IsHoldRights = false;

                    context.Entry(objUser).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else
                {
                    string Name = SaveImagesinServer("FILE2");
                    Session[SessionConstants.ProfilImage] = ConfigurationManager.AppSettings["ArtistImagePath"] + Name;
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -4, tab = "a2" });
                }
                else
                {
                    return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -4, tab = "a2" });
                }
            }

            string SelectedTab = "";
            if (!string.IsNullOrEmpty(Convert.ToString(Session[SessionConstants.ProfilImage])))
            {
                SelectedTab = "a2";
            }
            else
            {
                SelectedTab = "a3";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
            {
                return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = 4, tab = SelectedTab });
            }
            else
            {
                return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = 4, tab = SelectedTab });
            }
        }

        public ActionResult AddCoverPicture()
        {
            return PartialView();
        }


        [HttpPost]
        public ActionResult AddCoverPicture(FormCollection fC)
        {           

            try
            {
                int ArtistId = default(int);

                if (!string.IsNullOrEmpty(Convert.ToString(Session["ArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["ArtistId"] ?? 0), out ArtistId);
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["EditArtistId"] ?? 0), out ArtistId);
                }

                if (!string.IsNullOrEmpty(fC["hidCoordinates1"]))
                {
                    int xAxis = Convert.ToInt32(fC["hidCoordinates1"].Split('|')[0]);
                    int yAxis = Convert.ToInt32(fC["hidCoordinates1"].Split('|')[1]);
                    int width = Convert.ToInt32(fC["hidCoordinates1"].Split('|')[2]);
                    int height = Convert.ToInt32(fC["hidCoordinates1"].Split('|')[3]);

                    Bitmap profImage = (Bitmap)Bitmap.FromFile(Server.MapPath(Convert.ToString(Session[SessionConstants.CoverImage])));
                    Bitmap CroppedImage = CommonMethods_bak.CropImage(profImage, xAxis, yAxis, width, height);
                    string Name = Guid.NewGuid() + ".jpg";

                    profImage.Dispose();

                    var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

                    if (!System.IO.File.Exists(path))
                    {
                        CroppedImage.Save(path, System.Drawing.Imaging.ImageFormat.Jpeg);
                        System.IO.File.Delete(Server.MapPath(Convert.ToString(Session[SessionConstants.CoverImage])));

                        Session[SessionConstants.CoverImage] = Path.Combine(ConfigurationManager.AppSettings["ArtistImagePath"], Name);
                    }

                    tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == ArtistId).FirstOrDefault();

                    tblUser objUser = context.tblUsers.Where(x => x.UserId == objArtist.FK_UserId).FirstOrDefault();

                    objUser.CoverImage = Name;
                    objUser.IsHoldRights = false;

                    context.Entry(objUser).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else
                {
                    string Name = SaveImagesinServer2("FILE2");
                    Session[SessionConstants.CoverImage] = ConfigurationManager.AppSettings["ArtistImagePath"] + Name;
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -4, tab = "a3" });
                }
                else
                {
                    return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -4, tab = "a3" });
                }
            }

            string SelectedTab = "";
            if (!string.IsNullOrEmpty(Convert.ToString(Session[SessionConstants.CoverImage])))
            {
                SelectedTab = "a3";
            }
            else
            {
                SelectedTab = "a4";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
            {
                return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = 4, tab = SelectedTab });
            }
            else
            {
                return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = 4, tab = SelectedTab });
            }
        }

        public PartialViewResult AddArtistCategory()
        {
            int id = default(int);
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ArtistId"])))
            {
                int.TryParse(Convert.ToString(Session["ArtistId"] ?? 0), out id);
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
            {
                int.TryParse(Convert.ToString(Session["EditArtistId"] ?? 0), out id);
            }
            List<tblArtistCategoryMapping> objSubCategoryList = context.tblArtistCategoryMappings.Where(x => x.FK_ArtistId == id).ToList();

            try
            {
                objSubCategoryList = context.tblArtistCategoryMappings.Where(x => x.FK_ArtistId == id).ToList();
            }
            catch (Exception ex) { }
            return PartialView(objSubCategoryList);
        }

        [HttpPost]
        public ActionResult AddArtistCategory(FormCollection fc)
        {
            List<tblArtistCategoryMapping> objSubCategoryList = new List<tblArtistCategoryMapping>();
            try
            {
                int id = default(int);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["ArtistId"] ?? 0), out id);
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["EditArtistId"] ?? 0), out id);
                }

                int count = default(int);

                int.TryParse(fc["hidSubcatCount"], out count);

                for (int i = 1; i <= count; i++)
                {
                    int catId = default(int);
                    int subCatId = default(int);

                    int.TryParse(fc["ddlCategory"], out catId);
                    int.TryParse(fc["chkSubCat" + i], out subCatId);

                    if (catId > 0 && subCatId > 0)
                    {
                        tblArtistCategoryMapping objMapping = new tblArtistCategoryMapping()
                        {
                            FK_ArtistId = id,
                            FK_CategoryId = catId,
                            FK_SubCategoryId = subCatId
                        };

                        context.tblArtistCategoryMappings.Add(objMapping);
                    }
                }

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -4, tab = "a4" });
                }
                else
                {
                    return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -4, tab = "a4" });
                }
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
            {
                return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = 2, tab = "a5" });
            }
            else
            {
                return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = 2, tab = "a5" });
            }
        }

        public PartialViewResult AddPictures()
        {
            List<tblArtistPortfolio> listimage = new List<tblArtistPortfolio>();
            try
            {
                int id = default(int);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["ArtistId"] ?? 0), out id);
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["EditArtistId"] ?? 0), out id);
                }

                listimage = context.tblArtistPortfolios.Where(x => x.FK_Artist == id).ToList();
            }
            catch (Exception ex) { }
            return PartialView(listimage);
        }

        [HttpPost]
        public ActionResult AddPictures(FormCollection fc)
        {
            List<tblArtistPortfolio> listimages = new List<tblArtistPortfolio>();
            try
            {
                int ArtistId = default(int);

                if (!string.IsNullOrEmpty(Convert.ToString(Session["ArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["ArtistId"] ?? 0), out ArtistId);
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["EditArtistId"] ?? 0), out ArtistId);
                }

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var Photo = Request.Files[i];
                    if (Photo.ContentType.Contains("image"))
                    {
                        var fileName = Photo.FileName;
                        string ext = Path.GetExtension(fileName);
                        string Name = Guid.NewGuid() + ext;
                        var newFileName = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

                        var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), fileName);
                        Photo.SaveAs(path);

                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Copy(path, newFileName);
                            System.IO.File.Delete(path);
                        }

                        if (System.IO.File.Exists(newFileName))
                        {
                            tblArtistPortfolio objPhoto = new tblArtistPortfolio();
                            objPhoto.FileName = Name;
                            objPhoto.FK_Artist = ArtistId;
                            objPhoto.FileType = "image";
                            objPhoto.Url = "";
                            objPhoto.AddedOn = DateTime.Now;
                            objPhoto.Title = fc["image_caption"];
                            context.tblArtistPortfolios.Add(objPhoto);
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                        {
                            return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -4, tab = "a5" });
                        }
                        else
                        {
                            return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -4, tab = "a5" });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -4, tab = "a5" });
                }
                else
                {
                    return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -4, tab = "a5" });
                }
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
            {
                return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = 4, tab = "a6" });
            }
            else
            {
                return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = 4, tab = "a6" });
            }
        }

        public PartialViewResult AddAudios()
        {
            List<tblArtistPortfolio> listaudio = new List<tblArtistPortfolio>();
            try
            {
                int id = default(int);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["ArtistId"] ?? 0), out id);
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["EditArtistId"] ?? 0), out id);
                }

                listaudio = context.tblArtistPortfolios.Where(x => x.FK_Artist == id).ToList();
            }
            catch (Exception ex) { }
            return PartialView(listaudio);
        }

        [HttpPost]
        public ActionResult AddAudios(FormCollection fc)
        {
            List<tblArtistPortfolio> listaudio = new List<tblArtistPortfolio>();
            try
            {
                int ArtistId = default(int);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["ArtistId"] ?? 0), out ArtistId);
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["EditArtistId"] ?? 0), out ArtistId);
                }

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var Audio = Request.Files[i];
                    if (Audio.ContentType.Contains("audio"))
                    {
                        var fileName = Audio.FileName;
                        string ext = Path.GetExtension(fileName);
                        string Name = Guid.NewGuid() + ext;
                        var newFileName = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

                        var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), fileName);
                        Audio.SaveAs(path);

                        if (System.IO.File.Exists(path))
                        {
                            System.IO.File.Copy(path, newFileName);
                            System.IO.File.Delete(path);
                        }

                        if (System.IO.File.Exists(newFileName))
                        {
                            tblArtistPortfolio objAudio = new tblArtistPortfolio();
                            objAudio.FileName = Name;
                            objAudio.FK_Artist = ArtistId;
                            objAudio.FileType = "audio";
                            objAudio.Url = "";
                            objAudio.AddedOn = DateTime.Now;
                            objAudio.Title = fc["audio_title"];
                            context.tblArtistPortfolios.Add(objAudio);
                            context.SaveChanges();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                        {
                            return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -3, tab = "a6" });
                        }
                        else
                        {
                            return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -3, tab = "a6" });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -4, tab = "a6" });
                }
                else
                {
                    return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -4, tab = "a6" });
                }
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
            {
                return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = 4, tab = "a7" });
            }
            else
            {
                return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = 4, tab = "a7" });
            }
        }

        public PartialViewResult AddVideo()
        {
            List<tblArtistPortfolio> listvideo = new List<tblArtistPortfolio>();
            try
            {
                int id = default(int);
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["ArtistId"] ?? 0), out id);
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["EditArtistId"] ?? 0), out id);
                }

                listvideo = context.tblArtistPortfolios.Where(x => x.FK_Artist == id).ToList();
            }
            catch (Exception ex) { }
            return PartialView(listvideo);
        }

        [HttpPost]
        public ActionResult AddVideo(FormCollection fc)
        {
            List<tblArtistPortfolio> listvideo = new List<tblArtistPortfolio>();
            try
            {
                int ArtistId = default(int);

                if (!string.IsNullOrEmpty(Convert.ToString(Session["ArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["ArtistId"] ?? 0), out ArtistId);
                }
                else if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    int.TryParse(Convert.ToString(Session["EditArtistId"] ?? 0), out ArtistId);
                }
                string video_url = string.Empty;
                if (ModelState.IsValid)
                {
                    if (fc["video_url"].Split('?').Length > 1)
                    {
                        string queryString = fc["video_url"].Split('?')[1];

                        if (!string.IsNullOrEmpty(queryString))
                        {
                            video_url = "http://www.youtube.com/embed/" + queryString.Replace("v=", "");
                        }
                    }

                    tblArtistPortfolio objVideo = new tblArtistPortfolio();
                    objVideo.FileName = "";
                    objVideo.FK_Artist = ArtistId;
                    objVideo.FileType = "video";
                    objVideo.Url = video_url;
                    objVideo.AddedOn = DateTime.Now;
                    objVideo.Title = fc["Video_title"];
                    context.tblArtistPortfolios.Add(objVideo);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -4, tab = "a7" });
                }
                else
                {
                    return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -4, tab = "a7" });
                }
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
            {
                return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = 4, tab = "a7" });
            }
            else
            {
                return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = 4, tab = "a7" });
            }
        }

        public ActionResult DeleteArtistPortfolio(int id, string selectTab = "")
        {
            try
            {
                tblArtistPortfolio objPhoto = context.tblArtistPortfolios.Where(x => x.PortfolioId == id).FirstOrDefault();

                context.tblArtistPortfolios.Remove(objPhoto);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -1, tab = selectTab });
                }
                else
                {
                    return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -1, tab = selectTab });
                }
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
            {
                return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = 3, tab = selectTab });
            }
            else
            {
                return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = 3, tab = selectTab });
            }
        }

        public ActionResult AddArtistDetails(int Status = 0, string tab = "")
        {
            Session["EditArtistId"] = null;
            switch (Status)
            {
                case 1:
                    ViewBag.Message = "Record saved successfully.";
                    break;
                case 2:
                    ViewBag.Message = "Record updated successfully.";
                    break;
                case 3:
                    ViewBag.Message = "The record deleted successfully.";
                    break;
                case 4:
                    ViewBag.Message = "Your file is Uploaded Successfully.";
                    break;
                case -1:
                    ViewBag.ErrorMessage = "Sorry! Server error. Please try again.";
                    break;
                case -2:
                    ViewBag.ErrorMessage = "Sorry! Invalid youtube link. Please add embeded video.";
                    break;
                case -3:
                    ViewBag.ErrorMessage = "Sorry! Invalid file type.";
                    break;
                case -4:
                    ViewBag.ErrorMessage = "Sorry! Invalid input. Please verify your data.";
                    break;
                case -5:
                    ViewBag.ErrorMessage = "Sorry! This username/email already used by another user. Please try different.";
                    break;
                default:
                    break;
            }

            return View();
        }

        public ActionResult EditArtist(int id, int Status = 0, string tab = "")
        {
            ArtistModel objModel = new ArtistModel();
            try
            {

                tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == id).FirstOrDefault();
                if (objArtist != null)
                {
                    objModel = new ArtistModel()
                    {
                        ArtistId = objArtist.ArtistId,
                        ArtistName = objArtist.ArtistName,
                        DateOfBirth = objArtist.DateOfBirth,
                        Sex = objArtist.Sex,
                        HouseNumber = objArtist.HouseNumber,
                        Address = objArtist.Address.Trim(),
                        City = objArtist.City.Trim(),
                        State = objArtist.State.Trim(),
                        Country = objArtist.Country.Trim(),
                        Pin = objArtist.Pin,
                        Email = objArtist.Email,
                        Website = objArtist.Website,
                        Phone = objArtist.Phone,
                        Mobile = objArtist.Mobile,
                        AboutMe = objArtist.AboutMe,
                        FK_UserId = objArtist.FK_UserId
                    };
                    //objArtist.ArtistName = objModel.ArtistName;
                    //objArtist.DateOfBirth = objModel.DateOfBirth;
                    //objArtist.Sex = objModel.Sex;
                    //objArtist.HouseNumber = objModel.HouseNumber;
                    //objArtist.Address = objModel.Address;
                    //objArtist.City = objModel.City;
                    //objArtist.State = objModel.State;
                    //objArtist.Country = objModel.Country;
                    //objArtist.Pin = objModel.Pin;
                    //objArtist.Email = objModel.Email;
                    //objArtist.Website = objModel.Website;
                    //objArtist.Phone = objModel.Phone;
                    //objArtist.Mobile = objModel.Mobile;
                    //objArtist.AboutMe = objModel.AboutMe;
                    //objArtist.FK_UserId = objModel.FK_UserId;
                    //context.Entry(objArtist).State = EntityState.Modified;
                    //context.SaveChanges();

                }

                Session["EditArtistId"] = id;

                switch (Status)
                {
                    case 1:
                        ViewBag.Message = "Artist record saved successfully.";
                        break;
                    case 2:
                        ViewBag.Message = "Artist record updated successfully.";
                        break;
                    case 3:
                        ViewBag.Message = "The record deleted successfully.";
                        break;
                    case 4:
                        ViewBag.Message = "Your file is Uploaded Successfully.";
                        break;
                    case -1:
                        ViewBag.ErrorMessage = "Sorry! Server error. Please try again.";
                        break;
                    case -2:
                        ViewBag.ErrorMessage = "Sorry! Invalid youtube link. Please add embeded video.";
                        break;
                    case -3:
                        ViewBag.ErrorMessage = "Sorry! Invalid file type.";
                        break;
                    case -4:
                        ViewBag.ErrorMessage = "Sorry! Invalid input. Please verify your data.";
                        break;
                    case -5:
                        ViewBag.ErrorMessage = "Sorry! This username already taken by another user. Please try different.";
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex) { }
            return View(objModel);
        }

        //private string SaveImagesinServer(string file)
        //{
        //    var profileImage = Request.Files[file];
        //    var fileName = profileImage.FileName;
        //    string ext = Path.GetExtension(fileName);
        //    string Name = Guid.NewGuid() + ext;
        //    var newFileName = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

        //    var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), fileName);
        //    profileImage.SaveAs(path);

        //    if (System.IO.File.Exists(path))
        //    {
        //        System.IO.File.Copy(path, newFileName);
        //        Session[SessionConstants.ProfilImage] = newFileName;
        //        Session[SessionConstants.ImageName] = Name;

        //        System.IO.File.Delete(path);
        //    }
        //    return Name;
        //}

        private string SaveImagesinServer(string file)
        {
            var profileImage = Request.Files[file];
            WebImage img = new WebImage(profileImage.InputStream);

            int imgWidth = img.Width;

            if (imgWidth > 1024)
            {
                img.Resize(1024, 768, true);
            }

            var fileName = profileImage.FileName;
            //var fileName = img.FileName;
            string ext = Path.GetExtension(fileName);
            string Name = Guid.NewGuid() + ext;
            var newFileName = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

            var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), fileName);
            //profileImage.SaveAs(path);
            img.Save(path);

            if (System.IO.File.Exists(path))
            {
                System.IO.File.Copy(path, newFileName);
                Session[SessionConstants.ProfilImage] = newFileName;
                Session[SessionConstants.ImageName] = Name;

                System.IO.File.Delete(path);
            }
            return Name;
        }


        private string SaveImagesinServer2(string file)
        {
            var profileImage = Request.Files[file];
            WebImage img = new WebImage(profileImage.InputStream);
            string Name;
            int imgWidth = img.Width;

            if (imgWidth > 1400)
            {
                img.Resize(1200, 900, true);

                var fileName = profileImage.FileName;
                //var fileName = img.FileName;
                string ext = Path.GetExtension(fileName);
                Name = Guid.NewGuid() + ext;
                var newFileName = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), Name);

                var path = Path.Combine(Server.MapPath("~" + ConfigurationManager.AppSettings["ArtistImagePath"]), fileName);
                //profileImage.SaveAs(path);
                img.Save(path);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Copy(path, newFileName);
                    Session[SessionConstants.CoverImage] = newFileName;
                    Session[SessionConstants.ImageName] = Name;

                    System.IO.File.Delete(path);
                }
                return Name;

            }
            else //(imgWidth < 1400)
            {

                Name = "Please upload image with high resolution";
                //ViewBag.Resize(Name);
                return Name;
            }
        }

        private void AddArtistDetails(ArtistModel objModel, FormCollection fc)
        {
            DateTime dob = DateTime.Parse(objModel.DateOfBirth.ToString(), CultureInfo.InvariantCulture);

            tblUser objUser = new tblUser()
            {
                UserName = objModel.UserName,
                Password = objCommon.GetAutogeneratedPassword(),
                IsActive = true,
                FK_RoleId = (int)ArtistWala.Models.AppsConstants.UserRoles.Artist,
                CreatedDate = DateTime.Now,
                Name = objModel.ArtistName
            };

            context.tblUsers.Add(objUser);
            context.SaveChanges();

            int UserId = objUser.UserId;
            // New Code for save in Client Table
            tblClient objClient = new tblClient()
            {
                ClientName = objModel.ArtistName,
                Email=objModel.Email,
                ContactNo=objModel.Mobile,
                FK_UserId=UserId,
                CreatedDate = DateTime.Now,
                IsActive = true,
            };
            context.tblClients.Add(objClient);
            context.SaveChanges();


            tblArtist objArtist = new tblArtist()
            {
                ArtistName = objModel.ArtistName,
                DateOfBirth = dob,
                Sex = objModel.Sex,
                HouseNumber = objModel.HouseNumber,
                Address = objModel.Address.Trim(),
                City = objModel.City.Trim(),
                State = objModel.State.Trim(),
                Country = objModel.Country.Trim(),
                Pin = objModel.Pin,
                Email = objModel.Email,
                Website = objModel.Website,
                Phone = objModel.Phone,
                Mobile = objModel.Mobile,
                AddedOn = DateTime.Now,
                AboutMe = objModel.AboutMe,
                FK_UserId = UserId,
                IsActive = objModel.IsActive,
                IsFeatured = objModel.IsFeatured,
                IsGroup = objModel.IsGroup,
                FB_FanLink = objModel.FB_FanLink,
                MinmumRates = objModel.MinmumRates
            };

            context.tblArtists.Add(objArtist);
            context.SaveChanges();

            int groupCount = default(int);

            int.TryParse(fc["hidGroupMember"], out groupCount);

            for (int i = 1; i <= groupCount; i++)
            {
                if (!string.IsNullOrEmpty(fc["txtGrMemberName" + i]))
                {
                    string Name = string.Empty;
                    if (!string.IsNullOrEmpty(fc["txtGrMemberName" + i]))
                    {
                        Name = SaveImagesinServer("txtGrMemberImg" + i);
                    }

                    tblArtistGroupMember objGroup = new tblArtistGroupMember()
                    {
                        FK_ArtistId = objArtist.ArtistId,
                        ImagePath = Name,
                        MemberName = fc["txtGrMemberName" + i],
                        MemberDescription = fc["txtGrDescription" + i],
                        IsActive = true
                    };

                    context.tblArtistGroupMembers.Add(objGroup);
                    context.SaveChanges();
                }
            }
            SendSMSNewUser(objModel);
            SendEmailNewUser(objModel); 
            Session["ArtistId"] = objArtist.ArtistId;
            ModelState.Clear();
        }

        private void SendEmailNewUser(ArtistModel oArtist)
        {
            tblUser objUser = new tblUser();
            objUser = context.tblUsers.Where(x => x.UserName == oArtist.UserName).FirstOrDefault();
            tblUserMessage objUserMessage = new tblUserMessage();
            AESEncryption oDecript = new AESEncryption();
            string DecriptedPassword = oDecript.DecryptPassword(objUser.Password);
            
            
            //tblEmailTemplate email = context.tblEmailTemplates.Where(x => x.TemplateId == 1).FirstOrDefault();
            //string emailBody = email.EmailBody.Replace("@Subject", objUserMessage.MessageSubject).Replace("@name", objUser.Name).Replace("@username", objUser.UserName).Replace("@password", DecriptedPassword);
            //objCommon.sendMailInHtmlFormat(email.AddCC, oArtist.Email, email.AddBCC, email.EmailSubject, emailBody);

            tblEmailTemplate email2 = context.tblEmailTemplates.Where(x => x.TemplateId == 2).FirstOrDefault();
            string emailBody2 = email2.EmailBody.Replace("@name", objUser.Name).Replace("@username", objUser.UserName).Replace("@password", DecriptedPassword); ;
            objCommon.sendMailInHtmlFormat(email2.AddCC, oArtist.Email, email2.AddBCC, email2.EmailSubject, emailBody2);
        }
        private void SendSMSNewUser(ArtistModel oArtist)
        {
            SMSCAPI oSMS = new SMSCAPI();
            tblUser objUser = new tblUser();
            AESEncryption oDecript = new AESEncryption();
            objUser = context.tblUsers.Where(x => x.UserName == oArtist.UserName).FirstOrDefault();
            string UserName = objUser.UserName;
            string Name = objUser.Name;
            string DecriptedPassword = oDecript.DecryptPassword(objUser.Password);
            tblSMSTemplate SMSTo = context.tblSMSTemplates.Where(x => x.TemplateId == 13).FirstOrDefault();
            string SMSBody = SMSTo.SMSBody.Replace("@Name", UserName).Replace("@Username", UserName).Replace("@Password", DecriptedPassword);
            //string SMSBody = "Dear " + Name + ", Your account is created and your username is " + UserName + " and password is " + DecriptedPassword + ". You can now login and enjoy our free services. Artistwala.com";
            oSMS.SendSMS(oArtist.Mobile, SMSBody);
        }
        private void UpdateArtistDetails(ArtistModel objModel, int ArtistId, FormCollection fc)
        {
            tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == ArtistId).FirstOrDefault();
            tblClient objClient = context.tblClients.Where(x => x.FK_UserId != objArtist.FK_UserId).FirstOrDefault();
            tblUser objUser = context.tblUsers.Where(x => x.UserId == objArtist.FK_UserId).FirstOrDefault();

            DateTime dob = DateTime.Parse(objModel.DateOfBirth.ToString(), CultureInfo.InvariantCulture);

            objUser.UserName = objModel.UserName;
            context.Entry(objUser).State = EntityState.Modified;
            context.SaveChanges();

            objClient.Email = objModel.Email;
            objClient.ContactNo = objModel.Mobile;
            context.Entry(objClient).State = EntityState.Modified;
            context.SaveChanges();


            objArtist.ArtistName = objModel.ArtistName;
            objArtist.DateOfBirth = dob;
            objArtist.Sex = objModel.Sex;
            objArtist.HouseNumber = objModel.HouseNumber;
            objArtist.Address = objModel.Address;
            objArtist.City = objModel.City;
            objArtist.State = objModel.State;
            objArtist.Country = objModel.Country;
            objArtist.Pin = objModel.Pin;
            objArtist.Email = objModel.Email;
            objArtist.Website = objModel.Website;
            objArtist.Phone = objModel.Phone;
            objArtist.Mobile = objModel.Mobile;
            objArtist.AboutMe = objModel.AboutMe;
            objArtist.FK_UserId = objModel.FK_UserId;
            objArtist.IsActive = objModel.IsActive;
            objArtist.IsFeatured = objModel.IsFeatured;
            objArtist.IsGroup = objModel.IsGroup;
            objArtist.FB_FanLink = objModel.FB_FanLink;
            objArtist.MinmumRates = objModel.MinmumRates;

            context.Entry(objArtist).State = EntityState.Modified;
            context.SaveChanges();

            int groupCount = default(int);

            int.TryParse(fc["hidGroupMember"], out groupCount);

            for (int i = 1; i <= groupCount; i++)
            {
                if (!string.IsNullOrEmpty(fc["txtGrMemberName" + i]))
                {
                    string Name = string.Empty;
                    if (!string.IsNullOrEmpty(fc["txtGrMemberName" + i]))
                    {
                        Name = SaveImagesinServer("txtGrMemberImg" + i);
                    }

                    tblArtistGroupMember objGroup = new tblArtistGroupMember()
                    {
                        FK_ArtistId = objArtist.ArtistId,
                        ImagePath = Name,
                        MemberName = fc["txtGrMemberName" + i],
                        MemberDescription = fc["txtGrDescription" + i],
                        IsActive = true
                    };

                    context.tblArtistGroupMembers.Add(objGroup);
                    context.SaveChanges();
                }
            }
            ModelState.Clear();
        }

        public ActionResult DeleteGroupMember(int id)
        {
            try
            {
                tblArtistGroupMember objGroupMember = context.tblArtistGroupMembers.Where(x => x.GroupMemberId == id).FirstOrDefault();

                context.tblArtistGroupMembers.Remove(objGroupMember);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -1, tab = "a1" });
                }
                else
                {
                    return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -1, tab = "a1" });
                }
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
            {
                return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = 3, tab = "a1" });
            }
            else
            {
                return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = 3, tab = "a1" });
            }
        }

        public ActionResult DeleteArtist(int id)
        {
            try
            {
                tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == id).FirstOrDefault();

                List<tblArtistGroupMember> objGroupMember = objArtist.tblArtistGroupMembers.ToList();
                foreach (var item in objGroupMember)
                {
                    context.tblArtistGroupMembers.Remove(item);
                    context.SaveChanges();
                }

                List<tblArtistPortfolio> objArtistPortfolio = objArtist.tblArtistPortfolios.ToList();

                foreach (var item in objArtistPortfolio)
                {
                    context.tblArtistPortfolios.Remove(item);
                    context.SaveChanges();
                }

                List<tblArtistCategoryMapping> objArtistCategoryMapping = objArtist.tblArtistCategoryMappings.ToList();

                foreach (var item in objArtistCategoryMapping)
                {
                    context.tblArtistCategoryMappings.Remove(item);
                    context.SaveChanges();
                }

                List<tblArtistRating> objArtistRating = objArtist.tblArtistRatings.ToList();

                foreach (var item in objArtistRating)
                {
                    context.tblArtistRatings.Remove(item);
                    context.SaveChanges();
                }

                context.tblArtists.Remove(objArtist);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return RedirectToAction("List", "Artist", new { Area = "Admin", Status = -1 });
            }

            return RedirectToAction("List", "Artist", new { Area = "Admin", Status = 1 });
        }

        public ActionResult DeleteCategoryMapping(int id)
        {
            try
            {
                tblArtistCategoryMapping objMapping = context.tblArtistCategoryMappings.Where(x => x.ArtistCategoryMappingId == id).FirstOrDefault();

                context.tblArtistCategoryMappings.Remove(objMapping);
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
                {
                    return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = -1, tab = "a2" });
                }
                else
                {
                    return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = -1, tab = "a2" });
                }
            }
            if (!string.IsNullOrEmpty(Convert.ToString(Session["EditArtistId"])))
            {
                return RedirectToAction("EditArtist", "Artist", new { Area = "Admin", id = Convert.ToInt32(Session["EditArtistId"]), Status = 3, tab = "a2" });
            }
            else
            {
                return RedirectToAction("AddArtistDetails", "Artist", new { Area = "Admin", Status = 3, tab = "a2" });
            }
        }

        public ActionResult ApproveArtist(int id)
        {
            try
            {
                tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == id).FirstOrDefault();
                objArtist.IsActive = true;
                context.Entry(objArtist).State = EntityState.Modified;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return RedirectToAction("List", "Artist", new { Area = "Admin", Status = -1 });
            }

            return RedirectToAction("List", "Artist", new { Area = "Admin", Status = 2 });
        }

        public ActionResult RejectArtist(int id)
        {
            try
            {
                tblArtist objArtist = context.tblArtists.Where(x => x.ArtistId == id).FirstOrDefault();
                objArtist.IsActive = false;
                context.Entry(objArtist).State = EntityState.Modified;
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                return RedirectToAction("List", "Artist", new { Area = "Admin", Status = -1 });
            }

            return RedirectToAction("List", "Artist", new { Area = "Admin", Status = 3 });
        }

        public JsonResult CheckUserId()
        {
            FormsIdentity frmTicket = (FormsIdentity)User.Identity;
            FormsAuthenticationTicket ticket = frmTicket.Ticket;
            int accountID = Convert.ToInt32(ticket.UserData);


            var UserDetails = "";
            if (UserDetails != null)
            {

            }
            else
            {
                ViewBag.errorMessage = "Something is wrong!";
            }
            return Json("Success");
        }
    }
}