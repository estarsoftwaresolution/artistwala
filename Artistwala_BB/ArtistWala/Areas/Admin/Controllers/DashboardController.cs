﻿using ArtistWala.Areas.Admin.Models;
using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ArtistWala.Areas.Admin.Controllers
{
    public class DashboardController : Controller
    {
        ArtistWalaEntities context = new ArtistWalaEntities();
        AESEncryption objEncrp = new AESEncryption();
        //
        // GET: /Admin/Home/
        [CustomAuthorization(LoginPage = "~/Admin/Dashboard/AdminLogin")]
        public ActionResult Index()
        {
            FormsIdentity frmTicket = (FormsIdentity)User.Identity;
            FormsAuthenticationTicket ticket = frmTicket.Ticket;
            int accountID = Convert.ToInt32(ticket.UserData);

            DashaboardModel obj = new DashaboardModel()
            {
                ArtistCount = context.tblArtists.Where(x => x.IsActive == true).Count(),
                MessageCount = context.tblUserMessages.Where(x => x.IsUnread == true && x.Fk_UserId == accountID).Count(),
                ApprovalRequestCount = context.tblArtists.Where(x => (x.IsActive ?? false) == false).Count(),
            };

            return View(obj);
        }

        [CustomAuthorization(LoginPage = "~/Admin/Dashboard/AdminLogin")]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
            Session.Abandon();

            return RedirectToAction("AdminLogin", "Dashboard", new { Area = "Admin" });
        }

        public ActionResult AdminLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AdminLogin(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                String userName = model.UserName;
                String passward = objEncrp.EncryptPassword(model.Password);
                tblUser UserTbl = AuthenticateUser(userName, passward);
                if (UserTbl != null)
                {
                    if (UserTbl.FK_RoleId == 1 || UserTbl.FK_RoleId == 2)
                    {
                        model.Password = null;
                        model.Name = UserTbl.Name;
                        model.UserId = UserTbl.UserId;

                        model.RoleId = UserTbl.FK_RoleId;

                        Session[SessionConstants.RoleId] = UserTbl.FK_RoleId;

                        return GetUserAuthentication(model);
                    }
                    else
                    {
                        ViewBag.Message = "You are not authorized.";
                        return View(model);
                    }
                }
                else
                {
                    // If we got this far, something failed, redisplay form
                    ViewBag.Message = "Username / Password is incorrect";
                    return View(model);
                }
            }
            else
            {
                ViewBag.Message = "Please enter your Username / Password";
                // If we got this far, something failed, redisplay form
                return View(model);
            }
        }

        private tblUser AuthenticateUser(String userName, String userPassword)
        {
            return context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == userName.Trim().ToUpper()
                                            && x.Password.Trim().ToUpper() == userPassword.Trim().ToUpper()
                                            && x.IsActive == true).FirstOrDefault();
        }
        private ActionResult GetUserAuthentication(LoginModel model)
        {

            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
            1, // Ticket version
            model.UserName,// Username to be associated with this ticket
            DateTime.Now, // Date/time ticket was issued
            DateTime.Now.AddHours(5), // Date and time the cookie will expire
            false, // if user has chcked rememebr me then create persistent cookie
            model.UserId.ToString(), // store the user data, in this case userId of the user
            FormsAuthentication.FormsCookiePath); // Cookie path specified in the web.config file in <Forms> tag if any.

            // To give more security it is suggested to hash it
            string hashCookies = FormsAuthentication.Encrypt(ticket);
            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hashCookies); // Hashed ticket

            Session[ArtistWala.Models.SessionConstants.UserProfileObject] = model;

            // Add the cookie to the response, user browser
            Response.Cookies.Add(cookie);
            string ReturnUrl = Convert.ToString(Session["ReturnUrl"]);

            if (!string.IsNullOrEmpty(ReturnUrl))
            {
                //ReturnUrl = ReturnUrl.Replace("/", "");
                return Redirect(ReturnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
            }
        }

    }
}