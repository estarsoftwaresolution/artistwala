﻿using ArtistWala.Areas.Admin.Models;
using ArtistWala.Common;
using ArtistWala.Models;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArtistWala.Areas.Admin.Controllers
{
    [CustomAuthorization(LoginPage = "~/Admin/Dashboard/AdminLogin")]
    public class ModaratorController : Controller
    {
        ArtistWalaEntities context = new ArtistWalaEntities();
        CommonMethods objCommon = new CommonMethods();
        AESEncryption objEncrp = new AESEncryption();
        //
        // GET: /Admin/Modarator/
        public ActionResult Index()
        {
            try
            {
                if (!objCommon.IsPageAccessible())
                {
                    return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
                }
            }
            catch (Exception ex) { }
            List<tblModarator> objMod = context.tblModarators.ToList();
            return View(objMod);
        }

        //
        // GET: /Admin/Modarator/Create
        public ActionResult Create(int id = 0)
        {
            ModaratorModel objMod = new ModaratorModel();
            try
            {
                if (!objCommon.IsPageAccessible())
                {
                    return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
                }

                if (id > 0)
                {
                    tblModarator objModarator = context.tblModarators.Where(x => x.ModaratorId == id).FirstOrDefault();

                    objMod.UserName = objModarator.tblUser.UserName;
                    objMod.Address = objModarator.Address;
                    objMod.City = objModarator.City;
                    objMod.Country = objModarator.Country;
                    objMod.DateOfBirth = objModarator.DateOfBirth;
                    objMod.Email = objModarator.Email;
                    objMod.HouseNumber = objModarator.HouseNumber;
                    objMod.IsActive = (objModarator.IsActive ?? false);
                    objMod.Phone = objModarator.Phone;
                    objMod.Mobile = objModarator.Mobile;
                    objMod.Pin = objModarator.Pin;
                    objMod.State = objModarator.State;
                    objMod.Sex = objModarator.Sex;
                    objMod.ModaratorName = objModarator.Name;
                }
            }
            catch (Exception ex) { }
            return View(objMod);
        }

        //
        // POST: /Admin/Modarator/Create
        [HttpPost]
        public ActionResult Create(ModaratorModel objMod, int id = 0)
        {
            if (!objCommon.IsPageAccessible())
            {
                return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
            }
            try
            {
                if (ModelState.IsValid)
                {
                    if (id > 0)
                    {
                        tblModarator objModarator = context.tblModarators.Where(x => x.ModaratorId == id).FirstOrDefault();

                        tblUser objUser = context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == objMod.UserName.Trim().ToUpper() && x.UserId != objModarator.FK_UserId).FirstOrDefault();
                        if (objUser == null)
                        {
                            tblUser objUsers = context.tblUsers.Where(x => x.UserId == objModarator.FK_UserId).FirstOrDefault();

                            objUsers.UserName = objMod.UserName;
                            objUsers.Password = objEncrp.EncryptPassword(objMod.Password);

                            context.Entry(objUsers).State = EntityState.Modified;
                            context.SaveChanges();

                            objModarator.Address = objMod.Address;
                            objModarator.City = objMod.City;
                            objModarator.Country = objMod.Country;
                            objModarator.DateOfBirth = objMod.DateOfBirth;
                            objModarator.Email = objMod.Email;
                            objModarator.HouseNumber = objMod.HouseNumber;
                            objModarator.IsActive = objMod.IsActive;
                            objModarator.Phone = objMod.Phone;
                            objModarator.Mobile = objMod.Mobile;
                            objModarator.Pin = objMod.Pin;
                            objModarator.State = objMod.State;
                            objModarator.Sex = objMod.Sex;
                            objModarator.Name = objMod.ModaratorName;

                            context.Entry(objModarator).State = EntityState.Modified;
                            context.SaveChanges();

                            tblEmailTemplate email = context.tblEmailTemplates.Where(x => x.TemplateId == 1).FirstOrDefault();
                            string emailBody = email.EmailBody.Replace("@Name", objUser.Name).Replace("@UserName", objUser.UserName);
                            objCommon.sendMailInHtmlFormat(email.AddCC, objMod.Email, email.AddBCC, email.EmailSubject, emailBody);

                            ModelState.Clear();
                            ViewBag.Message = "Modarator has been successfully updated.";
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "This user name already exists.";
                            //return RedirectToAction("Index", "Modarator", new { Area = "Admin" });
                        }
                    }
                    else
                    {
                        tblUser objUser = context.tblUsers.Where(x => x.UserName.Trim().ToUpper() == objMod.UserName.Trim().ToUpper()).FirstOrDefault();
                        if (objUser == null)
                        {
                            tblUser objUsers = new tblUser()
                            {
                                UserName = objMod.UserName,
                                Password = objEncrp.EncryptPassword(objMod.Password),
                                IsActive = true,
                                FK_RoleId = (int)ArtistWala.Models.AppsConstants.UserRoles.Moderator,
                                CreatedDate = DateTime.Now,
                                Name = objMod.ModaratorName
                            };

                            context.tblUsers.Add(objUsers);
                            context.SaveChanges();

                            int UserId = objUsers.UserId;

                            tblModarator objModarator = new tblModarator()
                            {
                                AddedOn = DateTime.Now,
                                Address = objMod.Address,
                                City = objMod.City,
                                Country = objMod.Country,
                                DateOfBirth = objMod.DateOfBirth,
                                Email = objMod.Email,
                                HouseNumber = objMod.HouseNumber,
                                IsActive = objMod.IsActive,
                                Phone = objMod.Phone,
                                Mobile = objMod.Mobile,
                                FK_UserId = UserId,
                                Pin = objMod.Pin,
                                State = objMod.State,
                                Sex = objMod.Sex,
                                Name = objMod.ModaratorName
                            };

                            context.tblModarators.Add(objModarator);
                            context.SaveChanges();
                            ModelState.Clear();
                            ViewBag.Message = "Modarator has been successfully added.";
                        }
                        else
                        {
                            ViewBag.ErrorMessage = "This user name already exists.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Sorry! Please try again later.";
            }

            return View();
        }

        //
        // GET: /Admin/Modarator/Delete/5
        public ActionResult Delete(int id)
        {
            if (!objCommon.IsPageAccessible())
            {
                return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
            }
            try
            {
                tblModarator objArtist = context.tblModarators.Where(x => x.ModaratorId == id).FirstOrDefault();

                context.tblModarators.Remove(objArtist);
                context.SaveChanges();
            }
            catch (Exception ex) { }
            return RedirectToAction("Index", "Modarator", new { Area = "Admin" });
        }

        public ActionResult ModaratorReport()
        {
            return View();
        }

        public PartialViewResult ModReportFilter()
        {
            List<tblModarator> objMod = context.tblModarators.ToList();
            return PartialView(objMod);
        }
        [HttpGet]
        public ActionResult Content(string Value = "")
        {
            ContentModel oContentModel = new ContentModel();
            if (Value == "")
            {

                var properties = (from t in typeof(tblStaticContent).GetProperties()
                                  select t.Name).Skip(1).ToList();
                foreach (var item in properties)
                {
                    oContentModel.ContentList.Add(new SelectListItem { Text = item, Value = item });
                }
                string str = Convert.ToString(properties.FirstOrDefault());
                var blogs = context.Database.SqlQuery<string>("SELECT CAST([StaticContentId] AS VARCHAR(20))+'_'+[" + str + "] as col FROM dbo.tblStaticContent where [" + str + "]!='' ").ToList();
                foreach (var item in blogs)
                {
                    string name = string.Empty;
                    int id = 0;
                    name = item.Split('_')[1];
                    id = Convert.ToInt32(item.Split('_')[0]);
                    oContentModel.ContentViewList.Add(new ContentDetails { StaticContentId = id, StaticContentName = name, ColumnName = str });
                }
            }
            else
            {
                var properties = (from t in typeof(tblStaticContent).GetProperties()
                                  select t.Name).Skip(1).ToList();
                foreach (var item in properties)
                {
                    if (item == Value)
                    {
                        oContentModel.ContentList.Add(new SelectListItem { Text = item, Value = item, Selected = true });
                    }
                    else
                    {
                        oContentModel.ContentList.Add(new SelectListItem { Text = item, Value = item });
                    }
                }
                string str = Value;
                var blogs = context.Database.SqlQuery<string>("SELECT CAST([StaticContentId] AS VARCHAR(20))+'_'+[" + str + "] as col FROM dbo.tblStaticContent where [" + str + "]!='' ").ToList();
                foreach (var item in blogs)
                {
                    string name = string.Empty;
                    int id = 0;
                    name = item.Split('_')[1];
                    id = Convert.ToInt32(item.Split('_')[0]);
                    oContentModel.ContentViewList.Add(new ContentDetails { StaticContentId = id, StaticContentName = name, ColumnName = str });
                }
            }

            return View(oContentModel);
        }
        [HttpPost]
        public ActionResult Content(ContentModel oModel)
        {
            tblStaticContent oStatic = new tblStaticContent();
            var blog = context.Database.ExecuteSqlCommand("INSERT INTO tblStaticContent(" + oModel.Id + ") VALUES ('" + oModel.ContentValue + "')");
            return RedirectToAction("Content", "Modarator");
        }
        public ActionResult DeleteContent(string id, string ColumnName)
        {
            string Str = string.Empty;
            string DeleteString = string.Empty;
            var blog = context.Database.ExecuteSqlCommand("UPDATE tblStaticContent SET " + ColumnName + "='' WHERE StaticContentId = " + id + "");
            var properties = (from t in typeof(tblStaticContent).GetProperties()
                              select t.Name).Skip(1).ToList();
            foreach (var item in properties)
            {
                Str += "("+item + "='' OR " + item + " IS NULL) || AND ";
            }
            string[] DelectStringArr = Str.Split(new[] { "||" }, StringSplitOptions.None);
            var list = DelectStringArr.Take(DelectStringArr.Count() - 1).ToList();
            foreach (var item in list)
            {
                DeleteString += item;
            }
            var DeleteRecord = context.Database.ExecuteSqlCommand("DELETE FROM tblStaticContent WHERE " + DeleteString + "");
            return RedirectToAction("Content", "Modarator");
        }
    }
}
