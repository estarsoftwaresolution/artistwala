﻿using ArtistWala.Areas.Admin.Models;
using ArtistWala.Common;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArtistWala.Areas.Admin.Controllers
{
    public class CategoryController : Controller
    {
        ArtistWalaEntities context = new ArtistWalaEntities();
        CommonMethods objCommon = new CommonMethods();
        //
        // GET: /Admin/Category/
        public ActionResult AddCategory(int id = 0, int status = 0)
        {
            if (objCommon.IsPageAccessible())
            {
                return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
            }
            switch (status)
            {
                case 2:
                    ViewBag.Message = "The record has been added successfully.";
                    break;
                case 3:
                    ViewBag.Message = "The record has been updated successfully.";
                    break;
                case 1:
                    ViewBag.Message = "The record has been deleted successfully.";
                    break;
                case -1:
                    ViewBag.ErrorMessage = "Sorry! Please try again later.";
                    break;
                case -2:
                    ViewBag.ErrorMessage = "The category already exist. Please try aother category name.";
                    break;
                case -3:
                    ViewBag.ErrorMessage = "The Category is associated with Artist. Cannot be deleted.";
                    break;
                default:
                    break;
            }

            CategoryModel objCategory = new CategoryModel();
            if (id > 0)
            {
                var item = context.tblArtistCategories.Where(x => x.ArtistCategoryId == id).FirstOrDefault();
                objCategory = new CategoryModel()
                {
                    ArtistTypeId = item.FK_ArtistCategoryTypeId,
                    CategoryId = item.ArtistCategoryId,
                    CategoryName = item.CategoryName,
                    IsActive = item.IsActive.Value
                };
            }
            objCategory.objArtistCategory = context.tblArtistCategories.ToList();

            return View(objCategory);
        }

        [HttpPost]
        public ActionResult AddCategory(CategoryModel objCat, int id = 0)
        {
            tblArtistCategory objCategory = new tblArtistCategory();
            try
            {
                if (id > 0)
                {                   
                    objCategory = context.tblArtistCategories.Where(x => x.ArtistCategoryId == id).FirstOrDefault();

                    objCategory.CategoryName = objCat.CategoryName;
                    objCategory.FK_ArtistCategoryTypeId = objCat.ArtistTypeId;
                    objCategory.IsActive = objCat.IsActive;

                    context.Entry(objCategory).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else
                {
                    objCategory = new tblArtistCategory()
                    {
                        CategoryName = objCat.CategoryName,
                        FK_ArtistCategoryTypeId = objCat.ArtistTypeId,
                        IsActive = objCat.IsActive
                    };

                    context.tblArtistCategories.Add(objCategory);
                    context.SaveChanges();
                }

                ModelState.Clear();

                if (id > 0)
                {
                    return RedirectToAction("AddCategory", "Category", new { Area = "Admin", id = 0, status = 3 });
                }
                else
                {
                    return RedirectToAction("AddCategory", "Category", new { Area = "Admin", id = 0, status = 2 });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("AddCategory", "Category", new { Area = "Admin", id = 0, status = -1 });
            }
        }

        public ActionResult DeleteCategory(int id = 0)
        {
            try
            {
                if (context.tblArtistCategoryMappings.Any(x => x.FK_CategoryId == id))
                {
                    return RedirectToAction("AddCategory", "Category", new { Area = "Admin", status = -3 });
                }
                else
                {
                    tblArtistCategory objPhoto = context.tblArtistCategories.Where(x => x.ArtistCategoryId == id).FirstOrDefault();

                    context.tblArtistCategories.Remove(objPhoto);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("AddCategory", "Category", new { Area = "Admin", status = -1 });
            }
            return RedirectToAction("AddCategory", "Category", new { Area = "Admin", status = 1 });
        }

        public ActionResult AddSubCategory(int id = 0, int status = 0)
        {
            if (objCommon.IsPageAccessible())
            {
                return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
            }
            switch (status)
            {
                case 2:
                    ViewBag.Message = "The record has been added successfully.";
                    break;
                case 3:
                    ViewBag.Message = "The record has been updated successfully.";
                    break;
                case 1:
                    ViewBag.Message = "The record has been deleted successfully.";
                    break;
                case -1:
                    ViewBag.ErrorMessage = "Sorry! Please try again later.";
                    break;
                case -2:
                    ViewBag.ErrorMessage = "The Subcategory already exist. Please try aother category name.";
                    break;
                case -3:
                    ViewBag.ErrorMessage = "The Subcategory is associated with Artist. Cannot be deleted.";
                    break;
                default:
                    break;
            }

            SubcategoryModel objSubCategory = new SubcategoryModel();
            if (id > 0)
            {
                var item = context.tblArtistSubCategories.Where(x => x.ArtistSubCategoryId == id).FirstOrDefault();
                objSubCategory = new SubcategoryModel()
                {
                    CategoryId = item.FK_ArtistCategoryId,
                    SubCategoryName = item.SubCategoryName,
                    IsActive = item.IsActive.Value
                };
            }
            objSubCategory.objCategory = context.tblArtistCategories.ToList();
            objSubCategory.objSubCategory = context.tblArtistSubCategories.ToList();

            return View(objSubCategory);
        }

        [HttpPost]
        public ActionResult AddSubCategory(SubcategoryModel objSubcat, int id = 0)
        {
            tblArtistSubCategory objCategory = new tblArtistSubCategory();
            try
            {

                if (id > 0)
                {
                    //if (context.tblArtistCategories.Any(x => x.CategoryName.Contains(objCat.CategoryName) && x.ArtistCategoryId != id))
                    //{
                    //    return RedirectToAction("AddCategory", "Category", new { Area = "Admin", status = -2 });
                    //}

                    objCategory = context.tblArtistSubCategories.Where(x => x.ArtistSubCategoryId == id).FirstOrDefault();

                    objCategory.SubCategoryName = objSubcat.SubCategoryName;
                    objCategory.FK_ArtistCategoryId = objSubcat.CategoryId;
                    objCategory.IsActive = objSubcat.IsActive;

                    context.Entry(objCategory).State = EntityState.Modified;
                    context.SaveChanges();
                }
                else
                {
                    //if (context.tblArtistCategories.Any(x => x.CategoryName.Contains(objCat.CategoryName)))
                    //{
                    //    return RedirectToAction("AddCategory", "Category", new { Area = "Admin", status = -2 });
                    //}

                    objCategory = new tblArtistSubCategory()
                    {
                        SubCategoryName = objSubcat.SubCategoryName,
                        FK_ArtistCategoryId = objSubcat.CategoryId,
                        IsActive = objSubcat.IsActive
                    };

                    context.tblArtistSubCategories.Add(objCategory);
                    context.SaveChanges();
                }

                ModelState.Clear();

                if (id > 0)
                {
                    return RedirectToAction("AddSubCategory", "Category", new { Area = "Admin", id = 0, status = 3 });
                }
                else
                {
                    return RedirectToAction("AddSubCategory", "Category", new { Area = "Admin", id = 0, status = 2 });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("AddSubCategory", "Category", new { Area = "Admin", id = 0, status = -1 });
            }
        }

        public ActionResult DeleteSubCategory(int id = 0)
        {
            try
            {
                if (context.tblArtistCategoryMappings.Any(x => x.FK_SubCategoryId == id))
                {
                    return RedirectToAction("AddSubCategory", "Category", new { Area = "Admin", status = -3 });
                }
                else
                {
                    tblArtistSubCategory objPhoto = context.tblArtistSubCategories.Where(x => x.ArtistSubCategoryId == id).FirstOrDefault();
                    context.tblArtistSubCategories.Remove(objPhoto);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("AddSubCategory", "Category", new { Area = "Admin", status = -1 });
            }
            return RedirectToAction("AddSubCategory", "Category", new { Area = "Admin", status = 1 });
        }
    }
}