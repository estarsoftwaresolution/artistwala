﻿using ArtistWala.Areas.Admin.Models;
using ArtistWala.Common;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ArtistWala.Areas.Admin.Controllers
{
    [CustomAuthorization(LoginPage = "~/Admin/Dashboard/AdminLogin")]
    public class SettingsController : Controller
    {
        ArtistWalaEntities context = new ArtistWalaEntities();
        CommonMethods objCommon = new CommonMethods();
        AESEncryption objEncrp = new AESEncryption();
        //
        // GET: /Admin/Settings/
        public ActionResult AddSettings(int id)
        {
            GeneralSettingModel obj = new GeneralSettingModel();
            try
            {
                if (!objCommon.IsPageAccessible())
                {
                    return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
                }

                tblGeneralSetting objSetting = context.tblGeneralSettings.Find(id);

                obj = new GeneralSettingModel()
                {
                    SMTPServer = objSetting.SMTPServer,
                    SMTPPort = (objSetting.SMTPPort ?? 0),
                    FromEmail = objSetting.FromEmail,
                    EmailPassword = objSetting.EmailPassword,
                    SSLSecurity = (objSetting.SSLSecurity ?? false),
                    SMSId = objSetting.SMSId,
                    SMSLink = objSetting.SMSLink,
                    SMSPassword = objSetting.SMSPassword,
                    MaxProfilePicSize = objSetting.MaxProfilePicSize ?? 0,
                    MaxPhotoSize = objSetting.MaxPhotoSize ?? 0,
                    MaxNoOfVideo = objSetting.MaxNoOfVideo ?? 0,
                    MaxNoOfPhoto = objSetting.MaxNoOfPhoto ?? 0,
                    MaxNoOfAudio = objSetting.MaxNoOfAudio ?? 0,
                    MaxCoverPicSize = objSetting.MaxCoverPicSize ?? 0,
                    MaxAudioSize = objSetting.MaxAudioSize ?? 0,
                    MobileVerification = (objSetting.MobileVerification ?? false),

                };

            }
            catch (Exception ex) { }

            return View(obj);
        }

        [HttpPost]
        public ActionResult AddSettings(GeneralSettingModel obj, int id)
        {
            try
            {
                if (!objCommon.IsPageAccessible())
                {
                    return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
                }

                tblGeneralSetting objSetting = context.tblGeneralSettings.Find(id);

                objSetting.SMTPServer = obj.SMTPServer;
                objSetting.SMTPPort = obj.SMTPPort;
                objSetting.FromEmail = obj.FromEmail;
                objSetting.EmailPassword = obj.EmailPassword;
                objSetting.SSLSecurity = obj.SSLSecurity;
                objSetting.SMSId = obj.SMSId;
                objSetting.SMSLink = obj.SMSLink;
                objSetting.SMSPassword = obj.SMSPassword;
                objSetting.MaxAudioSize = obj.MaxAudioSize;
                objSetting.MaxCoverPicSize = obj.MaxCoverPicSize;
                objSetting.MaxNoOfAudio = obj.MaxNoOfAudio;
                objSetting.MaxNoOfPhoto = obj.MaxNoOfPhoto;
                objSetting.MaxNoOfVideo = obj.MaxNoOfVideo;
                objSetting.MaxPhotoSize = obj.MaxPhotoSize;
                objSetting.MaxProfilePicSize = obj.MaxProfilePicSize;
                objSetting.SMSId = obj.SMSId;
                objSetting.MobileVerification = obj.MobileVerification;
                context.Entry(objSetting).State = EntityState.Modified;
                context.SaveChanges();
                ViewBag.Message = "The Setting has been successfully updated.";
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Sorry! Please try again later.";
            }
            return View(obj);
        }

        public ActionResult AddEmailTemplate(int id = 0)
        {
            EmailModel objEmail = new EmailModel();
            try
            {
                if (!objCommon.IsPageAccessible())
                {
                    return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
                }

                if (id > 0)
                {
                    tblEmailTemplate obj = context.tblEmailTemplates.Find(id);

                    objEmail = new EmailModel()
                    {
                        AddBCC = obj.AddBCC,
                        AddCC = obj.AddCC,
                        AssociatedTags = obj.AssociatedTags,
                        EmailBody = obj.EmailBody,
                        EmailSubject = obj.EmailSubject,
                        IsActive = (obj.IsActive ?? false),
                        TemplateId = obj.TemplateId,
                        TemplateName = obj.TemplateName
                    };
                }

            }
            catch (Exception ex) { }
            return View(objEmail);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddEmailTemplate(EmailModel obj, int id = 0)
        {
            try
            {
                if (!objCommon.IsPageAccessible())
                {
                    return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
                }

                tblEmailTemplate objEmail = context.tblEmailTemplates.Find(id);

                objEmail.AddBCC = obj.AddBCC;
                objEmail.AddCC = obj.AddCC;
                objEmail.AssociatedTags = obj.AssociatedTags;
                objEmail.EmailBody = obj.EmailBody;
                objEmail.EmailSubject = obj.EmailSubject;
                objEmail.IsActive = obj.IsActive;
                objEmail.TemplateName = obj.TemplateName;

                context.Entry(objEmail).State = EntityState.Modified;
                context.SaveChanges();
                ModelState.Clear();

                ViewBag.Message = "The Template has been successfully updated.";
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Sorry! Please try again later.";
            }
            return View();
        }

        public ActionResult AddSMSTemplate(int id = 0)
        {
            SMSModel objSMS = new SMSModel();
            try
            {
                if (!objCommon.IsPageAccessible())
                {
                    return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
                }

                if (id > 0)
                {
                    tblSMSTemplate obj = context.tblSMSTemplates.Find(id);

                    objSMS = new SMSModel()
                    {
                        TemplateId = obj.TemplateId,
                        TemplateName = obj.TemplateName,
                        SMSBody = obj.SMSBody,
                        IsActive = (obj.IsActive ?? false),
                    };
                }

            }
            catch (Exception ex) { }
            return View(objSMS);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddSMSTemplate(SMSModel obj, int id = 0)
        {
            try
            {
                if (!objCommon.IsPageAccessible())
                {
                    return RedirectToAction("Index", "Dashboard", new { Area = "Admin" });
                }

                tblSMSTemplate objSMS = context.tblSMSTemplates.Find(id);

                objSMS.TemplateName = obj.TemplateName;
                objSMS.SMSBody = obj.SMSBody;
                objSMS.IsActive = obj.IsActive;


                context.Entry(objSMS).State = EntityState.Modified;
                context.SaveChanges();
                ModelState.Clear();

                ViewBag.Message = "The Template has been successfully updated.";
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Sorry! Please try again later.";
            }
            return View();
        }

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel objCP)
        {
            try
            {
                int UserId = default(int);
                var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null)
                {
                    var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    int.TryParse(authTicket.UserData, out UserId);
                    // data is empty !!!
                }

                tblUser objUser = context.tblUsers.Find(UserId);

                if (objEncrp.DecryptPassword(objUser.Password) == objCP.OldPassword)
                {
                    objUser.Password = objEncrp.EncryptPassword(objCP.NewPassword);

                    context.Entry(objUser).State = EntityState.Modified;
                    context.SaveChanges();

                    var Modarators = context.tblModarators.Where(x => x.FK_UserId == UserId).FirstOrDefault();
                    string emailTo = string.Empty;
                    if (Modarators != null)
                    {
                        emailTo = Modarators.Email;
                    }

                    tblEmailTemplate email = context.tblEmailTemplates.Where(x => x.TemplateId == 4).FirstOrDefault();
                    string emailBody = email.EmailBody.Replace("@Name", objUser.Name).Replace("@Password", objEncrp.DecryptPassword(objUser.Password));
                    objCommon.sendMailInHtmlFormat(email.AddCC, emailTo, email.AddBCC, email.EmailSubject, emailBody);

                    ViewBag.Message = "Your password has been successfully updated.";
                }
                else
                {
                    ViewBag.ErrorMessage = "Your current password is not matched.";
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Sorry! Please try gain later.";
            }

            ModelState.Clear();

            return View();
        }
    }
}