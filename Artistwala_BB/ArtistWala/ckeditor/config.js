﻿/*
Copyright (c) 2003-2011, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license


http://docs.cksource.com/CKEditor_3.x/Developers_Guide/Toolbar
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.toolbar = 'MyToolbar';
    config.forcePasteAsPlainText = true;
    config.toolbar_MyToolbar =
	[
	{ name: 'document', items: ['Source', 'NewPage', 'Preview', 'Templates'] },
		{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
		{ name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
		{ name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley'
                 ]
		},
              
		{ name: 'styles', items: ['TextColor', 'BGColor', 'FontSize', 'Format'] },
		{ name: 'basicstyles', items: ['Bold', 'Italic', 'Strike', 'Underline', 'RemoveFormat'] },
		{ name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] },
		{ name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
		{ name: 'tools', items: ['Maximize', '-', 'About'] }
//		{ name: 'document', items: ['Source', 'NewPage', 'Preview'] },
//		{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
//		{ name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
//		{ name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'
//                 , 'Iframe']
//		},
//                '/',
//		{ name: 'styles', items: ['TextColor', 'BGColor',  'FontSize', 'Format'] },
//		{ name: 'basicstyles', items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat'] },
//		{ name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] },
//		{ name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
		//		{ name: 'tools', items: ['Maximize', '-', 'About'] }


	];
};
