﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ArtistWala
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "ArtistProfile",
                url: "Artist/{Profile}",
                defaults: new { controller = "Users", action = "ArtistDetails", Profile = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "QuickQuote",
                url: "QuickQuote/{Profile}",
                defaults: new { controller = "Users", action = "QuickQuote", Profile = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "AdvanceSearch",
                url: "Find/{SearchName}",
                defaults: new { controller = "Users", action = "SearchCategory", SearchName = UrlParameter.Optional }
            );
            //routes.MapRoute(
            //    name: "SearchResult",
            //    url: "Find/{SearchText}/{SearchId}/{Location}",
            //    defaults: new { controller = "Users", action = "SearchResults", SearchText = UrlParameter.Optional, SearchId = UrlParameter.Optional, Location = UrlParameter.Optional }
            //);
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
