﻿using System.Web;
using System.Web.Optimization;

namespace ArtistWala
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/js/jquery-{version}.js",
                        //"~/js/jquery.Jcrop.js",
                         //"~/js/jquery.min1.js",
                         "~/js/animatescroll.js",
                         "~/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js",
                     "~/js/fancybox/source/jquery.fancybox.js?v=2.1.5"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/js/jquery.validate*"
                        , "~/js/jquery.unobtrusive*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/js/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                //"~/css/jquery.Jcrop.css",
                    "~/css/style.css",
                     "~/css/responsive.css",
                     "~/css/extra.css",
                     "~/js/fancybox/source/jquery.fancybox.css?v=2.1.5"
                     
                     ));

            ScriptBundle thirdPartyScripts = new ScriptBundle("~/Scripts/ThirdParty");
            thirdPartyScripts.Include("~/Scripts/jquery-{version}.js",
                                "~/Scripts/bootstrap.min.js");

            bundles.Add(thirdPartyScripts);
            BundleTable.EnableOptimizations = true;
        }
    }
}
