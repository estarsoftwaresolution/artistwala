﻿using ArtistWala.Models;
using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ArtistWala.Common
{
    public class CustomAuthorization : AuthorizeAttribute
    {
        public string LoginPage { get; set; }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            ArtistWalaEntities context = new ArtistWalaEntities();
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.HttpContext.Response.Redirect(LoginPage);
            }

            LoginModel model = (LoginModel)HttpContext.Current.Session[SessionConstants.UserProfileObject];

            if (model != null)
            {
                if (model.RoleId != 1 && model.RoleId != 2)
                {
                    FormsAuthentication.SignOut();

                    HttpContext.Current.Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
                    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    HttpContext.Current.Response.Cache.SetNoStore();
                    HttpContext.Current.Session.Abandon();

                    filterContext.HttpContext.Response.Redirect(LoginPage);
                }                
            }           

            base.OnAuthorization(filterContext);
        }
    }
}