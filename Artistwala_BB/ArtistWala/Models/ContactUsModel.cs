﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Models
{
    public class ContactUsModel
    {
        [Required(ErrorMessage = "Please enter your name")]
        [StringLength(100)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter your email")]
        [StringLength(150)]
        public string Email { get; set; }
       
        [StringLength(100)]
        public string Company { get; set; }
        
        [StringLength(15)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Please enter your contact reason")]
        [StringLength(100)]
        public string ContactReason { get; set; }

        [Required(ErrorMessage = "Please enter your message")]
        [StringLength(400)]
        public string Message { get; set; }
    }
}