﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Models
{
    public class UserProfileModel
    {
        public int FK_UserId { get; set; }
        public int ClientId { get; set; }

        [Display(Name = "User Name")]
        [Required]
        [StringLength(20)]        
        public string UserName { get; set; }

        [Display(Name = "Client Name")]
        [Required]
        [StringLength(150)]
        public string ClientName { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(20)]
        public string ContactNo { get; set; }

        

    }
}