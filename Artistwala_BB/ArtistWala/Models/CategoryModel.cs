﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtistWala.Models
{
    public class CategoryModel
    {

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }
        public List<SubCategoryModel> Subcategorylist = new List<SubCategoryModel>();
    }

    public class SubCategoryModel
    {
        public int SubCategoryId { get; set; }

        public string SubCategoryName { get; set; }
    }
}