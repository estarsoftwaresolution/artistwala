﻿using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtistWala.Models
{
    public class SearchModel
    {
        public List<tblArtist> ArtistList { get; set; }

        public List<tblArtistCategoryType> CartgoryType { get; set; }
    }
}