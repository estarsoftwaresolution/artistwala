﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Models
{
    public class MyProfileModel
    {
        public int ArtistId { get; set; }

        [Display(Name = "User Name")]
        [Required]
        [StringLength(100)]
        public string UserName { get; set; }

        [Display(Name = "Artist Name")]
        [Required]
        [StringLength(150)]
        public string ArtistName { get; set; }

        [Display(Name = "Date Of Birth")]
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DateOfBirth { get; set; }

        [Required]
        [StringLength(10)]
        public string Sex { get; set; }

        [Display(Name = "House Number")]
        [Required]
        [StringLength(50)]
        public string HouseNumber { get; set; }

        [Required]
        [StringLength(350)]
        public string Address { get; set; }

        [Required]
        [StringLength(50)]
        public string City { get; set; }

        [Required]
        [StringLength(50)]
        public string State { get; set; }

        [Required]
        [StringLength(50)]
        public string Country { get; set; }

        [Required]
        [StringLength(10)]
        public string Pin { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        [StringLength(300)]
        public string Website { get; set; }

        [StringLength(20)]
        public string Phone { get; set; }

        [Required]
        [StringLength(20)]
        public string Mobile { get; set; }
        public bool? IsActive { get; set; }

        public DateTime AddedOn { get; set; }

        [Display(Name = "About Me")]
        [Required]
        [StringLength(500)]
        public string AboutMe { get; set; }
       
        public int? FK_UserId { get; set; }

        public string VideoId { get; set; }

        public Nullable<bool> Notification { get; set; }
    }

}