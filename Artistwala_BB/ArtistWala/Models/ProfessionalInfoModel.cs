﻿using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Models
{
    public class ProfessionalInfoModel
    {
        public int ArtistId { get; set; }
        public bool? IsGroup { get; set; }

        [Display(Name = "Featured")]
        public bool IsFeatured { get; set; }

        [Display(Name = "Minimum Rates")]
        [Required]
        public decimal MinmumRates { get; set; }

        [Display(Name = "Facebook Fan Link")]
        [StringLength(300)]
        public string FB_FanLink { get; set; }

        public List<tblArtistGroupMember> groupMember { get; set; }

    }
}