﻿using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace ArtistWala.Models
{
    public class ArtistRating
    {
        public int ArtistRatingId { get; set; }
        public Nullable<int> FK_ArtistId { get; set; }
        public Nullable<bool> IsRated { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> Rating { get; set; }
        public string Comment { get; set; }

        public string ArtistName { get; set; }
        public string ClientName { get; set; }

        public string ClientImage { get; set; }

        public string ProfileImage { get; set; }
        public List<tblArtistRating> ImageList { get; set; }
    }
}