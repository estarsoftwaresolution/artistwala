﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtistWala.Models
{
    /// <summary>
    /// /this is for artist over view
    /// </summary>
    public class ArtistOverviewModel
    {
        public int OverviewId { get; set; }
        public int ArtistId { get; set; }
        public string WhatToExpect { get; set; }
        public string Influence { get; set; }
        public string Calender { get; set; }
        public string Personal { get; set; }
        public string Tesimonials { get; set; }
        public string SetUpRequirements { get; set; }
    }
}