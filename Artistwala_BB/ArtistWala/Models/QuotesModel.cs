﻿using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;

namespace ArtistWala.Models
{
    public class QuotesModel
    {
        //this.tblArtistPortfolios = new HashSet<tblArtistPortfolio>();
        //public virtual ICollection<tblArtistPortfolio> tblArtistPortfolios { get; set; }
        public int QuoteId { get; set; }
        //public int ArtistId { get; set; }
        public string profile { get; set; }
        [Required]
        public string FK_EventType { get; set; }
        [Required]
        public int NoOfGuest { get; set; }

        [Required]
        [StringLength(40)]
        public string City { get; set; }

        [Required]
        [StringLength(40)]
        public string Venue { get; set; }
        public int Quote_Id { get; set; }
        public bool IsVenueKnown { get; set; }

        public bool IsEventDateKnown { get; set; }

        public bool IsSendFreeQuote { get; set; }

        [Required]
        public DateTime? EventDate { get; set; }        
        
        [Required]
        public string EventDuration { get; set; }
        public string DurationText { get; set; }
        public string TypeText { get; set; }


        [Required]
        public string EventStartTime { get; set; }

        public string StartTimeText { get; set; }
        public Nullable<int> AssignTo { get; set; }
        public bool UnAssign { get; set; }

        public string Review { get; set; }
        public Nullable<int> FK_ClientId { get; set; }
        public string Client_Name { get; set; }

        public string ArtistName { get; set; }
        public DateTime AddedDate { get; set; }
        [Required]
        public string SearchText1 { get; set; }
        [Required]
        public string Location1 { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        [StringLength(15)]
        public string Phone { get; set; }

        [Required]
        [StringLength(100)]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Email is not valid")]
        public string Email { get; set; }

        //[Required]
        //[StringLength(50)]
        //[DataType(DataType.Password)]
        //public string Password { get; set; }

        [Required]
        [StringLength(1900)]
        public string EventDetails { get; set; }
        public string Comment { get; set; }

        public List<tblStaticContent> EventsTypes { get; set; }
        public List<SelectListItem> EventsType = new List<SelectListItem>();
        public List<SelectListItem> EventTime = new List<SelectListItem>();
        public List<SelectListItem> EvntDuration = new List<SelectListItem>();

        public List<tblArtistCategoryMapping> ArtistCategoryMapping = new List<tblArtistCategoryMapping>();
        public List<tblArtistRating> tblArtistRating = new List<tblArtistRating>();
        //public DbSet<tblArtistPortfolio> tblArtistPortfolios { get; set; }
        //public List<tblArtistPortfolio> tblArtistPortfolios { get; set; }
        //public tblArtistPortfolio tblArtistPortfolios { get; set; }
        public List<tblQuote> AllQuotes { get; set; }
        public virtual tblQuote tblQuotes { get; set; }

        public virtual tblUser tblUsers { get; set; }
        public virtual tblArtist tblArtists { get; set; }
    }
}