﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Models
{
    public class LoginModel
    {

        public int UserId { get; set; }

        [Required(ErrorMessage = "Please enter your email")]
        [StringLength(100)]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Please enter your password")]
        [StringLength(50, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool IsActiveUser { get; set; }

        public int RoleId { get; set; }

        public bool IsRemember { get; set; }

        public string Name { get; set; }

        public bool IsArtist { get; set; }

        public bool IsPromoKit{ get; set; }

        public int ArtistId { get; set; }

        public string ProfileImage { get; set; }

        public string preview { get; set; }
    }
}