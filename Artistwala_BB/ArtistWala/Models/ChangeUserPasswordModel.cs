﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArtistWala.Models
{
    public class ChangeUserPasswordModel
    {
        [Required]
        [StringLength(50)]
        [DataType(DataType.Password)]
        public string OldPassword { get; set; }

        [MinLength(6, ErrorMessage = "Please enter min 6 character")]
        [Required]
        [StringLength(50)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [MinLength(6, ErrorMessage = "Please enter min 6 character")]
        [Required]
        [StringLength(50)]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Passwords must match")]
        public string ConfirmPassword { get; set; }
    }
}