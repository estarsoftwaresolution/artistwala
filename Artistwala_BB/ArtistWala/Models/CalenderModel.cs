﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ArtistWala.Models
{
    public class CalenderModel
    {

        public int CalanderEventId { get; set; }
        public int FK_ArtistId { get; set; }

        public string date { get; set; }

        [Required]
        public DateTime? ScheduleDate { get; set; }

        public int TypeId { get; set; }
        public int TimeId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public string Description { get; set; }

        public string Url { get; set; }

        public bool IsActive { get; set; }
        public List<SelectListItem> EventTypeList = new List<SelectListItem>();
        public List<SelectListItem> TimeiList = new List<SelectListItem>();
    }

    public class CalenderSchedule
    {

        public string date { get; set; }

        public string type { get; set; }

        public string title { get; set; }

        public string description { get; set; }

        public string url { get; set; }
        public string EventTime { get; set; }
    }
}