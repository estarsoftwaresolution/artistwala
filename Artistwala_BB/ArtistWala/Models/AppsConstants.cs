﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArtistWala.Models
{
    public class AppsConstants
    {

        public enum ArtistCategoryType
        {
            MusicalActs = 1,
            Entertainers = 2,
            Speakers = 3,
            OtherServices = 4
        }

        public enum UserRoles
        {
            Admin = 1,
            Moderator = 2,
            Artist = 3,
            Client = 4
        }

        public enum FileType
        {
            Image,
            Audio,
            Video,
            Link
        }

        public enum AuthenticationError
        {
            InvalidUser = 1,
            AccountExists = 2,
            NotExixts = 3,
            EmailSend = 4,
            ServerError = -1
        }
    }

    public class SessionConstants
    {
        public const string ProfilImage = "ProfilImage";
        public const string CoverImage = "CoverImage";
        public const string ImageName = "ImageName";
        public const string UserProfileObject = "UserProfileObject";
        public const string RoleId = "RoleId";
        public const string UserId = "UserId";
        public const string ArtistId = "ArtistId";
    }

    public class Messages
    {
        public const string RegistrationWelcomeMessage = "Welcome to Artistwala.";
        public const string RegistrationWelcomeMessageBody = "On the Insert tab, the galleries include items that are designed to coordinate with the overall look of your document. You can use these galleries to insert tables, headers, footers, lists, cover pages, and other document building blocks. When you create pictures, charts, or diagrams, they also coordinate with your current document look.";
    }
}