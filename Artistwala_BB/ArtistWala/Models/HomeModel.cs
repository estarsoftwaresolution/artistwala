﻿using ArtistWala.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ArtistWala.Models
{
    public class HomeModel
    {
        public List<tblArtistCategoryType> CartgoryType { get; set; }

        [StringLength(50)]
        [Required]
        public string SearchText { get; set; }
        [StringLength(50)]
        [Required]
        public string Location { get; set; }
        public string SearchId { get; set; }
    }
}